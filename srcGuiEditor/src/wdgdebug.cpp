/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgdebug.h"
#include "ui_wdgdebug.h"
#include <QClipboard>

void wdgDebug::showStdErr()
{
  QString line;
  line = QString(this->octProc->readAllStandardError());
  QStringList lst = line.split('\n', QString::SkipEmptyParts);
  for(int i=0; i < lst.count(); i++)
    if(lst[i].length())
        ui->lstConsoleOut->addItem(">> " + lst[i]);
  ui->lstConsoleOut->scrollToBottom();
}

void wdgDebug::showStdOut()
{
    QStringList lines;
    lines= QString(this->octProc->readAllStandardOutput()).split('\n', QString::SkipEmptyParts);
    for(int i=0; i < lines.count(); i++)
      if(lines[i].length())
          ui->lstConsoleOut->addItem(">> " + lines[i]);
    ui->lstConsoleOut->scrollToBottom();
}

void wdgDebug::availData()
{
    QString line;
    lineOutput = lineOutput + QString(this->octProc->readAll());
    while(lineOutput.indexOf('\n') != -1)
    {
         line = lineOutput.left(lineOutput.indexOf('\n'));
         lineOutput.remove(0, lineOutput.indexOf('\n'));
         line.remove('\n');
         if(line.length())
           ui->lstConsoleOut->addItem(line);
    }
}

wdgDebug::wdgDebug(QProcess * octProc, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wdgDebug)
{
    this->octProc = octProc;
    ui->setupUi(this);
}

wdgDebug::~wdgDebug()
{
    delete ui;
}

void wdgDebug::on_btnExec_clicked()
{
    if(this->octProc)
    {
        if(this->octProc->isOpen())
        {
            this->octProc->write((ui->leCMD->text()+"\n").toLatin1());
            this->ui->lstConsoleOut->addItem("<< " + ui->leCMD->text());
            ui->leCMD->clear();
        }
        else
        {
            ui->lstConsoleOut->addItem(tr("Octave is not running!"));
        }
    }
    else
    {
        ui->lstConsoleOut->addItem("Octave is not running!");
    }
}

void wdgDebug::on_btnCopyOutput_clicked()
{
    QList<QListWidgetItem *> selection = ui->lstConsoleOut->selectedItems();
    QList<QListWidgetItem *>::iterator it;
    QString txt;
    for(it = selection.begin(); it != selection.end(); it++)
    {
        txt = txt + (*it)->text() + "\r\n";
    }
    qApp->clipboard()->setText(txt);
}

void wdgDebug::on_btnClearConsole_clicked()
{
    ui->lstConsoleOut->clear();
}
