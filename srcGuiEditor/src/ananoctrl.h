/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ANANOCTRL_H
#define ANANOCTRL_H

#include <QWidget>
#include "abstractuicctrl.h"
#include <QLabel>
#include "commonproperties.h"
#include "controlgenerator.h"


class expOctClass: public abstractPropEditor
{
  Q_OBJECT
private:
    bool expClass;
public:
    expOctClass(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual bool canGenerateCode();
    virtual QWidget * getEditWidget();
public slots:
  virtual void callBackChanged(int state);
};

class cbArduinoSerialPortEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    QString serialPort;
  public:
    cbArduinoSerialPortEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void cbSerialPortChanged(QString text);
};

class cbPinTypeEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    int pinNumber;
    bool pinIsReadOnly;
  public:
    cbPinTypeEditor(abstractUICCtrl *ctrl, int pinNumber, bool isReadOnly = false);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void cbPinDirChanged(QString text);
};


class aNanoCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;  

public:
    static unsigned int getNameCounter();
    aNanoCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual ~aNanoCtrl();
    //
    virtual QString className() { return "aNanoCtrl";}
    virtual bool isAutoSize() { return true;}
};


class aNanoCtrlGen: public controlGenerator
{
public:
  aNanoCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // ANANOCTRL_H
