/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgsrceditor.h"
#include "ui_wdgsrceditor.h"
#include "mainwnd.h"

wdgSrcEditor::wdgSrcEditor(QWidget *parent, QMdiArea *mdi) :
    QWidget(parent),
    ui(new Ui::wdgSrcEditor)
{   
    ui->setupUi(this);
    this->clients = mdi;
    this->selectedWdg = NULL;
    ui->sourceCode->clear();


    ui->sourceCode->setFont(mainWnd::getMainWnd()->getAppConfig()->getEditorFont());
    QFontMetrics fm(mainWnd::getMainWnd()->getAppConfig()->getEditorFont());
    ui->sourceCode->setTabStopWidth(mainWnd::getMainWnd()->getAppConfig()->getEditorTabSize() * fm.width(" "));

    connect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));
}

wdgSrcEditor::~wdgSrcEditor()
{
    disconnect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));    
    delete ui;
}

void wdgSrcEditor::updateWidgetListRecursive(QList<abstractUICCtrl *> widgets)
{
    QList<abstractUICCtrl *>::Iterator it;
    if(widgets.count() > 0)
    {
        for(it = widgets.begin(); it != widgets.end(); it++)
        {
            this->widgets.push_back(*it);
            if((*it)->isContainer())
                this->updateWidgetListRecursive((*it)->addedWidgets);
        }
    }
}

void wdgSrcEditor::updateWidgetList(QList<abstractUICCtrl *> widgets, bool force)
{
    QList<abstractUICCtrl *>::Iterator it;
    QStringList src;
    QString srcLine;

    /* Desconcetamos signal y slot asociado al cambio de control.
     * Por las dudas, luego, guardamos el código fuente que puede estar escrito en el control.
    */
    disconnect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));

    if(this->selectedWdg != NULL)
    {
        if(ui->sourceCode->document()->isModified())
        {

         for (QTextBlock it = ui->sourceCode->document()->begin();
              it != ui->sourceCode->document()->end();
              it = it.next())
         {
              srcLine = it.text();
              srcLine.remove('\n');
              srcLine.remove('\r');
              src.append(srcLine);
          }
          this->selectedWdg->setSrcCallBack(src);
        }
    }

    /*
      Actualizamos la lista de controles de modo recursivo si corresponde
    */
    if((this->widgets != widgets) || (force))
    {

      this->widgets.clear();
      this->selectedWdg = NULL;
      for(it = widgets.begin(); it != widgets.end(); it++)
      {
          this->widgets.push_back(*it);
          if((*it)->isContainer() && ((*it)->className() != "frameDlg"))
              this->updateWidgetListRecursive((*it)->addedWidgets);
      }

       /* Recargamos la lista de controles */
      ui->cbSrcControl->clear();
      for(it = this->widgets.begin(); it != this->widgets.end(); it++)
         ui->cbSrcControl->addItem(QString((*it)->getCtrlName()));
    }

    /* Establecemos el primer control como seleccinado (siempre va a existir
     * un primer control, la ventana propiamente dicha),
     * pero por las dudas comprobamos que sea válido.
     */
    if(!this->widgets.isEmpty())
    {
        this->selectedWdg = this->widgets.first();
        src = this->selectedWdg->getSrcCallBack();
        ui->sourceCode->clear();
        QString srcText;
        for(int i = 0; i < src.count(); i++)
             srcText += src[i] + '\n';
        ui->sourceCode->setPlainText(srcText);
        ui->sourceCode->document()->setModified(false);
    }
    else
        this->selectedWdg = NULL;
    connect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));
}

void wdgSrcEditor::saveSourceCode()
{
    QStringList src;
    QString srcLine;
    if(this->selectedWdg != NULL)
    {
        if(ui->sourceCode->document()->isModified())
        {
            for (QTextBlock it = ui->sourceCode->document()->begin();
                 it != ui->sourceCode->document()->end(); it = it.next())
            {
                srcLine = it.text();
                srcLine.remove('\n');
                srcLine.remove('\r');
                src.append(srcLine);
            }
            this->selectedWdg->setSrcCallBack(src);
        }
        ui->sourceCode->document()->setModified(false);
    }
}

void wdgSrcEditor::closeChildWnd(void)
{
    QString srcLine;
    QStringList src;
    disconnect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));

    if(this->selectedWdg != NULL)
    {
        if(ui->sourceCode->document()->isModified())
        {
          for (QTextBlock it = ui->sourceCode->document()->begin();
               it != ui->sourceCode->document()->end();
               it = it.next())
          {
              srcLine = it.text();
              srcLine.remove('\n');
              srcLine.remove('\r');
              src.append(srcLine);
          }
          this->selectedWdg->setSrcCallBack(src);
        }
    }
    this->widgets.clear();
    this->selectedWdg = NULL;
    ui->cbSrcControl->clear();
    ui->sourceCode->clear();
    connect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));
}

void wdgSrcEditor::deleteControls(void)
{
    disconnect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));
    this->widgets.clear();
    this->selectedWdg = NULL;
    ui->cbSrcControl->clear();
    ui->sourceCode->clear();
    connect(ui->cbSrcControl, SIGNAL(currentTextChanged(QString)), this, SLOT(changeCmbIndex(QString)));
}

void wdgSrcEditor::closeDlg()
{
    ui->sourceCode->clear();
    this->widgets.clear();
    ui->cbSrcControl->clear();
}

void wdgSrcEditor::actualWidget(abstractUICCtrl * ctrl)
{
    int idx;
    idx = this->ui->cbSrcControl->findText(ctrl->getCtrlName());
    this->ui->cbSrcControl->setCurrentIndex(idx);
    //this->ui->cbSrcControl->setCurrentText(ctrl->getCtrlName());
}

void wdgSrcEditor::updateWidgetsNames()
{
    QString selectedName;
    QList<abstractUICCtrl *>::Iterator it;
    this->changeCmbIndex(ui->cbSrcControl->currentText());
    if(this->selectedWdg != NULL)
      selectedName = this->selectedWdg->getCtrlName();
    ui->cbSrcControl->clear();
    for(it = this->widgets.begin(); it != this->widgets.end(); it++)
      ui->cbSrcControl->addItem((*it)->getCtrlName());
    if(this->selectedWdg != NULL)
      this->ui->cbSrcControl->setCurrentIndex(this->ui->cbSrcControl->findText(selectedName));
}

void wdgSrcEditor::changeCmbIndex(const QString &arg1)
{
    QStringList src;
    QList<abstractUICCtrl *>::Iterator it;
    QString srcLine;
    if(this->selectedWdg != NULL)
    {
        if(ui->sourceCode->document()->isModified())
        {
          for (QTextBlock it = ui->sourceCode->document()->begin();
               it != ui->sourceCode->document()->end();
               it = it.next())
          {
              srcLine = it.text();
              srcLine.remove('\n');
              srcLine.remove('\r');
              src.append(srcLine);
          }
          this->selectedWdg->setSrcCallBack(src);
        }
    }

    for(it = this->widgets.begin();
        it != this->widgets.end();
        it++)
    {
        if((*it)->getCtrlName() == arg1)
        {
            this->selectedWdg = *it;
            src = this->selectedWdg->getSrcCallBack();
            ui->sourceCode->clear();
            QString srcText;
            for(int i = 0; i < src.count(); i++)
            {
                if(i < (src.count() - 1))
                   srcText += src[i] + '\n';
                else
                   srcText += src[i];
            }
            ui->sourceCode->setPlainText(srcText);
            ui->sourceCode->document()->setModified(false);
        }
    }
}
