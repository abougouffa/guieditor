/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ABSTRACTUICCTRL
#define ABSTRACTUICCTRL

#include "adjpointtopleftclass.h"
#include "adjpointmidleleftclass.h"
#include "adjpointbottomleftclass.h"
#include "adjpointtoprightclass.h"
#include "adjpointmidlerightclass.h"
#include "adjpointbottomrightclass.h"
#include "adjpointtopmidleclass.h"
#include "adjpointbottommidleclass.h"
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QCheckBox>
#include "childwnddlg.h"

class abstractUICCtrl;
class abstractPropEditor;

//!  Representación una interfaz que se utiliza para derivar un control para el editor de propiedades.
/*!
  Cada una de las propiedades de cada control tienen un nombre y un valor que puede
  ser editado. El cambio en el valor de una propiedad puede estar dado por 3 orígenes:
  - Se modificó a través del editor de propiedades (la más simple y donde esta clase representa
  el control genérico que se utilizada para interactuar con el usuario).
  - El control cambió debido a que el usuario lo seleccionó y presionó una tecla de dirección.
  - El control cambió debido a que el usuario hizo click sobre alguno de los puntos de ajuste.
  Un problema es lograr que, más allá del origen del cambio, siempre mantenga actualizado
  el control y el valor presentado en el editor de propiedades.
  Para lograr esto, se trabaja con un widget genérico que puede ser actualizado. Debido a que los
  controles derivan todos de la clase QWidget, para poder representar un control genérico
  que pueda ser actualizado, se utilizó herencia múltiple. Entonces, los controles que se utilizan
  derivan de un QWidget (QLineEdit fundamentalmente, QComboBox o QSpinBox) y de un derivado de esta
  clase. Como esta clase guarda una referencia a la propiedad que incluye el nombre y contiene un
  método abstracto que representa la actualización, para cada control específico (QLineEdit, QComboBox,etc)
  se tiene un derivado de esta clase que es el responsable de actualizar los valores en el
  inspector de propiedades según cambien. (O sea, actualiza el valor en el editor de propiedades
  cuando el origen del cambio en las propiedades fue dado por otro origen, o el teclado o el mouse).
*/
class updPropWdg
{
  private:
    //! Propiedad abstracta, asociada al control actualizable.
    abstractPropEditor * prop;
  public:
    //! Constructor que toma como argumento la propiedad a la cual se encuentra asociado.
    explicit updPropWdg(abstractPropEditor * prop);
    //! Getter para evitar el acceso directo al puntero y mantener el encapsulamiento.
    abstractPropEditor * getPropEditor();
    //! Método virtual puro que, según el tipo de control asociado a la propiedad, debe ser implementado
    /*!
      El método updPropValue debe implementarse de modo que, cuando sea invocado, el valor
      del control que se visualiza en el inspector de propiedades se corresponda con el
      valor de la propiedad. Esto es necesario porque para realizar la actualización de los
      widgets se debe contar con un método que sincronice el valor de las propiedades con
      su contenido. Debido a que no existe, se utiliza herencia múltiple de un control y
      de esta interfáz. La interfaz proporciona el método actualizable.
    */
    virtual void updPropValue()=0;
};

//!  Interfaz actualizable aplicada a controles que se basan en un QLineEdit
/*!
  Esta interfaz solo implementa el método updPropValue que toma el valor
  almacenado en la propiedad en la clase base y lo asigna a la propiedad text
  del QLineEdit.
*/
class editUpdPropWdg:public QLineEdit, public updPropWdg
{
    Q_OBJECT
public:
    explicit editUpdPropWdg(abstractPropEditor * prop);
    virtual void updPropValue();
};

//!  Interfaz actualizable aplicada a controles que se basan en un QComboBox
/*!
  Esta interfaz solo implementa el método updPropValue que toma el valor
  almacenado en la propiedad en la clase base y lo asigna a la propiedad currentIndex
  del QComboBox.
*/
class comboUpdPropWdg: public QComboBox, public updPropWdg
{
    Q_OBJECT
public:
    explicit comboUpdPropWdg(abstractPropEditor * prop);
    virtual void updPropValue();
};

//!  Interfaz actualizable aplicada a controles que se basan en un QSpinBox
/*!
  Esta interfaz solo implementa el método updPropValue que toma el valor
  almacenado en la propiedad en la clase base y lo asigna a la propiedad value
  del QSpinBox.
*/
class spinUpdPropWdg: public QSpinBox, public updPropWdg
{
    Q_OBJECT
public:
    explicit spinUpdPropWdg(abstractPropEditor * prop);
    virtual void updPropValue();
};

//!  Interfaz actualizable aplicada a controles que se basan en un QCheckBox
/*!
  Esta interfaz solo implementa el método updPropValue que toma el valor
  almacenado en la propiedad en la clase base y lo asigna a la propiedad value
  del QCheckBox.
*/
class checkBoxUpdPropWdg: public QCheckBox, public updPropWdg
{
    Q_OBJECT
public:
    explicit checkBoxUpdPropWdg(abstractPropEditor * prop);
    virtual void updPropValue();
};

//! Clase base utilizada para representar una propiedad abstracta genérica de un control.
/*!
  \brief Clase abstractPropEditor
  Todas las propiedades deben derivar de esta clase y es el punto de partida para
  resolver la mayoría de los problemas que resuelve la aplicación. Toda propiedad tiene
  un nombre, este nombre es utilizado para representar el nombre de la propiedad en
  el inspector de propiedades, pero también y más importante se utiliza para persistir
  el estado de los objetos en archivos y para restaurarlos. Los diálogos y controles
  se almacenan en archivos XML, así cada control se identifica por un nombre de clase y
  para cada clase se identifica cada propiedad por su nombre.

  Debido a que la mayoría de las propiedades tienen un valor asociado y que debe ser
  representado en el control utilizado no se tiene una variable que contenga el valor
  de la propiedad. En vez de esto se guarda la referencia la control asociado. Así, cuando
  cambia el valor de la propiedad desde el inspector de propiedades es simple modificar
  el atributo del control correspondiente.

  Todos las propiedades se representan en el inspector de propiedades con dos widgets.
  - Uno se utiliza para representar el título (y retorna un QLabel).
  - El valor editable se representa con un control derivado de un "control actualizable updPropWdg
  y de un QWidget. Los métodos getTitleWidget y getEditWidget retornan los controles asociados.

  En lo que respecta a los métodos canGenerateCode y generateCode se utilizan para indicar
  cuando una propiedad es capaz de generar líneas de código exportable. Esto nos lleva a que
  no todas las propiedades (necesariamente) generan código.

  El slot valueChanged se utiliza para captar las modificaciones que se realicen sobre
  el control asociado a la propiedad. De este modo, cuando el valor de la propiedad
  es modificado en el inspector de propiedades, se invoca automáticamente a este slot,
  pudiéndose realizar los cambios que sean necesarios en el control asociado. Por defecto
  este slot se conecta a la señal editingFinished del QLineEdit. En caso de utilizar otro
  control asociado a la propiedad (por ejemplo un QComboBox), lo que se lograría sobre
  escribiendo el método getEditWidget se debería definir otro slot y conectarlo con la señal
  que corresponda antes de retornar el widget.
*/
class abstractPropEditor: public QObject
{
    Q_OBJECT
  protected:
    QString propName;
    abstractUICCtrl *ctrl;
  public:
    //! El constructor es el responsable se asociar a cada propiedad un control.
    abstractPropEditor(abstractUICCtrl *ctrl);
    //! Retorna el nombre de la propiedad.
    QString getPropName();
    //! Retorna el valor de la propiedad como una cadena de caracteres.
    virtual QString getValue() = 0;
    //! Método responsable de validar una cadena que contiene el valor escrito en el editor
    virtual bool isValidValue(QString) = 0;
    //! Este método establece un valor en la propiedad y no realiza validación.
    virtual void setValue(QString) = 0;
    //! Retorna el código que genera una estado particular en el control en octave.
    virtual QString generateCode();
    //! Indica cuando la propiedad puede generar código. Si este método retorna false, generateCode debería retornar una cadena nula.
    virtual bool canGenerateCode();
    //! Retorna un QLabel conteniendo el nombre de la propiedad para ser registrado en el editor de propiedades
    virtual QWidget * getTitleWidget();
    //! Retorna un Widget (por defecto un QLineEdit) conteniendo el valor actual de la propiedad.
    virtual QWidget * getEditWidget();
  public slots:
    //! Slot asociado al QLineEdit retornado por getEditWidget.
    /*!
     * Este slot es el responsable de validar el valor contenido en el QLineEdit asociado.
     * Si este valor es válido, entonces el nuevo valor de la propiedad debe ser asignado
     * utilizando el método setValue.
     */
    virtual void valueChanged();
};

//! Propiedad concreta utilizada para representar el nombre del control.
/*!
 * \brief Clase namePropEditor
 *
 * Esta clase representa el nombre del control. Debido a que el nombre del control
 * es utilizado para su creación pero no en el resto del código, esta propiedad
 * no genera código. Es utilizada al momento de generar el nombre del control
 * cuando se genera el script de octave y para almacenar y recuperar el formulario
 * desde un archivo.
 * Dadas las características de la propiedad serán valores válidos cadenas iniciadas
 * con una letra, sin caracteres especiales ni espacios en blanco.
 */
class namePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    bool nameExists(QString newName, abstractUICCtrl *ctrl, abstractUICCtrl *ctrlContainer);
public:
    //! Construye la propiedad a partir de un control y establece el nombre de la propiedad.
    namePropEditor(abstractUICCtrl *ctrl);
    //! Getter que retorna el valor actual de la propiedad.
    virtual QString getValue();
    //! Retorna true si el valor es válido (la cadena puede representar el nombre de una variable)
    /*!
     * El valor es validado utilizando una expresión regular. La expresión utilizada es ^[a-zA-Z_][a-zA-Z0-9_]*$
     * Esto debe entenderse como:
     * - La cadena puede comenzar con una letra mayúscula, minúscula o guión bajo.
     * - Debe continuar con una letra mayúscula, minúscula, guión bajo o un número.
     */
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual bool canGenerateCode() {return false;}
    //! Retorna un Widget (por defecto un QLineEdit) conteniendo el valor actual de la propiedad.
    /*!
     * Este método debió ser reimplementado para poder asociarse a un slot diferente que permita
     * por cada cambio de la propiedad name actualizar la lista de controles en la ventana de edición
     * de código fuente.
     */
    virtual QWidget * getEditWidget();
public slots:
  //! Slot asociado al QLineEdit y utilizado en getEditWidget para establecer el nuevo valor.
  /*!
   * Este slot es el responsable de validar el valor contenido en el QLineEdit asociado.
   * Si este valor es válido, entonces el nuevo valor de la propiedad debe ser asignado
   * utilizando el método setValue. Ha sido sobre escrito respecto del método de la clase base
   * debido a que por cada cambio en el nombre es necesario también modificar la lista de controles
   * en la ventana de edición de código fuente.
   */
  virtual void nameChanged();
};
//! Esta clase representa una colección propiedades asociadas a un control.
/*!
 * \brief Clase propEditor
 *
 * Cada control tiene asociadas un conjunto de propiedades. De este modo, cada propiedad
 * constituye un elemento atómico y esta clase (propEditor) es la responsable de agruparlos.
 * El agrupamiento se logra a través de una lista implementada como un objeto de la clase
 * QList.
 * La mayoría de los métodos de la clase implementan las funcionalidades básicas para la
 * gestión de la lista.
 */
class propEditor
{
private:
    //! Lista de objectos que representan las propiedades.
    QList<abstractPropEditor *> propertieList;
    //! Iterador utilizado para recorrer la lista.
    QList<abstractPropEditor *>::iterator itera;
public:
    //! En esta clase el constructor no realiza ninguna tarea.
    propEditor();
    //! Registra una propiedad asociada en la lista.
    /*!
     * Este método es uno de los más importantes de esta clase. La relación que se
     * tiene entre los controles y las propiedades es indirecta, aún en el proceso
     * de creación. Esta relación se logra, creando los diferentes objetos asociados
     * a las propiedades de un control desde el constructor del control. Allí, una vez
     * creada la propiedad es registrada en el objeto de la clase propEditor contenido
     * en el control  abstractUICCtrl. La relación se termina de cerrar a través
     * del constructor de las propiedades que recibe como argumento la dirección del objeto
     * control asociado.
     */
    void registerProp(abstractPropEditor * prop);
    //! Retorna la primera propiedad de la lista e inicializa el iterador.
    abstractPropEditor * getFirst();
    //! Retorna el siguiente objeto que representa una propiedad haciendo avanzar el iterador.
    /*!
     * Retorna la dirección de un objeto que representa una propiedad del objeto que contiene
     * el objeto de la clase propEditor. Antes de realizar invocaciones a este método debería
     * invocarse una vez al método getFirst. Accediendo de este modo al primer elemento de la
     * lista. Luego debe invocarse al método getNext a los siguientes valores. Cuando se alcanzó
     * el último valor de la lista (en realidad el end del iterador que indica el fin de la lista)
     * este método retorna null. Por esto, la forma genérica de uso es:
     *
     *
     *
     \code
      abstractPropEditor * prop;
      prop = properties.getFirst();
      while(prop)
      {
         prop->getValue()
          ...
         prop = properties.getNext();
      }
      \endcode
    */
    abstractPropEditor * getNext();
    //! Este método retorna, para un nombre de propiedad contenido en la lista el valor de la misma.
    /*!
     * El argumento representa el nombre de la propiedad de interés. Debido a que la
     * búsqueda se realiza en una lista enlazada donde no hay ninguna regla o algoritmo
     * para realizar la inserción, se ha implementado como una búsqueda secuencial.
     * Por todo esto, es un método que no debería utilizarce mucho.
     */
    QString getValueByName(QString name);
    //! Método similar a getValueByName, pero en vez de retornar el valor de la propiedad retorna el objeto que representa la propiedad.
    abstractPropEditor * getPropByName(QString name);
    //! Método que retorna la cantidad total de propiedades registradas en la lista.
    unsigned int getCount();
    //! Destructor por defecto. Es responsable de eliminar todos los objetos que representan propiedades y que están contenidos en la lista.
    virtual ~propEditor();
};

class childWndDlg;

//! Clase utilizada para representar un control genérico.
/*!
 * \brief Clase abstractUICCtrl
 *
 * Esta clase representa un control gráfico. Ha sido definida fundamentalmente para poder
 *trabajar con los diferentes controles utilizando polimorfismo. Por lo que todas las
 * listas que contienen los controles agregados a una ventana, serán listas de objetos
 *de esta clase.
 */
class abstractUICCtrl
{
  private:
    //! Esta propiedad representa el nombre del control.
    /*!
     * Si bien existe una propiedad que permite modificar el nombre del control, las propiedades
     * deben entenderse como métodos de acceso especiales a los atributos de un control.
     * Por lo que en este diseño, las propiedades no almacenan valores específicos de las propiedades.
     * En general, lo que se hace, es que el valor de las propiedades se asocia a algún atributo
     * del objeto al que pertenece la propiedad. Por ejemplo en el caso de la posición, este valor
     * es almacenado en la geometría del control (del qwdidget) de esta forma se logra la correlacionar
     * fácilmente el valor de la propiedad con la representación del control.
     * En el caso de atributos que no tienen una representación directa, como es el caso del nombre
     * del control, el valor de la propiedad se implementa como un atributo de la clase que representa
     * el control.
     */
    QString ctrlName;
    //! Representa el widget que contiene al control.
    /*!
     * Todos los Widgets están se agregan sobre otro widget que hace las veces de contenedor.
     * Este atributo (parentContainer), representa al control que contiene al QWidget.
     */
    QWidget * parentContainer;

    //! Representa el control que contiene el actual dentro de la jerarquía de octave
    /*!
     * Del mismo modo que en la interfaz gráfica de Qt, los controles en octave tienen una estructura
     * jerárquica. Esta no puede ser implementada directamente a través de especialización (herencia)
     * debido a que se debería reimplementar toda la gerarquía de controles de Qt. Para resolver esto
     * se plantea una jerarquía en paralelo que permite conocer, para un control en octave, cuál es
     * su "antecesor" jerárquico.
     */
    abstractUICCtrl * octaveParent;
    //! Control que representa en la ventana al control gráfico.
    /*!
     * Cada uno de los controles que se pueden colocar en una ventana de diálogo tiene
     * (necesariamente) un widget asociado. De este modo se logra que pueda verse en la
     * ventana. Este puntero almacena la referencia del control utilizado para lograr la
     * representación gráfica del control en tiempo de edición.
     */
    QWidget *vAssociatedWidget;




    bool vCanChangeName;
  protected:
    //! Punto de ajuste ubicado arriba/izquierda
    adjPointTopLeftClass *adjTL;
    //! Punto de ajuste ubicado medio/izquierda
    adjPointMidleLeftClass *adjML;
    //! Punto de ajuste ubicado abajo/izquierda
    adjPointBottomLeftClass *adjBL;

    //! Punto de ajuste ubicado arriba/derecha
    adjPointTopRightClass *adjTR;
    //! Punto de ajuste ubicado medio/derecha
    adjPointMidleRightClass *adjMR;
    //! Punto de ajuste ubicado abajo/derecha
    adjPointBottomRightClass *adjBR;

    //! Punto de ajuste ubicado al medio del borde superior
    adjPointTopMidleClass *adjTM;
    //! Punto de ajuste ubicado al medio del borde inferior
    adjPointBottomMidleClass *adjBM;


    //! Representa si el control puede o no contener otros controles
    /*!
     * Desde un punto de vista de generalidad, todos los controles, por su estructura, podrían contener
     * otros controles. Desde un punto de vista de diseño, esto podría ser posible. El diseño del modelo
     * puede pertirlo, sin embargo, desde un punto de vista de la implementación no todos los controles
     * pueden poser controles en su interior. En la práctica, solo un container vinculado a un QWidget,
     * podría contener otros controles.
     * Este atributo, por defecto se inicializa en falso, pero en estos dos casos se altera a true en
     * los constructores específicos a fin de representar que puede contener otros controles.
     */
    bool vIsContainer;

    //! Atributo utilizado para representar si un control tiene o no un callback asociado.
    bool vHaveCallback;
    //! Este atributo representa el código fuente asociado al callback del control.
    QStringList srcCallBack;

    //! Ventana a la que pertenece el control.
    /*!
     * \brief parentWnd representa la venatana en la que se encuentra insertado el control.
     * Es utilizada para referenciar y actualizar las ventanas de propiedades y de codigo fuente.
     */
    childWndDlg * parentWnd;

    //! Retorna el conjunto de lineas de codigo comunes a todas las funciones de callback
    /*! Todas las funciones de callback parten de una declaracion comun. Esta funcion retorna
     *  las cadenas de caracteres que se agregaran previas al codigo especifico de la funcion
     *  que ha sido agregado por el usuario.
     */
    virtual QStringList preCallback(QString fcnName="", QStringList argName = QStringList());

    //! Retorna el conjunto de lineas de codigo comunes a todas las funciones de callback que cierran la declaracion
    /*! Todas las funciones de callback parten de una declaracion comun. Esta funcion retorna
     *  las cadenas de caracteres que se agregaran posteriores al codigo especifico de la funcion
     *  que ha sido agregado por el usuario.
     */
    virtual QStringList posCallback(void);


  public:
    void setAssociatedWidget(QWidget *q){ this->vAssociatedWidget = q;                                          
                                          this->vAssociatedWidget->setParent(this->parentContainer);}
   /*Actualiza el widget parent del control así como de los puntos de ajuste
    * Debe invocarse cuando se coloca un widget en un contendor diferente de una ventana.
    */
    void setContainnerWidget(QWidget *p)
    {
        this->vAssociatedWidget->setParent(p);
        this->adjTL->setParent(p);
        this->adjML->setParent(p);
        this->adjBL->setParent(p);
        this->adjTR->setParent(p);
        this->adjMR->setParent(p);
        this->adjBR->setParent(p);
        this->adjTM->setParent(p);
        this->adjBM->setParent(p);
    }

    QWidget * getAssociatedWidget()     { return this->vAssociatedWidget;}
    QList<abstractUICCtrl *> addedWidgets;
    QList<abstractUICCtrl *>::iterator itAddedWdg;
    childWndDlg * getParentWnd(){return this->parentWnd;}
    void setParentWnd(childWndDlg *p){this->parentWnd = p;}
    bool haveCtrlWithName(QString, abstractUICCtrl *);
    abstractUICCtrl * getFirstCtrl();
    abstractUICCtrl * getNextCtrl();
    virtual void addWidget(abstractUICCtrl *w);

    void setCanChangeName(bool can) { vCanChangeName = can;}
    bool canChangeName() {return vCanChangeName;}

    //! Constructor de la clase. Toma como argumento la dirección de memoria del control de nivel superior que contendrá a este control.
    /*!
     * El constructor de esta clase es responsable de:
     * - Asociar la dirección de memoria del objeto que contendrá el control gráfico al atributo
     * parentContainer.
     * - Crear los objetos asociados a los puntos de ajuste.
     * - Para una clase derivada concreta, crear el widget que representará el control en la ventana.
     * - Registrar cada una de las propiedades que se apliquen a este control a través de
     * invocaciones como:
     \code
      this->properties.registerProp(new namePropEditor(this));
     \endcode
     *
     */
    explicit abstractUICCtrl(QWidget *parent,
                             abstractUICCtrl *octaveParent,
                             childWndDlg * parentWnd);
    /// Este método se utiliza para cambiar la vinculación de un control a una ventana y, ocasionalmente
    /// a un control parent.
    virtual void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd) = 0;

    //! Getter asociado a la propiedad vIsContainer.
    bool isContainer(){ return this->vIsContainer; }

    //! Getter para la propiedad octaveParent
    /*!
     * Retorna el control que contiene, en la jerarquía de octave, al control actual.
     * Notar que puede ser un puntero nulo si se trata del primer nivel de la jerarquía.
     */
    abstractUICCtrl * getOctaveParent();

    //! Setter para la propiedad octaveParent
    /*!
     * Establece el control que contiene, en la jerarquía de octave, al control actual.
     * Notar que puede ser un puntero nulo si se trata del primer nivel de la jerarquía.
     */
    void setOctaveParent(abstractUICCtrl *p) {this->octaveParent = p;}

    //! Getter para la propiedad parentContainer
    /*!
     * Retorna la dirección del objeto que contiene al widget actual.
     */
    QWidget * getParentContainer();
    //! Este atributo retorna una bandera que indica si el control debe actualizar tamaño de modo automático o no.
    /*!
     * Al momento de escribir esta documentación, el único control que ha sido pensado como
     * capaz de soportar esta funcionalidad es el QLabel debido a es conveniente que recalcule
     * su tamaño en función del contenido. El resto de los controles no cuentan con esta función.
     * Sin embargo, si su tamaño es cero en alguna dimensión al momento de agregarse a la ventana
     * su tamaño se toma un tamaño por defecto. Esto no tiene que ver con esta funcionalidad
     * pero dada su semejanza en el sentido funcional se ha documentado aquí.
     *
     */
    virtual bool isAutoSize();
    //! Destructor de la clase. La tarea más importante que realiza este método es elminar todos los objetos que representan los puntos de ajuste
    virtual ~abstractUICCtrl();
    //! Getter para el atributo ctrlName.
    QString getCtrlName();    
    //! Setter para el atributo ctrlName.
    void setCtrlName(QString name);
    //! Al invocarse este método, hace que el control aparezca como seleccionado.
    /*!
     * Un control seleccionado muestra los puntos de ajuste al rededor de si mismo (en realidad,
     * cada punto de ajuste se dibuja a si mismo). Este método hace que se dibujen todos los
     * puntos de ajuste.
     */
    void select();
    //! Hace que el control aparezca como deseleccionado.
    /*!
     * De modo similar al caso del método select() este método hace que los puntos de ajuste
     *del control se oculten invocando al método hide() de cada punto de ajuste.
     */
    void deselect();
    //! Actualiza la posición de los puntos de ajuste para el control.
    /*!
     * Todos los controles pueden ser movidos y dedimensionados utilizando el inspector de
     *propiedades, el mouse o el teclado (con las feclas de dirección). Ocasionalmente, cuando
     *se utilizan las teclas de dirección se da que al salir los puntos de ajuste del espacio
     *visible de la ventana no vuelven a hacerse visible. Este método, al ser invocado, hace
     *que se recalculen las coordenadas de cada punto de ajuste en función de la geometría y
     *posición del control al que se encuentran asociados.
     */
    void updateAdjPoints();
    //! Retorna la geometría del control asociado.
    virtual const QRect position();
    //! Retorna el control gráfico utilizado para representar al control en la ventana.
    virtual QWidget *associatedWidget();
    //! Getter para el nombre de la clase del control.
    virtual QString className() = 0;
    //! Getter para la lista de cadenas que representaría al control en un archivo .m
    /*!
     * Cada propiedad, en general, se asocia a un parámetro de un uicontrol. El proceso de
     *lograr la representación completa de un control es generar cada una de las cadenas que
     *representa una propiedad de un control lográndose así un conjunto de cadenas que representan
     *al control en su todo. Este método retorna la representación completa del control a partir
     *de los valores de las propiedades.
     */
    virtual QStringList generateMFile(QString path) = 0;
    //! Crea un archivo de callback para el control en la ruta indicada.
    /*! Este método debería ser invocado solo desde generateMFile(QString path)
     * Al momento de desarrollar esta funcionalidad, Octave solo soporta asociar callbacks que
     *estén definidos en archivos externos. No pudiéndose contener en un solo archivo .m múltiples callbacks.
     *Es por este, que es te método genera para un control su callback asociada.
    */
    virtual QStringList createCallBack(QString path) = 0;

    //! Indica si es necesario o no asociar la declaración de un callback a un control
    virtual bool mustDeclareCallBack(void) {return true;}

    //! Retorna un valor booleano que indica si existe un callback asociado al control.
    virtual bool haveCallBack(void);
    //! Setter para el atributo vHaveCallback.
    virtual void setHaveCallBack(bool v);

    //! Getter asociado a la propiedad srcCallBack
    QStringList getSrcCallBack();
    //! Setter asociado a la propiedad srcCallBack
    void setSrcCallBack(QStringList src);

    //! Objeto que representa la lista de todas las propiedades que son aplicables al objeto.    
    propEditor properties;
};

Q_DECLARE_METATYPE(abstractUICCtrl *);

#endif // ABSTRACTUICCTRL
