/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "listctrl.h"
#include "editablelist.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include "wdgitemeditor.h"
#include "ui_wdgitemeditor.h"

//// Implementación de los métodos de los editores de propiedades


listItemPropEditor::listItemPropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "String";
}

QString listItemPropEditor::getValue()
{
    int i;
    editableList * list;
    QString ret;
    list = (editableList * )this->ctrl->associatedWidget();
    for(i=0; i < list->count(); i++)
    {
        ret = ret + list->item(i)->text();
        if(i < list->count() - 1)
            ret += "|";
    }
    return ret;
}

bool listItemPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
  //  QRegularExpression r("^\\[[-+]?\\d{1,10}\\s[-+]?\\d{1,10}\\s\\d{1,10}\\s\\d{1,10}\\]$");
    // return newVal.contains(r) == 1;
    return true;
}

void listItemPropEditor::setValue(QString newVal)
{
    QStringList v;
    editableList * list = (editableList * )this->ctrl->associatedWidget();
    int i;
    if(this->isValidValue(newVal))
    {
        list->clear();
        v = newVal.split("|");
        for(i=0; i<v.count(); i++)
            list->addItem(v[i]);
    }
}

QString listItemPropEditor::generateCode()
{
    QString ret;
    ret = "'String', '" + this->getValue() + "'";
    return ret;
}


QWidget * listItemPropEditor::getEditWidget()
{
    wdgItemEditor * wdgId = new wdgItemEditor(this);
    wdgId->setItems(this->getValue());
    connect(wdgId->getLEItems(), SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
    return wdgId;
}

void listItemPropEditor::valueItemChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {
        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
}

lstBgClrPropEditor::lstBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
}

QString lstBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Base).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Base).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Base).blueF(), 'f', 3) + "]";
    return ret;
}

bool lstBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void lstBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::Base, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString lstBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * lstBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void lstBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

lstFgClrPropEditor::lstFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString lstFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Text).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Text).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Text).blueF(), 'f', 3) + "]";
    return ret;
}

bool lstFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void lstFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::Text, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString lstFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * lstFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void lstFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int listCtrl::uicNameCounter = 0;

listCtrl::listCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableList * list = new editableList(parent, this);
    uicNameCounter++;
    this->setCtrlName("ListBox_"+QString::number(uicNameCounter));
    list->addItem("Value A");
    list->addItem("Value B");
    list->addItem("Value C");
    this->setAssociatedWidget(list);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());


    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new listItemPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new lstBgClrPropEditor(this));
    this->properties.registerProp(new lstFgClrPropEditor(this));

}

void listCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableList * list = dynamic_cast<editableList *>(this->associatedWidget());
    list->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList listCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','listbox'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList listCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

listCtrl::~listCtrl()
{
  delete this->getAssociatedWidget();
}


/// Implementación de la clase generadora
///



listCtrlGen::listCtrlGen():controlGenerator("listCtrl")
{
}

abstractUICCtrl * listCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    listCtrl * list = new listCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = list->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = list->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      list->setSrcCallBack(src.split('\n'));

    list->deselect();
    return list;

}
