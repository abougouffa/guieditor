/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "serialcmd.h"

int serialCmd::maxRetry = 5;

serialCmd::serialCmd(prgAction *act, bool isLastCmd)
{
    this->retryCount = maxRetry;
    this->associatedAction = act;
    this->lastCmd = isLastCmd;
    this->retVal = 0x00;
}

prgAction * serialCmd::getAction(void)
{
    return this->associatedAction;
}

bool serialCmd::isLastCmd(void)
{
    return this->lastCmd;
}

uint32_t serialCmd::getRcvData()
{
    return this->retVal;
}

bool serialCmd::mustSkip(void)
{
    return false;
}

void serialCmd::addRetry()
{
    retryCount++;
}

int serialCmd::getRetry()
{
    return retryCount;
}

serialCmd::~serialCmd(void)
{
}
