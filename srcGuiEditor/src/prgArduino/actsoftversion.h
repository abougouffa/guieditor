/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ACTSOFTVERSION_H
#define ACTSOFTVERSION_H
#include "prgaction.h"
#include "scgetparam.h"

class actSoftVersion : public prgAction
{
private:
    scGetParam * cmd01;
    scGetParam * cmd02;
public:
    actSoftVersion():prgAction()
    {
        cmd01 = new scGetParam(this, Parm_STK_SW_MAJOR, false);
        cmd02 = new scGetParam(this, Parm_STK_SW_MINOR, true);
    }
    virtual QString info()
    {
        QString ret;
        ret = "Software Version: " + QString::number(cmd01->getRcvData()) + "." + QString::number(cmd02->getRcvData());
        return ret;
    }

    virtual void getSerialCmd(QVector<serialCmd *> &toSend){
        toSend.push_back(cmd01);
        toSend.push_back(cmd02);
    }

    virtual ~actSoftVersion()
    {
        delete cmd01;
        delete cmd02;
    }
};

#endif // ACTSOFTVERSION_H
