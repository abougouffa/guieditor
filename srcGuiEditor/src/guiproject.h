/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef GUIPROJECT_H
#define GUIPROJECT_H

#include "childwnddlg.h"
#include <QObject>
#include <QStringList>
#include <QString>
#include "projectitem.h"
#include <QList>
#include "prjtreewidget.h"
#include "childwnddlg.h"

class guiProject;
class childWndDlg;

class treeItemProject :  public abstractPrjTreeItem
{
    Q_OBJECT
private:
    guiProject * prj;
public:
    explicit treeItemProject(guiProject * p, QTreeWidget * parent = 0);
    void activate();
    void populateMenu(QMenu &);
    virtual void selectProject();
    guiProject * getPrj(){return prj;}
private slots:
    void actAddNewDlg(bool checked = false);
    void actAddNewMFile(bool checked = false);
    void actAddNewImage(bool checked = false);
    void actRun(bool checked = false);
    void actExport(bool checked = false);
    void actGenPkg(bool checked = false);
    void actProjectProperties(bool checkded = false);
    void actSave(bool checked = false);
    void actClose(bool checked = false);
};

class guiProject
{
private:
  QString vName;
  QString vPath;
  childWndDlg* vMainDlgName;

  unsigned vVerA;  // Versión del proyecto:
  unsigned vVerB;  //  vVerA.vVerB.vVerC
  unsigned vVerC;  //

  bool vDefBasePath;
  bool vDefImgPath;

  QStringList vSearchPath;
  void copyFolder(QString src, QString dst);
public:
    QList<projectItem *> items;
    guiProject(QString path, QString name);
    ~guiProject();

    QString name();

    QString path();
    QString fullPath();

    bool defBasePath();
    bool defImgPath();

    void setDefBasePath(bool);
    void setDefImgPath(bool);

    QString exportPath();
    QString fcnPath();
    QString guiPath();
    QString imgPath();
    QString otherPath();
    QString pkgPath();
    QString runPath();
    QString libsPath();

    void setVersion(int A, int B, int C);
    void version(int &A, int &B, int &C);

    void setMainDlg(QString);
    void setMainDlg(childWndDlg *);
    childWndDlg * getMainDlg();

    void setSearchPath(QStringList);
    QStringList searchPath();

    void show(QTreeWidgetItem *);
    void update();

    void exportPrj();
    void run();
    void generatePkg();

    void save();
};

#endif // GUIPROJECT_H
