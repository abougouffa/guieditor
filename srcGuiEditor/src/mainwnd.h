/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef MAINWND_H
#define MAINWND_H

#include <QMainWindow>
#include <QLabel>
#include <QMouseEvent>
#include <QCloseEvent>
#include <QDockWidget>

#include "configsettings.h"
#include <QProcess>

#include "wdgprjman.h"
#include "wdgprop.h"
#include "wdgdebug.h"
#include "wdgsrceditor.h"

#include "pkggenwnd.h"


enum eUICState
{
    uicStateNone = 0,
    uicStateAddWidget
};

namespace Ui {
class mainWnd;
}

class mainWnd : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit mainWnd(configSettings *appCfg, QWidget *parent = 0);
    ~mainWnd();
    void runScript(QString fn, QStringList pathsToAdd);

static wdgProp   * getPropPan();
static wdgDebug  * getDebugWnd();
static wdgSrcEditor * getSrcWnd();
static wdgPrjMan * getPrjWnd();
static mainWnd   * getMainWnd();
static pkgGenWnd * getPkgGenWnd();
    Ui::mainWnd *ui;
    abstractUICCtrl *toAdd;
    eUICState uicState;
    configSettings * getAppConfig();
private:
    static mainWnd * mainWndPtr;
    static wdgProp *propPanel;
    static wdgDebug * debugPanel;
    static wdgSrcEditor * srcEdPanel;
    static wdgPrjMan * prjManPanel;
    static pkgGenWnd *pkgWnd;

    QDockWidget * dockSrc;
    QDockWidget * dockPrj;
    QDockWidget * dockDbg;
    QDockWidget * dockProp;

    configSettings *cfg;
    QProcess octaveExec;
private slots:
    void on_actNew_triggered();
    void on_actAddBtn_triggered();
    void on_actExit_triggered();
    void on_actAddLineEdit_triggered();
    void on_actAddLabel_triggered();
    void on_actViewPropEdit_triggered();
    void on_actSave_triggered();
    void on_actLoad_triggered();
    void on_actExport_triggered();
    void on_actViewSrc_triggered();
    void on_actAddCombo_triggered();
    void on_actAddList_triggered();
    void on_actAddCheckBox_triggered();
    void on_actAddRbBtn_triggered();
    void on_actAddToggle_triggered();
    void on_actAddSlider_triggered();
    void on_actAddImg_triggered();
    void on_actSaveAs_triggered();
    void on_actProperties_triggered();
    void on_actRun_triggered();
    void on_actStop_triggered();
    void on_actViewOctCon_triggered();
    void on_actAbout_triggered();
    void on_addANano_triggered();
    void on_actAddNodeMCU_triggered();
    void on_addEduCIAA_triggered();
    void on_actAddFrame_triggered();
    void on_actAddGroupBox_triggered();
    void on_actViewProject_triggered();
    void on_actNewProject_triggered();
    void on_actOpenProject_triggered();
    void on_actRunPrj_triggered();
    void on_actSaveProject_triggered();
    void on_actClosePrj_triggered();

    void on_actAddCallBack_triggered();

    void on_actAddBtnGrp_triggered();

private:


protected:
    virtual void closeEvent(QCloseEvent *e);
};

#endif // MAINWND_H
