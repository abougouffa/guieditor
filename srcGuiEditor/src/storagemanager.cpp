/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "storagemanager.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include "buttonctrl.h"
#include "editctrl.h"
#include "textctrl.h"
#include "cmbctrl.h"
#include "togglectrl.h"
#include "checkboxctrl.h"
#include "radiobuttonctrl.h"
#include "framectrl.h"
#include "listctrl.h"
#include "sliderctrl.h"
#include "imgctrl.h"
#include "ananoctrl.h"
#include "nodeMCUctrl.h"
#include "eduCIAACtrl.h"
#include "grouppanelctrl.h"
#include "callbackctrl.h"
#include "buttongroupctrl.h"
#include <QFileInfo>
#include <QDebug>

QList<controlGenerator *>& storageManager::getGeneratorList()
{
        static QList<controlGenerator *>  generatorList;
        return generatorList;
}

int storageManager::registeredGeneratosCount()
{
    return getGeneratorList().count();
}

void storageManager::registerGenerators()
{
    getGeneratorList().push_back(new buttonCtrlGen());
    getGeneratorList().push_back(new editCtrlGen());
    getGeneratorList().push_back(new textCtrlGen());
    getGeneratorList().push_back(new rbCtrlGen());
    getGeneratorList().push_back(new cbCtrlGen());
    getGeneratorList().push_back(new cmbCtrlGen());
    getGeneratorList().push_back(new listCtrlGen());
    getGeneratorList().push_back(new frameCtrlGen());
    getGeneratorList().push_back(new toggleCtrlGen());
    getGeneratorList().push_back(new sliderCtrlGen());
    getGeneratorList().push_back(new imgCtrlGen());
    getGeneratorList().push_back(new aNanoCtrlGen());
    getGeneratorList().push_back(new nodeMCUCtrlGen());
    getGeneratorList().push_back(new eduCIAACtrlGen());
    getGeneratorList().push_back(new groupPanelCtrlGen());
    getGeneratorList().push_back(new callBackCtrlCtrlGen());
    getGeneratorList().push_back(new buttonGroupCtrlGen());
}

void storageManager::unregisterGenerators()
{
    QList<controlGenerator *>::iterator it;
    for(it = getGeneratorList().begin(); it != getGeneratorList().end(); it++)
        delete *it;
    getGeneratorList().clear();
}

void storageManager::loadDlgFromFileRecursive(childWndDlg * dlg,
                                           abstractUICCtrl * octaveParent,
                                           QXmlStreamReader &xml)
{
    QList<controlGenerator *>::iterator it;
    abstractUICCtrl * ctrl;
    QXmlStreamReader::TokenType readedToken;
    readedToken = xml.readNext();
    int x = 0;
    while(!xml.atEnd())
    {
        if(readedToken == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "uiControl")
            {
                x++;
                it = getGeneratorList().begin();
                ctrl = NULL;
                while((it != getGeneratorList().end()) && (!ctrl))
                {
                    if(xml.attributes().value("class").toString() == (*it)->getClassName())
                    {
                        ctrl = (*it)->getControl(xml, octaveParent, dlg);
                        ctrl->setOctaveParent(octaveParent);
                        octaveParent->addWidget(ctrl);
                        if(ctrl->isContainer())
                        {
                            loadDlgFromFileRecursive(dlg, ctrl, xml);
                        }
                    }
                    it ++;
                }
            }
            readedToken = xml.tokenType();
        }
        else if(readedToken == QXmlStreamReader::EndElement)
        {
            if (x == 0)
                return;
            x--;
            readedToken = xml.readNext();

        }
        else
          readedToken = xml.readNext();
    }
}

void storageManager::loadDlgFromFile(childWndDlg * dlg, QString name)
{
    QList<controlGenerator *>::iterator it;
    QFile f(name);
    QXmlStreamReader xml;
    abstractPropEditor * prop;
    QString globalSrc;
    abstractUICCtrl *ctrl;
    QPalette palette;
    frameDlg * frm = dlg->getFrame();
    QXmlStreamReader::TokenType readedToken;
    f.open(QIODevice::ReadOnly);
    xml.setDevice(&f);
    if(storageManager::registeredGeneratosCount() == 0)
        storageManager::registerGenerators();

    readedToken = xml.readNext();
    while(!xml.atEnd())
    {
        if(readedToken == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "dialog")
            {
                prop = frm->properties.getFirst();
                while(prop)
                {
                    prop->setValue(xml.attributes().value(prop->getPropName()).toString());
                    prop = frm->properties.getNext();
                }
            }
            else if(xml.name() == "GlobalSrc")
            {
                globalSrc = xml.readElementText();
                if(globalSrc.length() > 0)
                    frm->setSrcCallBack(globalSrc.split('\n'));
            }
            else if(xml.name() == "uiControl")
            {
                it = getGeneratorList().begin();
                ctrl = NULL;
                while((it != getGeneratorList().end()) && (!ctrl))
                {
                    if(xml.attributes().value("class").toString() == (*it)->getClassName())
                    {
                        ctrl = (*it)->getControl(xml, frm, dlg);

                        frm->addWidget(ctrl);
                        palette = ctrl->associatedWidget()->palette();

                        if(ctrl->isContainer())
                            loadDlgFromFileRecursive(dlg, ctrl, xml);
                    }
                    it ++;
                }
            }
        }
        readedToken = xml.readNext();
    }
    storageManager::unregisterGenerators();
    f.close();
    dlg->documentHaveName = true;
    dlg->documentName = name;
    QFileInfo fi(name);
    dlg->setWindowTitle(fi.fileName());
}

void storageManager::saveDlgToFileRecursive(abstractUICCtrl *ctrl, QXmlStreamWriter &xml)
{
    int i;
    QString srcLine;
    abstractUICCtrl *ctrToSave;
    abstractPropEditor * prop;
    ctrToSave = ctrl->getFirstCtrl();
    while(ctrToSave)
    {
          xml.writeStartElement("uiControl");
          xml.writeAttribute("class", ctrToSave->className());
          prop = ctrToSave->properties.getFirst();
          while(prop)
          {
              xml.writeAttribute(prop->getPropName(), prop->getValue());
              prop = ctrToSave->properties.getNext();
          }
          //xml.writeCharacters("\n");

          if(ctrToSave->isContainer())
              xml.writeStartElement("ContainerSrc");
          for(i=0; i < ctrToSave->getSrcCallBack().count(); i++)
          {
              srcLine = ctrToSave->getSrcCallBack()[i];
              srcLine.remove('\n');
              srcLine.remove('\r');
              if(i < ctrToSave->getSrcCallBack().count() - 1)
                  srcLine = srcLine + "\n";
              xml.writeCharacters(srcLine);
          }
          if(ctrToSave->isContainer())
              xml.writeEndElement();

          if(ctrToSave->isContainer())
              saveDlgToFileRecursive(ctrToSave, xml);

          xml.writeEndElement();

          ctrToSave = ctrl->getNextCtrl();
    }
}

void storageManager::saveDlgToFile(childWndDlg *dlg, QString fileName)
{
    int i;
    QString srcLine;
    QXmlStreamWriter xml;
    abstractUICCtrl *ctrl;
    abstractPropEditor * prop;    
    if(dlg)
    {
        frameDlg * frm = dlg->getFrame();
        QFile f(fileName);
        f.open(QIODevice::WriteOnly);
        xml.setDevice(&f);
        xml.setAutoFormatting(true);

        xml.writeStartDocument();

        xml.writeStartElement("dialog");
        prop = frm->properties.getFirst();
        while(prop)
        {
            xml.writeAttribute(prop->getPropName(), prop->getValue());
            prop = frm->properties.getNext();
        }

        xml.writeStartElement("GlobalSrc");
        for(i=0; i < frm->getSrcCallBack().count(); i++)
        {
            srcLine = frm->getSrcCallBack()[i];
            if(srcLine.contains('\r'))
              srcLine.remove('\r');
            if(srcLine.contains('\n'))
              srcLine.remove('\n');
            if(i < frm->getSrcCallBack().count() - 1)
                srcLine = srcLine + "\n";
            xml.writeCharacters(srcLine);
        }
        xml.writeEndElement();

        ctrl = frm->getFirstCtrl();
        while(ctrl)
        {
            if(ctrl != frm)
            {
                xml.writeStartElement("uiControl");
                xml.writeAttribute("class", ctrl->className());
                prop = ctrl->properties.getFirst();
                while(prop)
                {
                    xml.writeAttribute(prop->getPropName(), prop->getValue());
                    prop = ctrl->properties.getNext();
                }

                if(ctrl->isContainer())
                    xml.writeStartElement("ContainerSrc");
                for(i=0; i < ctrl->getSrcCallBack().count(); i++)
                {
                    srcLine = ctrl->getSrcCallBack()[i];
                    srcLine.remove('\n');
                    srcLine.remove('\r');
                    if(i < ctrl->getSrcCallBack().count() - 1)
                        srcLine = srcLine + "\n";
                    xml.writeCharacters(srcLine);
                }
                if(ctrl->isContainer())
                    xml.writeEndElement();
                if(ctrl->isContainer())
                    saveDlgToFileRecursive(ctrl, xml);
                xml.writeEndElement();
            }
            ctrl = frm->getNextCtrl();
        }
        xml.writeEndElement();
        xml.writeEndDocument();

        f.close();
    }
}

void storageManager::saveProject(guiProject * prj)
{
    QString fileName = prj->fullPath() + ".prj";
    QFile f(fileName);
    int vA, vB, vC;
    QXmlStreamWriter xml;
    int i;
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    xml.setDevice(&f);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    prj->version(vA, vB, vC);
    xml.writeStartElement("project");
    xml.writeAttribute("version", QString::number(vA));
    xml.writeAttribute("revision", QString::number(vB));
    xml.writeAttribute("subrevision", QString::number(vC));

    xml.writeAttribute("defineBasePath", prj->defBasePath()?"true":"false");
    xml.writeAttribute("defineImagePath", prj->defImgPath()?"true":"false");

    if(prj->getMainDlg())
      xml.writeAttribute("startDlg", prj->getMainDlg()->getName());

    for(i = 0; i < prj->searchPath().count(); i++)
      xml.writeAttribute("searchPath_" + QString::number(i), prj->searchPath()[i]);

    xml.writeEndElement();
    xml.writeEndDocument();
    f.close();
}

guiProject * storageManager::loadProject(QString fileName)
{

    QFileInfo projectInfo(fileName);
    int vA, vB, vC;
    int i;
    QString pathToAdd;
    QStringList searchPath;
    QString prjPath  = projectInfo.absolutePath();
    QString prjFileName = projectInfo.fileName().replace(".prj", "");
    guiProject * ret = new guiProject(prjPath, prjFileName);
    ret->update();
    QFile f(fileName);
    QXmlStreamReader xml;
    f.open(QIODevice::ReadOnly);
    xml.setDevice(&f);
    QXmlStreamReader::TokenType readedToken;

    readedToken = xml.readNext();
    while(!xml.atEnd())
    {
        if(readedToken == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "project")
            {
                vA = xml.attributes().value("version").toInt();
                vB = xml.attributes().value("revision").toInt();
                vC = xml.attributes().value("subrevision").toInt();
                ret->setVersion(vA, vB, vC);
                i = 0;
                while(xml.attributes().hasAttribute("searchPath_" + QString::number(i)))
                {
                    pathToAdd = xml.attributes().value("searchPath_" + QString::number(i)).toString();
                    searchPath.push_back(pathToAdd);
                    i++;
                }
                ret->setSearchPath(searchPath);
                if(xml.attributes().hasAttribute("startDlg"))
                    ret->setMainDlg(xml.attributes().value("startDlg").toString());


                if(xml.attributes().hasAttribute("defineBasePath"))
                    ret->setDefBasePath((xml.attributes().value("defineBasePath") == "true"));

                if(xml.attributes().hasAttribute("defineImagePath"))
                    ret->setDefBasePath((xml.attributes().value("defineImagePath") == "true"));

            }
        }
        readedToken = xml.readNext();
    }
    f.close();

    return ret;
}
