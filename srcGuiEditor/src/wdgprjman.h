/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef WDGPRJMAN_H
#define WDGPRJMAN_H

#include <QWidget>
#include "guiproject.h"
#include <QList>
#include "prjtreewidget.h"

namespace Ui {
class wdgPrjMan;
}

class wdgPrjMan : public QWidget
{
    Q_OBJECT

public:
    explicit wdgPrjMan(QWidget *parent = 0);
    ~wdgPrjMan();
    void addProject(guiProject *);
    void refreshList();
    void setActiveProject(guiProject *);
    guiProject * activeProject();
    void closeActiveProject();
private slots:
    void on_projectTree_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_projectTree_itemActivated(QTreeWidgetItem *item, int column);

    void on_projectTree_itemClicked(QTreeWidgetItem *item, int column);

private:
    Ui::wdgPrjMan *ui;
    QList<guiProject *> openProjects;
    guiProject * vActiveProject;
};

#endif // WDGPRJMAN_H
