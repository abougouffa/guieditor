/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "togglectrl.h"
#include "editabletoggle.h"
#include "commonproperties.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"


toggleCheckPropEditor::toggleCheckPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "Value";
}

QString toggleCheckPropEditor::getValue()
{
    editableToggle * toggle;
    QString ret = "deselect";
    toggle = (editableToggle * )this->ctrl->associatedWidget();
    if(toggle->isChecked())
        ret = "select";
    return ret;
}

bool toggleCheckPropEditor::isValidValue(QString newVal)
{
    return (newVal == "select")||(newVal == "deselect");
}

void toggleCheckPropEditor::setValue(QString newVal)
{
    editableToggle * toggle;
    toggle = (editableToggle * )this->ctrl->associatedWidget();
    if(newVal == "select")
        toggle->setChecked(true);
    else
        toggle->setChecked(false);
}

QString toggleCheckPropEditor::generateCode()
{
    QString ret = "'Min', 0, 'Max', 1, ";
    if(this->getValue() == "select")
        ret += "'" + this->propName + "', 1";
    else
        ret += "'" + this->propName + "', 0";
    return ret;
}

QWidget * toggleCheckPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("select");
    cb->addItem("deselect");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(toggleCheckedChanged(QString)));
    return cb;
}

void toggleCheckPropEditor::toggleCheckedChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(toggleCheckedChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(toggleCheckedChanged(QString)));
    }
}




toggleBgClrPropEditor::toggleBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString toggleBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Button).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).blueF(), 'f', 3) + "]";
    return ret;
}

bool toggleBgClrPropEditor::isValidValue(QString newVal)
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void toggleBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Button, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString toggleBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}


QWidget * toggleBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void toggleBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

toggleFgClrPropEditor::toggleFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString toggleFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::ButtonText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::ButtonText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::ButtonText).blueF(), 'f', 3) + "]";
    return ret;
}

bool toggleFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void toggleFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::ButtonText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString toggleFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * toggleFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void toggleFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int toggleCtrl::uicNameCounter = 0;

unsigned int toggleCtrl::getNameCounter()
{
    return uicNameCounter;
}

toggleCtrl::toggleCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):
    abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableToggle * btn;
    uicNameCounter++;
    this->setCtrlName("Toggle_"+QString::number(uicNameCounter));
    btn = new editableToggle(parent, this);
    btn->setText(this->getCtrlName());
    btn->resize(60, 25);
    this->setAssociatedWidget(btn);

    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new textTogglePropEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new toggleBgClrPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new toggleCheckPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));    
    this->properties.registerProp(new toggleFgClrPropEditor(this));
}

void toggleCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableToggle * tg = dynamic_cast<editableToggle *>(this->associatedWidget());
    tg->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList toggleCtrl::generateMFile(QString path)
{
    QStringList ret;
    abstractPropEditor * prop;
    QString lineMCode;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol(") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','togglebutton'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList toggleCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

toggleCtrl::~toggleCtrl()
{
  delete this->getAssociatedWidget();
}

toggleCtrlGen::toggleCtrlGen():controlGenerator("toggleCtrl")
{
}

abstractUICCtrl * toggleCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    toggleCtrl * btn = new toggleCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = btn->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = btn->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      btn->setSrcCallBack(src.split('\n'));

    btn->deselect();
    return btn;
}
