/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "editableradiobutton.h"
#include <QApplication>
#include "framedlg.h"
#include <QFontMetrics>
#include "mainwnd.h"
#include "ui_mainwnd.h"

editableRadioButton::editableRadioButton(QWidget *parent, abstractUICCtrl *uiCtrl) :
    QRadioButton(parent)
{    
    underMovement = false;
    this->parentUICtrl = uiCtrl;
    this->setFocusPolicy(Qt::StrongFocus);
}


editableRadioButton::editableRadioButton(const QString & text, QWidget * parent, abstractUICCtrl *uiCtrl):
    QRadioButton(text, parent)
{
    QFontMetrics fm(this->font());
    this->resize(fm.boundingRect(this->text()).width() + 40, fm.boundingRect(this->text()).height());
    underMovement = false;    
    this->parentUICtrl=uiCtrl;
    this->setFocusPolicy(Qt::StrongFocus);
}

void editableRadioButton::setParents(QWidget *parent, abstractUICCtrl *uiCtrl)
{
    setParent(parent);
    this->parentUICtrl = uiCtrl;
}

editableRadioButton::~editableRadioButton()
{

}

int editableRadioButton::cordToGrid(int v)
{
    int ret;
    ret = v / 5;
    ret = ret * 5;
    return ret;
}

void editableRadioButton::mouseReleaseEvent(QMouseEvent *)
{
    underMovement = false;
    this->parentUICtrl->updateAdjPoints();
}

void editableRadioButton::mousePressEvent(QMouseEvent * event)
{
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();
    underMovement = true;
    moveXInit = this->cordToGrid(event->x());
    moveYInit = this->cordToGrid(event->y());

    if(!wnd->isSelected(this->parentUICtrl))
      wnd->addToSelection(this->parentUICtrl);
}

void editableRadioButton::mouseMoveEvent(QMouseEvent * event)
{
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();
    int dx;
    int dy;
    if(underMovement)
    {
        dx = this->cordToGrid(event->x()) - moveXInit;
        dy = this->cordToGrid(event->y()) - moveYInit;
        wnd->moveSelection(dx, dy);
    }
}


void editableRadioButton::moveEvent ( QMoveEvent * event )
{
    QRadioButton::moveEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableRadioButton::resizeEvent ( QResizeEvent * event )
{
    QRadioButton::resizeEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableRadioButton::keyPressEvent(QKeyEvent *e)
{
    if (e->type() == QEvent::KeyPress)
    {
        QKeyEvent* newEvent = new QKeyEvent(QEvent::KeyPress,e->key(), e->modifiers ());
        qApp->postEvent (this->parent(), newEvent, 0);
    }
}
