/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include <QApplication>
#include "framedlg.h"
#include <QFontMetrics>
#include <QPixmap>
#include "editableANano.h"
#include "ardunoprgwnd.h"
#include "ui_ardunoprgwnd.h"
#include "mainwnd.h"
#include "ui_mainwnd.h"

dblClickEventFilter::dblClickEventFilter(QObject *parent):QObject(parent)
{

}

bool dblClickEventFilter::eventFilter(QObject* object,QEvent* event)
{
    if(event->type() == QEvent::MouseButtonDblClick)
    {
        emit dynamic_cast<editableANano *>(object)->doubleClick();
        return true;
    }
    else
       return QObject::eventFilter(object,event);
}

editableANano::editableANano(QWidget *parent, abstractUICCtrl *uiCtrl):QLabel(parent)
{
    int i;
    underMovement = false;
    this->parentUICtrl = uiCtrl;
    this->pixImg = QPixmap(":/img/ANano");
    this->setPixmap(this->pixImg);
    this->setFocusPolicy(Qt::StrongFocus);
    this->installEventFilter(new dblClickEventFilter(this));
    connect(this, SIGNAL(doubleClick()), this, SLOT(onDoubleClick()));
    for(i = 0; i < 32; i++)
    {
      pinType[i] = none;
    }
    pinType[13] = output;
}

void editableANano::setParents(QWidget *parent, abstractUICCtrl *uiCtrl)
{
    setParent(parent);
    this->parentUICtrl = uiCtrl;
}

editableANano::~editableANano()
{

}

int editableANano::cordToGrid(int v)
{
    int ret;
    ret = v / 5;
    ret = ret * 5;
    return ret;
}

void editableANano::mouseReleaseEvent(QMouseEvent *)
{
    underMovement = false;
    this->parentUICtrl->updateAdjPoints();
}

void editableANano::mousePressEvent(QMouseEvent * event)
{
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();
    underMovement = true;
    moveXInit = this->cordToGrid(event->x());
    moveYInit = this->cordToGrid(event->y());

    if(!wnd->isSelected(this->parentUICtrl))
      wnd->addToSelection(this->parentUICtrl);
}

void editableANano::mouseMoveEvent(QMouseEvent * event)
{
    int dx;
    int dy;
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();

    if(underMovement)
    {
        dx = this->cordToGrid(event->x()) - moveXInit;
        dy = this->cordToGrid(event->y()) - moveYInit;
        wnd->moveSelection(dx, dy);
    }
}


void editableANano::moveEvent ( QMoveEvent * event )
{
    QLabel::moveEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableANano::resizeEvent ( QResizeEvent * event )
{    
    QLabel::resizeEvent(event);
    this->setPixmap(this->pixImg.scaled(this->width(),this->height()));
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();

}

void editableANano::keyPressEvent(QKeyEvent *e)
{
    if (e->type() == QEvent::KeyPress)
    {
        QKeyEvent* newEvent = new QKeyEvent(QEvent::KeyPress,e->key(), e->modifiers ());
        qApp->postEvent (this->parent(), newEvent, 0);
    }
}

void editableANano::onDoubleClick(void)
{
    ardUnoPrgWnd prgWnd(this);
    prgWnd.exec();
}
