/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "cmbctrl.h"
#include "editablecmb.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include "wdgitemeditor.h"
#include "ui_wdgitemeditor.h"





//// Implementación de los métodos de los editores de propiedades


cmbItemPropEditor::cmbItemPropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "String";
}

QString cmbItemPropEditor::getValue()
{
    int i;
    editableCmb * cmb;
    QString ret;
    cmb = (editableCmb * )this->ctrl->associatedWidget();
    for(i=0; i < cmb->count(); i++)
    {
        ret = ret + cmb->itemText(i);
        if(i < cmb->count() - 1)
            ret += "|";
    }
    return ret;
}

bool cmbItemPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
  //  QRegularExpression r("^\\[[-+]?\\d{1,10}\\s[-+]?\\d{1,10}\\s\\d{1,10}\\s\\d{1,10}\\]$");
    // return newVal.contains(r) == 1;
    return true;
}

void cmbItemPropEditor::setValue(QString newVal)
{
    QStringList v;
    editableCmb * cmb = (editableCmb * )this->ctrl->associatedWidget();
    int i;
    if(this->isValidValue(newVal))
    {
        cmb->clear();
        v = newVal.split("|");
        for(i=0; i<v.count(); i++)
            cmb->addItem(v[i]);
    }
}

QString cmbItemPropEditor::generateCode()
{
    QString ret;
    ret = "'String', '" + this->getValue() + "'";
    return ret;
}


QWidget * cmbItemPropEditor::getEditWidget()
{
    wdgItemEditor * wdgId = new wdgItemEditor(this);
    wdgId->setItems(this->getValue());
    connect(wdgId->getLEItems(), SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
    return wdgId;
}

void cmbItemPropEditor::valueItemChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {
        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
}

cmbBgClrPropEditor::cmbBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
}

QString cmbBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Button).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).blueF(), 'f', 3) + "]";
    return ret;
}

bool cmbBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void cmbBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::Button, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString cmbBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * cmbBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void cmbBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

cmbFgClrPropEditor::cmbFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString cmbFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::ButtonText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::ButtonText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::ButtonText).blueF(), 'f', 3) + "]";
    return ret;
}

bool cmbFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void cmbFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::ButtonText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString cmbFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * cmbFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void cmbFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int cmbCtrl::uicNameCounter = 0;

cmbCtrl::cmbCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableCmb * cmb = new editableCmb(parent, this);
    QFontMetrics fm(cmb->font());
    uicNameCounter++;
    this->setCtrlName("ComboBox_"+QString::number(uicNameCounter));
    cmb->setEditText(this->getCtrlName());
    cmb->resize(fm.width(this->getCtrlName()), 25);
    cmb->addItem("Value A");
    cmb->addItem("Value B");
    cmb->addItem("Value C");
    this->setAssociatedWidget(cmb);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());


    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new cmbItemPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new cmbFgClrPropEditor(this));
    this->properties.registerProp(new cmbBgClrPropEditor(this));
}

void cmbCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableCmb * cmb = dynamic_cast<editableCmb *>(this->associatedWidget());
    cmb->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList cmbCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','popupmenu'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList cmbCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

bool cmbCtrl::isAutoSize()
{
    return true;
}

cmbCtrl::~cmbCtrl()
{
  delete this->getAssociatedWidget();
}


/// Implementación de la clase generadora
///



cmbCtrlGen::cmbCtrlGen():controlGenerator("cmbCtrl")
{
}

abstractUICCtrl * cmbCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    cmbCtrl * cmb = new cmbCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = cmb->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = cmb->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      cmb->setSrcCallBack(src.split('\n'));

    cmb->deselect();
    return cmb;

}
