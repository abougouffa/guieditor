/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef SLIDERCTRL_H
#define SLIDERCTRL_H
#include "abstractuicctrl.h"
#include <QScrollBar>
#include <QWidget>
#include <QLineEdit>
#include "controlgenerator.h"

class valueSliderPropEditor: public abstractPropEditor
{
public:
    valueSliderPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "Value";
    }
    QString getValue(){

        return QString::number(((QScrollBar *)this->ctrl->associatedWidget())->value());
    }

    bool isValidValue(QString newVal){
        bool ret;
        int v;
        v = newVal.toInt(&ret);
        if(ret)
            ret = (v <= ((QScrollBar *)this->ctrl->associatedWidget())->maximum()) &&
                  (v >= ((QScrollBar *)this->ctrl->associatedWidget())->minimum());
        return ret;
    }
    virtual void setValue(QString newVal)
    {
        ((QScrollBar *)this->ctrl->associatedWidget())->setValue(newVal.toInt());
    }

    virtual bool canAgroup(){ return true;}
    virtual QString generateCode()
    {
        return "'" + this->propName + "', " + this->getValue();
    }
};

class maxSliderPropEditor: public abstractPropEditor
{
public:
    maxSliderPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "Max";
    }
    QString getValue(){

        return QString::number(((QScrollBar *)this->ctrl->associatedWidget())->maximum());
    }

    bool isValidValue(QString newVal){
        bool ret;
        newVal.toInt(&ret);
        return ret;
    }
    virtual void setValue(QString newVal)
    {
        ((QScrollBar *)this->ctrl->associatedWidget())->setMaximum(newVal.toInt());
    }

    virtual bool canAgroup(){ return true;}
    virtual QString generateCode()
    {
        return "'" + this->propName + "', " + this->getValue();
    }
};


class minSliderPropEditor: public abstractPropEditor
{
public:
    minSliderPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "Min";
    }
    QString getValue(){

        return QString::number(((QScrollBar *)this->ctrl->associatedWidget())->minimum());
    }

    bool isValidValue(QString newVal){
        bool ret;
        newVal.toInt(&ret);
        return ret;
    }
    virtual void setValue(QString newVal)
    {
        ((QScrollBar *)this->ctrl->associatedWidget())->setMinimum(newVal.toInt());
    }

    virtual bool canAgroup(){ return true;}
    virtual QString generateCode()
    {
        return "'" + this->propName + "', " + this->getValue();
    }
};

class sldBgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    sldBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class sldFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    sldFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class sliderCtrl : public abstractUICCtrl
{
  private:
    static unsigned int uicNameCounter;

  protected:
  public:
    static unsigned int getNameCounter();
    sliderCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual QString className() { return "sliderCtrl";}
    virtual ~sliderCtrl();
};

class sliderCtrlGen: public controlGenerator
{
public:
  sliderCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // SLIDERCTRL_H
