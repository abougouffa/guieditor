/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "sliderctrl.h"
#include "editableslider.h"
#include "commonproperties.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"

sldBgClrPropEditor::sldBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
}

QString sldBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Window).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).blueF(), 'f', 3) + "]";
    return ret;
}

bool sldBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void sldBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::Window, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString sldBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * sldBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void sldBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

sldFgClrPropEditor::sldFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString sldFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::WindowText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).blueF(), 'f', 3) + "]";
    return ret;
}

bool sldFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void sldFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::WindowText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString sldFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * sldFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void sldFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int sliderCtrl::uicNameCounter = 0;

unsigned int sliderCtrl::getNameCounter()
{
    return uicNameCounter;
}

sliderCtrl::sliderCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd)
    :abstractUICCtrl(parent, octaveParent, parentWnd)
{
    uicNameCounter++;
    editableSlider* sld = new editableSlider(parent, this);
    this->setAssociatedWidget(sld);
    this->setCtrlName("Slider_"+QString::number(uicNameCounter));
    sld->resize(60, 25);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));

    this->properties.registerProp(new valueSliderPropEditor(this));
    this->properties.registerProp(new maxSliderPropEditor(this));
    this->properties.registerProp(new minSliderPropEditor(this));
    this->properties.registerProp(new sldBgClrPropEditor(this));
    this->properties.registerProp(new sldFgClrPropEditor(this));

    this->srcCallBack.append(QObject::tr("% This code will be executed when user change the value of slider."));
    this->srcCallBack.append(QObject::tr("% As default, all events are deactivated, to activate must set the"));
    this->srcCallBack.append(QObject::tr("% property 'generateCallbck' from the properties editor"));
}

void sliderCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableSlider * slider = dynamic_cast<editableSlider *>(this->associatedWidget());
    slider->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList sliderCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','slider'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList sliderCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

sliderCtrl::~sliderCtrl()
{
    delete this->getAssociatedWidget();
}

sliderCtrlGen::sliderCtrlGen():controlGenerator("sliderCtrl")
{
}

abstractUICCtrl * sliderCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    sliderCtrl * sld = new sliderCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = sld->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = sld->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      sld->setSrcCallBack(src.split('\n'));
    sld->deselect();
    return sld;
}


