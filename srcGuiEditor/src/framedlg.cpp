/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "framedlg.h"
#include <QMouseEvent>
#include <QPen>
#include <QPainter>
#include "mainwnd.h"
#include <QApplication>
#include <cmath>
#include <QApplication>
#include <QDesktopWidget>
#include <QRegExp>
#include "commonproperties.h"
#include "wdgclrchooser.h"
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include "QTextStream"
#include "childwnddlg.h"
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QHeaderView>
#include <QPushButton>
#include <QRadioButton>

#define defaultUICWidth 90
#define defaultUICHeight 22

unsigned int frameDlg::formNumber = 0;

wndResizePropEditor::wndResizePropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "Resize";
    this->resizeValue = "off";
}
QString wndResizePropEditor::getValue()
{
    return this->resizeValue;
}

QString wndResizePropEditor::generateCode()
{
    QString ret;
    ret = "'resize', '";
    ret += this->resizeValue + "'";
    return ret;
}

bool wndResizePropEditor::isValidValue(QString newVal)
{
    return (newVal=="on") || (newVal=="off");
}

void wndResizePropEditor::setValue(QString newVal)
{
    if(this->isValidValue(newVal))
        this->resizeValue = newVal;
}

QWidget * wndResizePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("on");
    cb->addItem("off");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    return cb;
}

void wndResizePropEditor::valueChooserChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    }
}


wndPositionPropEditor::wndPositionPropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "Position";
}

QString wndPositionPropEditor::getValue()
{
    int screenHeight = QApplication::desktop()->screenGeometry().height();
    QString ret;
    childWndDlg * dlg;
    dlg = this->ctrl->getParentWnd();
    ret = "[";
    ret += QString::number(dlg->x()) + " ";
    ret += QString::number(screenHeight - dlg->y() - dlg->height()) + " ";
    ret += QString::number(dlg->width()) + " ";
    ret += QString::number(dlg->height()) + "]";
    return ret;
}

bool wndPositionPropEditor::isValidValue(QString newVal)
{
    QRegExp r("^\\[[-+]?\\d{1,10}\\s[-+]?\\d{1,10}\\s\\d{1,10}\\s\\d{1,10}\\]$");
    return newVal.contains(r) == 1;
}

void wndPositionPropEditor::setValue(QString newVal)
{
    QStringList v;
    childWndDlg * dlg;
    //int screenHeight = QApplication::desktop()->screenGeometry().height()-100;
    //int screenHeight = QApplication::desktop()->availableGeometry().height() - 76;

    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        dlg = this->ctrl->getParentWnd();
        //dlg->move(v[0].toInt(), screenHeight - v[1].toInt());
        dlg->resize(v[2].toInt(), v[3].toInt());
    }
}

QString wndPositionPropEditor::generateCode()
{
    QString ret;
    ret = "'Position', [";
    ret += "_xPos ";
    ret += "_yPos ";
    ret += QString::number(this->ctrl->associatedWidget()->width()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->height()) + "]";
    return ret;
}


frameBgClrPropEditor::frameBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "Color";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString frameBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Window).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).blueF(), 'f', 3) + "]";
    return ret;
}

bool frameBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
  //  QRegularExpression r("^\\[[-+]?\\d{1,10}\\s[-+]?\\d{1,10}\\s\\d{1,10}\\s\\d{1,10}\\]$");
    // return newVal.contains(r) == 1;
    return true;
}

void frameBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Window, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}
QString frameBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}

QWidget * frameBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void frameBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), 0, 0);
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

windowStylePropEditor::windowStylePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "windowStyle";
    this->windowStyleValue = "normal";
}

QString windowStylePropEditor::getValue()
{
    return this->windowStyleValue;
}

bool windowStylePropEditor::isValidValue(QString newVal)
{
    return (newVal == "normal") || (newVal == "docked") || (newVal == "modal");
}

void windowStylePropEditor::setValue(QString newVal)
{
    if(isValidValue(newVal))
        this->windowStyleValue = newVal;
}

QString windowStylePropEditor::generateCode()
{
    QString ret;
    ret = "'windowstyle', '";
    ret += this->windowStyleValue;
    ret += "'";
    return ret;
}

QWidget * windowStylePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("docked");
    cb->addItem("modal");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    return cb;
}

void windowStylePropEditor::valueChooserChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    }
}


////////////
//enum hDlgPos {leftHPos, rightHPos, centerHPos, defaultHPos};
hAlignDlgPropEditor::hAlignDlgPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
  this->propName = "HPosition";
  this->hPos = centerHPos;
}

QString hAlignDlgPropEditor::getValue()
{
    QString ret;
    switch(this->hPos)
    {
        case leftHPos:
          ret = "left";
          break;
        case rightHPos:
          ret = "right";
          break;
        case centerHPos:
          ret = "center";
          break;
        case defaultHPos:
          ret = "default";
          break;
    }
    return ret;
}

bool hAlignDlgPropEditor::isValidValue(QString newVal)
{
    bool ret = ((newVal == "left") || (newVal == "right") || (newVal == "center") || (newVal == "default"));
    return ret;
}

void hAlignDlgPropEditor::setValue(QString newVal)
{
    if(newVal == "left")
        this->hPos = leftHPos;
    else if(newVal == "right")
        this->hPos = rightHPos;
    else if(newVal == "center")
        this->hPos = centerHPos;
    else if(newVal == "default")
        this->hPos = defaultHPos;
}

QWidget * hAlignDlgPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("default");
    cb->addItem("left");
    cb->addItem("center");
    cb->addItem("right");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    return cb;
}

void hAlignDlgPropEditor::valueChooserChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    }
}

//enum vDlgPos {topVPos, bottomVPos, centerVPos, defaultVPos};
//class vAlignDlgPropEditor: public abstractPropEditor

vAlignDlgPropEditor::vAlignDlgPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
  this->vPos = centerVPos;
  this->propName = "VPosition";
}

QString vAlignDlgPropEditor::getValue()
{
    QString ret;
    switch(this->vPos)
    {
        case topVPos:
          ret = "top";
          break;
        case bottomVPos:
          ret = "bottom";
          break;
        case centerVPos:
          ret = "center";
          break;
        case defaultVPos:
          ret = "default";
          break;
    }
    return ret;
}

bool vAlignDlgPropEditor::isValidValue(QString newVal)
{
    bool ret = ((newVal == "top") ||
                (newVal == "bottom") ||
                (newVal == "center") ||
                (newVal == "default"));
    return ret;
}

void vAlignDlgPropEditor::setValue(QString newVal)
{
    if(newVal == "top")
        this->vPos = topVPos;
    else if(newVal == "bottom")
        this->vPos = bottomVPos;
    else if(newVal == "center")
        this->vPos = centerVPos;
    else if(newVal == "default")
        this->vPos = defaultVPos;
}

QWidget * vAlignDlgPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("default");
    cb->addItem("top");
    cb->addItem("center");
    cb->addItem("bottom");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    return cb;
}

void vAlignDlgPropEditor::valueChooserChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    }
}


////////////

void frameDlg::addWidget(abstractUICCtrl *w){
    /* Resolve exclusive radio button and push button implementation*/
    if (w->className() == "radioButtonCtrl")
        ((QRadioButton *)w->associatedWidget())->setAutoExclusive(false);
    else if(w->className() == "toggleCtrl")
        ((QPushButton *)w->associatedWidget())->setAutoExclusive(false);
    abstractUICCtrl::addWidget(w);
}

int frameDlg::cordToGrid(int v)
{
    int ret;
    ret = v / 5;
    ret = ret * 5;
    return ret;
}

frameDlg::frameDlg(QWidget *parent,
        childWndDlg *dlg) :
    QWidget(parent),
    abstractUICCtrl(parent, NULL, dlg)
{    
    formNumber ++;
    this->setAssociatedWidget(this);
    this->vIsContainer = true;


    this->setCtrlName("Dialog_"+QString::number(formNumber));


    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());

    this->deselect();

    this->properties.registerProp(new wndPositionPropEditor(this));
    this->properties.registerProp(new frameBgClrPropEditor(this));
    this->properties.registerProp(new vAlignDlgPropEditor(this));
    this->properties.registerProp(new hAlignDlgPropEditor(this));
    this->properties.registerProp(new wndResizePropEditor(this));
    this->properties.registerProp(new windowStylePropEditor(this));
    this->addedWidgets.push_back(this);

    this->getParentWnd()->getSrcWnd()->updateWidgetList(this->addedWidgets);
    this->setAutoFillBackground(true);


    this->srcCallBack.append(tr("%"));
    this->srcCallBack.append(tr("% The source code written here will be executed when"));
    this->srcCallBack.append(tr("% windows load. Work like 'onLoad' event of other languages."));
    this->srcCallBack.append(tr("%"));
    this->srcCallBack.append(tr(""));

    this->setWindowIcon(QPixmap(":/img/img/icono.svg"));
}

void frameDlg::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    this->setParent(parent);
    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);
}

unsigned int frameDlg::getFormNumber()
{
    return formNumber;
}

frameDlg::~frameDlg()
{
    QList<abstractUICCtrl *>::Iterator w;
    this->getParentWnd()->getSrcWnd()->closeChildWnd();

    mainWnd::getPropPan()->getTblProp()->setRowCount(0);
    mainWnd::getPropPan()->getTblProp()->verticalHeader()->hide();
    for(w = addedWidgets.begin(); w != addedWidgets.end(); ++w)
        if((*w) != this)
          delete (*w);
    addedWidgets.clear();
}

QStringList frameDlg::generateMFile(QString path)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    QString guiCtrlName;
    abstractPropEditor * prop;
    QStringList ret;    
    int i;
    QStringList controlList;
    QString callBackDef;

    int dlgWidth = this->width();
    int dlgHeight = this->height();
    int screenHeight = QApplication::desktop()->screenGeometry().height();

    ret.append("## -*- texinfo -*-");
    ret.append("## @deftypefn  {} {} dummy()");
    ret.append("##");
    ret.append("## This is a dummy function documentation. This file have a lot functions");
    ret.append("## and each one have a little documentation. This text is to avoid a warning when");
    ret.append("## install this file as part of package.");
    ret.append("## @end deftypefn");
    ret.append("##");
    ret.append("## Set the graphics toolkit and force read this file as script file (not a function file).");
    ret.append("##");
    ret.append("graphics_toolkit qt;");
    ret.append("##");
    ret.append("");
    ret.append("");
    ret << "##" << "##" << "## Begin callbacks definitions " <<  "##" << "";
    for(ctrl  = this->addedWidgets.begin();
        ctrl != this->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= this)
        {
            if((*ctrl)->haveCallBack())
                ret.append((*ctrl)->createCallBack(path));
            if((*ctrl)->isContainer())
                createCallBackRecursive(ret, path, *ctrl);
        }
    }
    ret << " ";

    ret.append("## @deftypefn  {} {@var{ret} = } show_" +  this->properties.getValueByName("Name") + "()");
    ret.append("##");
    ret.append("## Create windows controls over a figure, link controls with callbacks and return ");
    ret.append("## a window struct representation.");
    ret.append("##");
    ret.append("## @end deftypefn");

    ret.append("function ret = show_" + this->properties.getValueByName("Name") + "()");
    ret.append("  _scrSize = get(0, \"screensize\");");

    if(this->properties.getValueByName("HPosition") == "center")
        ret.append("  _xPos = (_scrSize(3) - " + QString::number(dlgWidth) + ")/2;");
    else if(this->properties.getValueByName("HPosition") == "left")
        ret.append("  _xPos = 1;");
    else if(this->properties.getValueByName("HPosition") == "right")
        ret.append("  _xPos = (_scrSize(3) - " + QString::number(dlgWidth) + ");");
    else if(this->properties.getValueByName("HPosition") == "default")
        ret.append("  _xPos = " + QString::number(this->x()) + ";");

    if(this->properties.getValueByName("VPosition") == "center")
        ret.append("  _yPos = (_scrSize(4) - " + QString::number(dlgHeight) + ")/2;");
    else if(this->properties.getValueByName("VPosition") == "top")
        ret.append("  _yPos = " + QString::number(screenHeight - this->height() - 30) + ";");
    else if(this->properties.getValueByName("VPosition") == "bottom")
        ret.append("  _yPos = 1;");
    else if(this->properties.getValueByName("VPosition") == "default")
        ret.append("  _yPos = " + QString::number(screenHeight - this->y() - this->height()) + ";");

    ret.append("   " + this->properties.getValueByName("Name") + " = figure ( ... ");

    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
          ret.append("\t" + prop->generateCode() + ", ...");
        prop = this->properties.getNext();
    }

    ret.append("\t'MenuBar', 'none');");

    /* Hide figure while is loading */
    ret.append("\t set(" + this->properties.getValueByName("Name") + ", 'visible', 'off');");
    /* */

    for(ctrl  = this->addedWidgets.begin();
        ctrl != this->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= this)
        {          
          ret << (*ctrl)->generateMFile(path);
          if((*ctrl)->getCtrlName() != this->getCtrlName())
            controlList.append((*ctrl)->getCtrlName());

         // if((*ctrl)->isContainer())
         //     generateMFileRecursive(path, ret, *ctrl, controlList);
         if((*ctrl)->isContainer())
             generateControlList(*ctrl, controlList);
        }
    }

    ret.append("");

    ret.append("  " + this->properties.getValueByName("Name") + " = struct( ...");
    if(this->addedWidgets.count() == 1)
    {
        ret.append("      'figure', " + this->properties.getValueByName("Name") + ");");
    }
    else
    {
        ret.append("      'figure', " + this->properties.getValueByName("Name") + ", ...");
    }
    for(i  = 0; i < controlList.count(); i++)
        {
          guiCtrlName = controlList[i];
          if(i+1 != controlList.count())
             ret.append("      '" + guiCtrlName + "', " + guiCtrlName + ", ...");
          else
             ret.append("      '" + guiCtrlName + "', " + guiCtrlName + ");");
        }
        ret.append("");


        ret.append("");

    for(ctrl  = this->addedWidgets.begin();
        ctrl != this->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= this)
        {
            if((*ctrl)->haveCallBack() && (*ctrl)->mustDeclareCallBack())
            {
                callBackDef = "  set (" + (*ctrl)->properties.getValueByName("Name") +
                        ", 'callback', {@" + (*ctrl)->properties.getValueByName("Name") + "_doIt, " + this->properties.getValueByName("Name") + "});";
                ret.append(callBackDef);
            }
            if((*ctrl)->isContainer())
                generateCallBackRecursive(ret, *ctrl, this->properties.getValueByName("Name"));
        }
    }

    ret.append("  dlg = struct(" + this->properties.getValueByName("Name") + ");" );

    ret.append("");
    ret.append("  set(" + this->properties.getValueByName("Name") + ".figure, 'visible', 'on');");
    ret.append("");
    for(int i=0; i < this->srcCallBack.count(); i++)
        ret.append(this->srcCallBack[i]);


    ret.append("  ret = " + this->properties.getValueByName("Name") + ";");

    ret << "end";
    ret.append("");
    return ret;
}

void frameDlg::createCallBackRecursive(QStringList &ret, QString path, abstractUICCtrl *cnt)
{
    QList<abstractUICCtrl *>::iterator ctrl;

    for(ctrl  = cnt->addedWidgets.begin();
        ctrl != cnt->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= this)
        {
            if((*ctrl)->haveCallBack())
                ret.append((*ctrl)->createCallBack(path));
            if((*ctrl)->isContainer())
                createCallBackRecursive(ret, path, *ctrl);
        }
    }
}

void frameDlg::generateControlList(abstractUICCtrl *wdg, QStringList & controlList)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = wdg->addedWidgets.begin();
        ctrl != wdg->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= wdg)
        {
          if((*ctrl)->getCtrlName() != this->getCtrlName())
            controlList.append((*ctrl)->getCtrlName());
          if((*ctrl)->isContainer())
            generateControlList(*ctrl, controlList);
        }
    }
}

void frameDlg::generateMFileRecursive(QString path, QStringList &ret, abstractUICCtrl *wdg, QStringList & controlList)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = wdg->addedWidgets.begin();
        ctrl != wdg->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= wdg)
        {
          ret << (*ctrl)->generateMFile(path);
          if((*ctrl)->getCtrlName() != this->getCtrlName())
            controlList.append((*ctrl)->getCtrlName());
          if((*ctrl)->isContainer())
            generateMFileRecursive(path, ret, *ctrl, controlList);
        }
    }

}

void frameDlg::generateCallBackRecursive(QStringList &ret, abstractUICCtrl *wdg, QString dialogName)
{
    QString callBackDef;
    QList<abstractUICCtrl *>::Iterator ctrl;

    for(ctrl  = wdg->addedWidgets.begin();
        ctrl != wdg->addedWidgets.end();
        ++ctrl)
      {
        if((*ctrl)!= wdg)
        {
            if((*ctrl)->haveCallBack() && (*ctrl)->mustDeclareCallBack())
            {
                callBackDef = "set (" + (*ctrl)->properties.getValueByName("Name") +
                        ", 'callback', {@" + (*ctrl)->properties.getValueByName("Name") + "_doIt, " + dialogName + "});";
                ret.append(callBackDef);
            }
            if((*ctrl)->isContainer())
              generateCallBackRecursive(ret, *ctrl, dialogName);
        }
      }

}

void frameDlg::updateSrcWndCtrls()
{
    this->getParentWnd()->getSrcWnd()->updateWidgetList(this->addedWidgets, true);
}

QStringList frameDlg::createCallBack(QString path __attribute__((unused)))
{
  QStringList ret;
  return ret;
}


void frameDlg::mousePressEvent(QMouseEvent * event)
{
    if((event->x()>5) &&
            (event->x() < this->width() - 10) &&
            (event->y() > 5) &&
            (event->y() < this->height() - 10) &&
            event->button() == Qt::LeftButton)
    {
        this->getParentWnd()->setSelXInit(cordToGrid(event->x()));
        this->getParentWnd()->setSelYInit(cordToGrid(event->y()));
        this->getParentWnd()->setSelWidth(0);
        this->getParentWnd()->setSelHeight(0);
        this->getParentWnd()->setSelectionState(selectionStarted);
    }    
    QWidget::mousePressEvent(event);
}

void frameDlg::mouseReleaseEvent(QMouseEvent * event)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    QRect selectionGeometry;
    this->getParentWnd()->setSelWidth(cordToGrid(event->x()) - this->getParentWnd()->getSelXInit());
    this->getParentWnd()->setSelHeight(cordToGrid(event->y())  - this->getParentWnd()->getSelYInit());

    if(this->getParentWnd()->getSelWidth() < 0)
    {
        this->getParentWnd()->setSelXInit(this->getParentWnd()->getSelXInit() + this->getParentWnd()->getSelWidth());
        this->getParentWnd()->setSelWidth(this->getParentWnd()->getSelWidth() * -1);
    }
    if(this->getParentWnd()->getSelHeight() < 0)
    {
        this->getParentWnd()->setSelYInit(this->getParentWnd()->getSelYInit() + this->getParentWnd()->getSelHeight());
        this->getParentWnd()->setSelHeight(this->getParentWnd()->getSelHeight() * -1);
    }
    if(mainWnd::getMainWnd()->uicState == uicStateAddWidget)
    {
        if((this->getParentWnd()->getSelHeight() == 0) || (this->getParentWnd()->getSelWidth() == 0))
        {
            this->getParentWnd()->setSelHeight(defaultUICHeight);
            this->getParentWnd()->setSelWidth(defaultUICWidth);
        }
        mainWnd::getMainWnd()->uicState = uicStateNone;
        /* Name validation */
        QString baseName = mainWnd::getMainWnd()->toAdd->getCtrlName();
        QString tmpName = baseName;
        int nameCnt = 1;
        while(haveCtrlWithName(tmpName, this))
        {
            tmpName = baseName + QString::number(nameCnt);
            nameCnt++;
        }
        mainWnd::getMainWnd()->toAdd->setCtrlName(tmpName);
        /* Resolve exclusive radio button and push button implementation*/
        if (mainWnd::getMainWnd()->toAdd->className() == "radioButtonCtrl")
            ((QRadioButton *)mainWnd::getMainWnd()->toAdd->associatedWidget())->setAutoExclusive(false);
        else if(mainWnd::getMainWnd()->toAdd->className() == "toggleCtrl")
            ((QPushButton *)mainWnd::getMainWnd()->toAdd->associatedWidget())->setAutoExclusive(false);

        this->addWidget(mainWnd::getMainWnd()->toAdd);
        if(!mainWnd::getMainWnd()->toAdd->isAutoSize())
          mainWnd::getMainWnd()->toAdd->associatedWidget()->setGeometry(
                    this->getParentWnd()->getSelXInit(),
                    this->getParentWnd()->getSelYInit(),
                    this->getParentWnd()->getSelWidth(),
                    this->getParentWnd()->getSelHeight());
        else
          mainWnd::getMainWnd()->toAdd->associatedWidget()->move(
                    this->getParentWnd()->getSelXInit(),
                    this->getParentWnd()->getSelYInit());
        mainWnd::getMainWnd()->toAdd->setParents(this, this, this->getParentWnd());
        mainWnd::getMainWnd()->toAdd->associatedWidget()->show();
        mainWnd::getMainWnd()->toAdd = NULL;
        this->getParentWnd()->setWindowModified(true);
        this->getParentWnd()->setSelectionState(selectionEnd);
        QWidget::update();
    }
    else if(this->getParentWnd()->selectionState() == selectionStarted)
    {
        this->getParentWnd()->setSelectionState(selectionEnd);
        QWidget::update();

        selectionGeometry.setX(this->getParentWnd()->getSelXInit());
        selectionGeometry.setY(this->getParentWnd()->getSelYInit());
        selectionGeometry.setWidth(this->getParentWnd()->getSelWidth());
        selectionGeometry.setHeight(this->getParentWnd()->getSelHeight());
        if(!(QApplication::keyboardModifiers() & Qt::ShiftModifier))
            this->getParentWnd()->deselectAllUIC();
        for(ctrl  = this->addedWidgets.begin();
            ctrl != this->addedWidgets.end();
            ++ctrl)
            if((*ctrl) != this)
            {
              if((*ctrl)->position().intersects(selectionGeometry))
              {
                (*ctrl)->select();
                this->getParentWnd()->selectedWidgets.push_back(*ctrl);
              }
            }
        this->getParentWnd()->selectionChanged();
    }
    QWidget::mouseReleaseEvent(event);
}

void frameDlg::mouseMoveEvent(QMouseEvent * event)
{
    if(this->getParentWnd()->selectionState() == selectionStarted)
    {
        this->getParentWnd()->setSelWidth(cordToGrid(event->x()) - this->getParentWnd()->getSelXInit());
        this->getParentWnd()->setSelHeight(cordToGrid(event->y()) - this->getParentWnd()->getSelYInit());
        QWidget::update();
    }
    QWidget::mouseMoveEvent(event);
}

void frameDlg::paintEvent(QPaintEvent *event)
{  
    QWidget::paintEvent(event);
    QPen lapiz(Qt::black);
    QBrush relleno(Qt::white);
    QPainter painter(this);
    lapiz.setWidth(1);
    lapiz.setStyle(Qt::DashLine);
    relleno.setStyle(Qt::NoBrush);
    painter.setPen(lapiz);
    painter.setBrush(relleno);

    int xIni = cordToGrid(event->rect().x());
    int yIni = cordToGrid(event->rect().y());
    for(int x = xIni; x < event->rect().width();x += 5)
      for(int y = yIni; y < event->rect().height(); y += 5)
          painter.drawPoint(x, y);

    if(this->getParentWnd()->selectionState() == selectionStarted)
        painter.drawRect(
                    this->getParentWnd()->getSelXInit(),
                    this->getParentWnd()->getSelYInit(),
                    this->getParentWnd()->getSelWidth(),
                    this->getParentWnd()->getSelHeight());
    painter.end();

}

/* Este método busca recursivamente sobre las listas de controles contenidos
 * un control particular y lo elimina de la lista (no al control) la eliminación del
 * control se realiza desde "deleteSelection".
 */

void frameDlg::removeControlFromList(abstractUICCtrl * ctrl, abstractUICCtrl* toDrop)
{
    QList<abstractUICCtrl *>::iterator it;
    for(it = ctrl->addedWidgets.begin(); it != ctrl->addedWidgets.end(); it++)
    {
        if((*it) == toDrop)
        {
            ctrl->addedWidgets.removeOne(toDrop);
            return;
        }
        else if((*it)->isContainer() && (*it)->className() != "frameDlg")
            this->removeControlFromList(*it, toDrop);
    }
}

void frameDlg::keyPressEvent(QKeyEvent * event )
{
    QList<abstractUICCtrl *>::iterator toDrop;
    if(event->key()==Qt::Key_Escape)
    {
        this->getParentWnd()->deselectAllUIC();
        event->accept();
    }
    else if(event->key()==Qt::Key_Delete)
    {
        /* Invocamos a este método a fin de elminar cualquier referencia a los
           widget, así no hay problema con los que se elminen*/
        // El problema se da porque un control que tiene un conjunto de subcontroles,
        // no elimina de su lista de controles contenidos los que son eliminados por
        // el "deleteSelection".
        this->getParentWnd()->getSrcWnd()->deleteControls();
        for( toDrop = this->getParentWnd()->selectedWidgets.begin();
            toDrop != this->getParentWnd()->selectedWidgets.end();
            toDrop++)
            removeControlFromList(this, *toDrop);
        this->getParentWnd()->deleteSelection();
        this->getParentWnd()->getSrcWnd()->updateWidgetList(this->addedWidgets, true);
        this->getParentWnd()->setWindowModified(true);
        event->accept();
    }
    else if(event->key()==Qt::Key_Up)
    {
        this->getParentWnd()->moveSelection(0, -5);
        this->getParentWnd()->setWindowModified(true);
        event->accept();
    }
    else if(event->key()==Qt::Key_Down)
    {
        this->getParentWnd()->moveSelection(0, 5);
        this->getParentWnd()->setWindowModified(true);
        event->accept();
    }
    else if(event->key()==Qt::Key_Left)
    {        
        this->getParentWnd()->moveSelection(-5, 0);
        this->getParentWnd()->setWindowModified(true);
        event->accept();
    }
    else if(event->key()==Qt::Key_Right)
    {
        this->getParentWnd()->setWindowModified(true);
        this->getParentWnd()->moveSelection(5, 0);
        event->accept();
    }
}


void frameDlg::moveEvent ( QMoveEvent * event )
{
    QWidget::moveEvent(event);
    this->getParentWnd()->selectionChanged();
}

void frameDlg::resizeEvent ( QResizeEvent * event )
{
    QWidget::resizeEvent(event);
    this->getParentWnd()->selectionChanged();
    this->getParentWnd()->setWindowModified(true);
}

