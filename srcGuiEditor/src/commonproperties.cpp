/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "commonproperties.h"
#include <QRegExp>
#include <QWidget>
#include <QFontDatabase>
#include "mainwnd.h"
#include "wdgclrchooser.h"

positionPropEditor::positionPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "Position";
}

QString positionPropEditor::getValue()
{
    QString ret;
    ret = "[";
    ret += QString::number(this->ctrl->associatedWidget()->x()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->y()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->width()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->height()) + "]";
    return ret;
}

bool positionPropEditor::isValidValue(QString newVal)
{
    QRegExp r("^\\[[-+]?\\d{1,10}\\s[-+]?\\d{1,10}\\s\\d{1,10}\\s\\d{1,10}\\]$");
    return newVal.contains(r) == 1;
}

void positionPropEditor::setValue(QString newVal)
{
    QStringList v;
    int w, h;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        this->ctrl->associatedWidget()->move(v[0].toInt(), v[1].toInt());
        w = v[2].toInt();
        h = v[3].toInt();
        this->ctrl->associatedWidget()->resize(w, h);
    }
}


QString positionPropEditor::generateCode()
{
    QString ret;
    ret = "'Position', [";
    ret += QString::number(this->ctrl->associatedWidget()->x()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->parentWidget()->height()  - this->ctrl->associatedWidget()->y() - this->ctrl->associatedWidget()->height()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->width()) + " ";
    ret += QString::number(this->ctrl->associatedWidget()->height()) + "]";
    return ret;
}

//////////////////////////////////////////////////////////////////////////

fontNamePropEditor::fontNamePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "FontName";
    this->ctrl->associatedWidget()->setFont(mainWnd::getMainWnd()->getAppConfig()->getCtrlFont() );
}

QString fontNamePropEditor::getValue()
{
    return this->ctrl->associatedWidget()->font().family();
}

bool fontNamePropEditor::isValidValue(QString newVal)
{
    return fontDB.families().contains(newVal);
}

void fontNamePropEditor::setValue(QString newVal)
{
    QFont font;
    font = this->ctrl->associatedWidget()->font();
    font.setFamily(newVal);
    this->ctrl->associatedWidget()->setFont(font);
}

QString fontNamePropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * fontNamePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItems(fontDB.families());
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
    return cb;
}


void fontNamePropEditor::fontChanged(const QString & txt)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(txt))
        this->setValue(txt);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
    }
}

fontSizePropEditor::fontSizePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "FontSize";
}

QString fontSizePropEditor::getValue()
{
    return QString::number(this->ctrl->associatedWidget()->font().pointSize());
}

bool fontSizePropEditor::isValidValue(QString newVal)
{
    bool Ok;
    double dVal;
    dVal = newVal.toInt(&Ok);
    return (dVal > 0) && Ok;
}

void fontSizePropEditor::setValue(QString newVal)
{
    QFont font = this->ctrl->associatedWidget()->font();
    font.setPointSize(newVal.toInt());
    this->ctrl->associatedWidget()->setFont(font);
}

QString fontSizePropEditor::generateCode()
{
    QString ret;

    ret = "'" + this->propName + "', " + this->getValue() + ", ";
    ret += "'FontUnits', 'points'";
    return ret;
}

QWidget * fontSizePropEditor::getEditWidget()
{
    spinUpdPropWdg * sb = new spinUpdPropWdg(this);
    sb->setAlignment(Qt::AlignRight);
    sb->setMinimum(1);
    sb->setMaximum(5000);
    sb->setSingleStep(1);
    sb->setValue(this->getValue().toInt());
    connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
    return sb;
}

void fontSizePropEditor::fontSizeChanged(const QString &text)
{
    spinUpdPropWdg * sb = (spinUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
        this->setValue(text);
    }
    else
    {
        disconnect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
        sb->setValue(this->getValue().toInt());
        connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
    }
}

fontWeightPropEditor::fontWeightPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "FontWeight";
}

QString fontWeightPropEditor::getValue()
{
    QString ret;
    ret = "normal";
    if(this->ctrl->associatedWidget()->font().bold())
        ret = "bold";
    return ret;
}

bool fontWeightPropEditor::isValidValue(QString newVal)
{
    return (newVal == "bold") || (newVal == "normal");
}

void fontWeightPropEditor::setValue(QString newVal)
{
    QFont font = this->ctrl->associatedWidget()->font();
    font.setBold(newVal == "bold");
    this->ctrl->associatedWidget()->setFont(font);
}

QString fontWeightPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * fontWeightPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("bold");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
    return cb;
}

void fontWeightPropEditor::fontWeightChanged(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
    }
}
//////////////////
fontAnglePropEditor::fontAnglePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "FontAngle";
}

QString fontAnglePropEditor::getValue()
{
    QString ret;
    ret = "normal";
    if(this->ctrl->associatedWidget()->font().italic())
        ret = "italic";
    return ret;
}

bool fontAnglePropEditor::isValidValue(QString newVal)
{
    return (newVal == "italic") || (newVal == "normal");
}

void fontAnglePropEditor::setValue(QString newVal)
{
    QFont font = this->ctrl->associatedWidget()->font();
    font.setItalic(newVal == "italic");
    this->ctrl->associatedWidget()->setFont(font);
}

QString fontAnglePropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * fontAnglePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("italic");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
    return cb;
}

void fontAnglePropEditor::fontAngleChanged(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
    }
}
///////////////////
toolTipPropEditor::toolTipPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "TooltipString";
}

QString toolTipPropEditor::getValue()
{
    return this->toolTipString;
}

bool toolTipPropEditor::isValidValue(QString newVal)
{
    newVal = newVal;
    return true;
}

void toolTipPropEditor::setValue(QString newVal)
{
    this->toolTipString = newVal;
    this->ctrl->associatedWidget()->setToolTip(newVal);
}

QString toolTipPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

/////////////////////////

callBackPropEditor::callBackPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "GenerateCallBack";
}
QString callBackPropEditor::getValue()
{
    QString ret;
    ret = "false";
    if(this->ctrl->haveCallBack())
        ret = "true";
    return ret;
}
bool callBackPropEditor::isValidValue(QString newVal)
{
    return (newVal.toLower() == "true") || (newVal.toLower() == "false");
}
void callBackPropEditor::setValue(QString newVal)
{
    this->ctrl->setHaveCallBack(newVal.toLower()=="true");
}

QString callBackPropEditor::generateCode()
{
    return "";
}

bool callBackPropEditor::canGenerateCode()
{
    return false;
}

QWidget * callBackPropEditor::getEditWidget()
{
    checkBoxUpdPropWdg * cb = new checkBoxUpdPropWdg(this);
    cb->setChecked(this->ctrl->haveCallBack());
    connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    return cb;
}

void callBackPropEditor::callBackChanged(int state)
{
    checkBoxUpdPropWdg * cb = (checkBoxUpdPropWdg *)QObject::sender();
    if(state == Qt::Unchecked)
    {
        this->setValue("false");
    }
    else if(state == Qt::Checked)
    {
        this->setValue("true");
    }
    else
    {
        disconnect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
        if(this->getValue().toLower() == "true")
            cb->setChecked(true);
        else
            cb->setChecked(false);
        connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    }
}

