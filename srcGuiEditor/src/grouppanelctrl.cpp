/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "grouppanelctrl.h"
#include "editablegrouppanel.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include <QStyleFactory>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QHeaderView>
#include <QPushButton>
#include <QRadioButton>

gbStyleGenerator::gbStyleGenerator(abstractUICCtrl *w)
{
    QFontDatabase fontDB;
    this->widget = w->associatedWidget();
    this->widget->setFont(mainWnd::getMainWnd()->getAppConfig()->getCtrlFont() );

    this->vFontName = this->widget->font().family();
    this->vFontSize = this->widget->font().pointSize();

    if(this->widget->font().bold())
        this->vFontWeight = fwBold;
    else
        this->vFontWeight = fwNormal;

    if(this->widget->font().italic())
        this->vFontAngle = faItalic;
    else
         this->vFontAngle = faNormal;

    this->vBackgroundColor = this->widget->palette().background().color();
    this->vBorderStyle = bsEtchedIn;
    this->vBorderWidth = 1;
    this->vForegroundColor = this->widget->palette().foreground().color();
    this->vTitlePos = tpLeftTop;


    this->widget->setStyleSheet(this->getStyleSheet());
}

QString gbStyleGenerator::getStyleSheet()
{
    QString ret;
    QString color;

    ret = ret + "QGroupBox {";

    color.sprintf("%02X%02X%02X", vBackgroundColor.red(), vBackgroundColor.green(), vBackgroundColor.blue());
    ret = ret + "background-color: #" + color + ";";

    ret = ret + "border-width: " + QString::number(this->vBorderWidth) + "px;";
    ret = ret + "border-top-color: gray;";
    ret = ret + "border-left-color: gray;";
    ret = ret + "border-bottom-color: gray;";
    ret = ret + "border-right-color: gray;";
    ret = ret + "border-style: solid;";
    ret = ret + "border-radius: 0px;";
    ret = ret + "padding: 20px;";
    ret = ret + "margin-top:10px;";
    ret = ret + "margin-bottom:0px;";

    switch(vTitlePos)
    {
      case tpLeftTop:case tpCenterTop:case tpRightTop:
        ret = ret + "margin-top:10px;";
        ret = ret + "margin-bottom:0px;";
        break;

      case tpLeftBottom:case tpCenterBottom:case tpRightBottom:
        ret = ret + "margin-top:0px;";
        ret = ret + "margin-bottom:10px;";
        break;
    }

    ret = ret + "font-family:" + this->vFontName + ";";
    ret = ret + "font-weight:" + vGbFontWeight[this->vFontWeight] + ";";
    ret = ret + "font-size:" + QString::number(this->vFontSize) + "pt;";
    ret = ret + "font-style:"+ vGbgFontAngle[this->vFontAngle] + ";";
    ret = ret + "}";

    ret = ret + "QGroupBox::title {";

    color.sprintf("%02X%02X%02X", vForegroundColor.red(), vForegroundColor.green(), vForegroundColor.blue());
    ret = ret + "color: #" + color + ";";

    ret = ret + "subcontrol-origin: border;";
    switch(vTitlePos)
    {
      case tpLeftTop:
        ret = ret + "subcontrol-position: top left;";
        ret = ret + "left:5px;";
        ret = ret + "top:-8px;";        
        break;
      case tpCenterTop:
        ret = ret + "subcontrol-position: top center;";
        ret = ret + "top:-8px;";
        break;
      case tpRightTop:
        ret = ret + "subcontrol-position: top right;";
        ret = ret + "top:-8px;";
        ret = ret + "right:10px;";
        break;
      case tpLeftBottom:
        ret = ret + "subcontrol-position: bottom left;";
        ret = ret + "left:5px;";
        ret = ret + "bottom:-8px;";
        break;
      case tpCenterBottom:
        ret = ret + "subcontrol-position: bottom center;";
        ret = ret + "bottom:-8px;";
        break;
      case tpRightBottom:
        ret = ret + "subcontrol-position: bottom right;";
        ret = ret + "bottom:-8px;";
        ret = ret + "right:10px;";
        break;
    }


    ret = ret + "font-family:" + this->vFontName + ";";

    ret = ret + "}";
    return ret;
}

int gbStyleGenerator::borderWidth(){
    return this->vBorderWidth;
}
void gbStyleGenerator::setBorderWidth(int w) {
    this->vBorderWidth = w;
    this->widget->setStyleSheet(this->getStyleSheet());
}

QColor gbStyleGenerator::backgroundColor(){
    return this->vBackgroundColor;
}
void gbStyleGenerator::setBackgroundColor(QColor c) {
    this->vBackgroundColor = c;
    this->widget->setStyleSheet(this->getStyleSheet());
}

QColor gbStyleGenerator::foregroundColor(){
    return this->vForegroundColor;
}
void gbStyleGenerator::setForegroundColor(QColor c){
    this->vForegroundColor = c;
    this->widget->setStyleSheet(this->getStyleSheet());
}

gbBorderStyle gbStyleGenerator::borderStyle(){
    return this->vBorderStyle;
}
void gbStyleGenerator::setBorderStyle(gbBorderStyle bs) {
    this->vBorderStyle = bs;
    this->widget->setStyleSheet(this->getStyleSheet());
}

gbTitlePos gbStyleGenerator::titlePos(){
    return this->vTitlePos;
}
void gbStyleGenerator::setTitlePos(gbTitlePos p) {
    this->vTitlePos = p;
    this->widget->setStyleSheet(this->getStyleSheet());
}

QString gbStyleGenerator::fontName(){
    return this->vFontName;
}

void gbStyleGenerator::setFontName(QString f){
    this->vFontName = f;
    this->widget->setStyleSheet(this->getStyleSheet());
}

gbFontWeight gbStyleGenerator::fontWeight()
{
    return this->vFontWeight;
}

void gbStyleGenerator::setFontWight(gbFontWeight fw)
{
    this->vFontWeight = fw;
    this->widget->setStyleSheet(this->getStyleSheet());
}

gbFontAngle gbStyleGenerator::fontAngle()
{
    return this->vFontAngle;
}

void gbStyleGenerator::setFontAngle(gbFontAngle fa)
{
    this->vFontAngle = fa;
    this->widget->setStyleSheet(this->getStyleSheet());
}


int gbStyleGenerator::fontSize(){
    return this->vFontSize;
}

void gbStyleGenerator::setFontSize(int s) {
    this->vFontSize = s;
    this->widget->setStyleSheet(this->getStyleSheet());
}


gbBgClrPropEditor::gbBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";    
    ctrl->associatedWidget()->setAutoFillBackground(true);
}

QString gbBgClrPropEditor::getValue()
{
    QString ret;
    QColor bk;
    bk = dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->backgroundColor();
    ret = "[";
    ret += QString::number(bk.redF(), 'f', 3) + " ";
    ret += QString::number(bk.greenF(), 'f', 3) + " ";
    ret += QString::number(bk.blueF(), 'f', 3) + "]";
    return ret;
}

bool gbBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void gbBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {        
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");

        QColor bkGround = QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat());
        dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setBackgroundColor(bkGround);
    }
}

QString gbBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}


QWidget * gbBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void gbBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}
////
///

gbFgClrPropEditor::gbFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString gbFgClrPropEditor::getValue()
{
    QString ret;
    QColor fg;
    fg = dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->foregroundColor();
    ret = "[";
    ret += QString::number(fg.redF(), 'f', 3) + " ";
    ret += QString::number(fg.greenF(), 'f', 3) + " ";
    ret += QString::number(fg.blueF(), 'f', 3) + "]";
    return ret;
}

bool gbFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void gbFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");

        QColor fg = QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat());
        dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setForegroundColor(fg);
    }
}

QString gbFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}


QWidget * gbFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void gbFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

///
////
gbTitlePropEditor::gbTitlePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "Title";
}

QString gbTitlePropEditor::getValue()
{
    return dynamic_cast<QGroupBox *>(this->ctrl->associatedWidget())->title();
}

bool gbTitlePropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    return true;
}

void gbTitlePropEditor::setValue(QString newVal)
{
    dynamic_cast<QGroupBox *>(this->ctrl->associatedWidget())->setTitle(newVal);
}

QString gbTitlePropEditor::generateCode()
{
    QString ret;
    ret = "'title', '";
    ret += dynamic_cast<QGroupBox *>(this->ctrl->associatedWidget())->title();
    ret += "'";
    return ret;
}
////
////
///

gbTitlePosPropEditor::gbTitlePosPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "TitlePosition";
}

QString gbTitlePosPropEditor::getValue()
{
    return vGbTitlePos[dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->titlePos()];
}

bool gbTitlePosPropEditor::isValidValue(QString newVal)
{
    bool ret = (newVal == vGbTitlePos[tpLeftTop]) ||
            (newVal == vGbTitlePos[tpCenterTop]) ||
            (newVal == vGbTitlePos[tpRightTop])||
            (newVal == vGbTitlePos[tpLeftBottom])||
            (newVal == vGbTitlePos[tpCenterBottom])||
            (newVal == vGbTitlePos[tpRightBottom]);
    return ret;
}

void gbTitlePosPropEditor::setValue(QString newVal)
{
    if(vGbTitlePos[tpLeftTop] == newVal)
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setTitlePos(tpLeftTop);
    else if(vGbTitlePos[tpCenterTop] == newVal)
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setTitlePos(tpCenterTop);
    else if(vGbTitlePos[tpRightTop] == newVal)
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setTitlePos(tpRightTop);
    else if(vGbTitlePos[tpLeftBottom] == newVal)
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setTitlePos(tpLeftBottom);
    else if(vGbTitlePos[tpCenterBottom] == newVal)
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setTitlePos(tpCenterBottom);
    else if(vGbTitlePos[tpRightBottom] == newVal)
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setTitlePos(tpRightBottom);
}

QString gbTitlePosPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * gbTitlePosPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);


    cb->addItem(vGbTitlePos[tpLeftTop]);
    cb->addItem(vGbTitlePos[tpCenterTop]);
    cb->addItem(vGbTitlePos[tpRightTop]);
    cb->addItem(vGbTitlePos[tpLeftBottom]);
    cb->addItem(vGbTitlePos[tpCenterBottom]);
    cb->addItem(vGbTitlePos[tpRightBottom]);

    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(titlePosChange(QString)));
    return cb;
}

void gbTitlePosPropEditor::titlePosChange(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(titlePosChange(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(titlePosChange(QString)));
    }
}

//////
///

gbFontNamePropEditor::gbFontNamePropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "FontName";
}

QString gbFontNamePropEditor::getValue()
{
    return dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->fontName();
}

bool gbFontNamePropEditor::isValidValue(QString newVal)
{
    return fontDB.families().contains(newVal);
}

void gbFontNamePropEditor::setValue(QString newVal)
{
    dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setFontName(newVal);
}

QString gbFontNamePropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * gbFontNamePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItems(fontDB.families());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
    return cb;
}


void gbFontNamePropEditor::fontChanged(const QString & txt)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(txt))
        this->setValue(txt);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
    }
}

///
///

gbFontSizePropEditor::gbFontSizePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "FontSize";
}

QString gbFontSizePropEditor::getValue()
{
    int size;
    size = dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->fontSize();
    return QString::number(size);
}

bool gbFontSizePropEditor::isValidValue(QString newVal)
{
    bool Ok;
    int dVal;
    dVal = newVal.toInt(&Ok);
    return (dVal > 0) && Ok;
}

void gbFontSizePropEditor::setValue(QString newVal)
{
    dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setFontSize(newVal.toInt());
}

QString gbFontSizePropEditor::generateCode()
{
    QString ret;

    ret = "'" + this->propName + "', " + this->getValue() + ", ";
    ret += "'FontUnits', 'points'";
    return ret;
}

QWidget * gbFontSizePropEditor::getEditWidget()
{
    spinUpdPropWdg * sb = new spinUpdPropWdg(this);
    sb->setAlignment(Qt::AlignRight);
    sb->setMinimum(1);
    sb->setMaximum(5000);
    sb->setSingleStep(1);
    sb->setValue(this->getValue().toInt());
    connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
    return sb;
}

void gbFontSizePropEditor::fontSizeChanged(const QString &text)
{
    spinUpdPropWdg * sb = (spinUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
        this->setValue(text);
    }
    else
    {
        disconnect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
        sb->setValue(this->getValue().toInt());
        connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
    }
}
///
///

gbFontWeightPropEditor::gbFontWeightPropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "FontWeight";
}

QString gbFontWeightPropEditor::getValue()
{
    return vGbFontWeight[dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->fontWeight()];
}

bool gbFontWeightPropEditor::isValidValue(QString newVal)
{
    return (newVal == "bold") || (newVal == "normal");
}

void gbFontWeightPropEditor::setValue(QString newVal)
{
    if (newVal == "bold")
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setFontWight(fwBold);
    else
      dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setFontWight(fwNormal);
}

QString gbFontWeightPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * gbFontWeightPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("bold");

    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
    return cb;
}

void gbFontWeightPropEditor::fontWeightChanged(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
    }
}
///
///

gbFontAnglePropEditor::gbFontAnglePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "FontAngle";
}

QString gbFontAnglePropEditor::getValue()
{
    return vGbgFontAngle[dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->fontAngle()];
}

bool gbFontAnglePropEditor::isValidValue(QString newVal)
{
    return (newVal == "italic") || (newVal == "normal");
}

void gbFontAnglePropEditor::setValue(QString newVal)
{
    if(newVal == "italic")
        dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setFontAngle(faItalic);
    else
        dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setFontAngle(faNormal);
}

QString gbFontAnglePropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * gbFontAnglePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("italic");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
    return cb;
}

void gbFontAnglePropEditor::fontAngleChanged(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
    }
}

///
///
gbBorderWidtPropEditor::gbBorderWidtPropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
        this->propName = "BorderWidth";
}

QString gbBorderWidtPropEditor::getValue(){
    int width;
    QString ret;
    width = dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->borderWidth();
    ret = QString::number(width);
    return ret;
}

bool gbBorderWidtPropEditor::isValidValue(QString newVal){
    bool ret;
    newVal.toInt(&ret);
    return ret;
}

void gbBorderWidtPropEditor::setValue(QString newVal){
    int width = newVal.toInt();
    dynamic_cast<groupPanelCtrl *>(this->ctrl)->styleGenerator->setBorderWidth(width);
}

QString gbBorderWidtPropEditor::generateCode(){
    return "'" + this->propName + "', " + this->getValue();
 }

QWidget * gbBorderWidtPropEditor::getEditWidget()
{
    spinUpdPropWdg * sb = new spinUpdPropWdg(this);
    sb->setAlignment(Qt::AlignRight);
    sb->setMinimum(1);
    sb->setMaximum(5000);
    sb->setSingleStep(1);
    sb->setValue(this->getValue().toInt());
    connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(borderSizeChanged(QString)));
    return sb;
}

void gbBorderWidtPropEditor::borderSizeChanged(const QString &text)
{
    spinUpdPropWdg * sb = (spinUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
        this->setValue(text);
    }
    else
    {
        disconnect(sb, SIGNAL(valueChanged(QString)), this, SLOT(borderSizeChanged(QString)));
        sb->setValue(this->getValue().toInt());
        connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(borderSizeChanged(QString)));
    }
}


unsigned int groupPanelCtrl::uicNameCounter = 0;

unsigned int groupPanelCtrl::getNameCounter()
{
    return uicNameCounter;
}


groupPanelCtrl::groupPanelCtrl(QWidget * parent,
                               abstractUICCtrl *octaveParent,
                               childWndDlg *parentWnd):
    abstractUICCtrl(parent, octaveParent, parentWnd)

{


    QColor bkGround;

    editableGroupPanel * groupPanel = new editableGroupPanel(parent, this);

    bkGround = groupPanel->palette().color(groupPanel->backgroundRole());

    uicNameCounter++;

    this->setCtrlName("GroupPanel_"+QString::number(uicNameCounter));
    groupPanel->setTitle(this->getCtrlName());
    this->vIsContainer = true;
    this->setAssociatedWidget(groupPanel);

    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new positionPropEditor(this));
    //this->properties.registerProp(new fontNamePropEditor(this));

    this->properties.registerProp(new gbTitlePropEditor(this));
    this->properties.registerProp(new gbBgClrPropEditor(this));
    this->properties.registerProp(new gbFgClrPropEditor(this));
    this->properties.registerProp(new gbBorderWidtPropEditor(this));    
    this->properties.registerProp(new gbFontNamePropEditor(this));
    this->properties.registerProp(new gbFontSizePropEditor(this));
    this->properties.registerProp(new gbFontWeightPropEditor(this));
    this->properties.registerProp(new gbFontAnglePropEditor(this));
    this->properties.registerProp(new gbTitlePosPropEditor(this));
    styleGenerator = new gbStyleGenerator(this);
}

void groupPanelCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableGroupPanel * gp = dynamic_cast<editableGroupPanel *>(this->associatedWidget());
    gp->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList groupPanelCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    QList<abstractUICCtrl *>::Iterator ctrl;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uipanel( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";

    for(ctrl  = this->addedWidgets.begin();
        ctrl != this->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= this)
          ret << (*ctrl)->generateMFile(path);
    }

    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList groupPanelCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

void groupPanelCtrl::addWidget(abstractUICCtrl *w){
    /* Resolve exclusive radio button and push button implementation*/
    if (w->className() == "radioButtonCtrl")
        ((QRadioButton *)w->associatedWidget())->setAutoExclusive(false);
    else if(w->className() == "toggleCtrl")
        ((QPushButton *)w->associatedWidget())->setAutoExclusive(false);
    abstractUICCtrl::addWidget(w);
}

groupPanelCtrl::~groupPanelCtrl()
{
    QList<abstractUICCtrl *>::Iterator w;    
    mainWnd::getPropPan()->getTblProp()->setRowCount(0);
    mainWnd::getPropPan()->getTblProp()->verticalHeader()->hide();
    for(w = addedWidgets.begin(); w != addedWidgets.end(); ++w)
        if((*w) != this)
          delete (*w);
    addedWidgets.clear();
    delete this->getAssociatedWidget();
    delete this->styleGenerator;
}

groupPanelCtrlGen::groupPanelCtrlGen():controlGenerator("groupPanel")
{
}

abstractUICCtrl * groupPanelCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    groupPanelCtrl * gp = new groupPanelCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    QXmlStreamReader::TokenType readedToken;
    prop = gp->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = gp->properties.getNext();
    }

    readedToken = xml.readNext();
    while(!xml.atEnd() && !xml.isEndElement())
    {
        if(readedToken == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "ContainerSrc")
            {
                src = xml.readElementText();
                if(src.length() > 0)
                    gp->setSrcCallBack(src.split('\n'));
            }
            readedToken = xml.tokenType();
        }
        else
            readedToken = xml.readNext();
    }
    gp->deselect();
    return gp;
}
