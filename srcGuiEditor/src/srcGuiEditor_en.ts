<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="configsettings.cpp" line="148"/>
        <source>Configuration changed</source>
        <translation>Configuration changed</translation>
    </message>
    <message>
        <location filename="configsettings.cpp" line="148"/>
        <source>To apply the selected fonts, guiEditor must be restart. Please, save your work and restart it.</source>
        <translation>To apply the selected fonts, guiEditor mut be restart. Please, save your work and restart it.</translation>
    </message>
    <message>
        <location filename="configsettings.cpp" line="153"/>
        <location filename="configsettings.cpp" line="159"/>
        <source>Cambio de configuración</source>
        <translation>Configuration changed</translation>
    </message>
    <message>
        <location filename="configsettings.cpp" line="153"/>
        <location filename="configsettings.cpp" line="159"/>
        <source>El idioma será actualizado la próxima vez que inicie la aplicación</source>
        <translation>The language has change, the new settings are applied in the next execution</translation>
    </message>
    <message>
        <source>% This definition is for backward compatibility and can be remove in next versions.</source>
        <translation type="vanished">% This definition is for backward compatibility and can be removed in next versions.</translation>
    </message>
    <message>
        <location filename="buttonctrl.cpp" line="216"/>
        <source>% This code will be executed when user click the button control.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="buttonctrl.cpp" line="217"/>
        <location filename="editctrl.cpp" line="275"/>
        <location filename="sliderctrl.cpp" line="201"/>
        <source>% As default, all events are deactivated, to activate must set the</source>
        <translation>% As default, all events are deactivated, to activate must set the</translation>
    </message>
    <message>
        <location filename="buttonctrl.cpp" line="218"/>
        <source>% property &apos;generateCallback&apos; from the properties editor</source>
        <oldsource>% propertie &apos;generateCallback&apos; from the properties editor</oldsource>
        <translation>% property &apos;generateCallback&apos; from the properties editor</translation>
    </message>
    <message>
        <location filename="editctrl.cpp" line="273"/>
        <source>% This code will be executed when the control lost focus and text has </source>
        <translation>% This code will be executed when de control lost focus and text has</translation>
    </message>
    <message>
        <location filename="editctrl.cpp" line="274"/>
        <source>% been changed or when press the &quot;enter&quot; key.</source>
        <translation>% been changed or when press the enter key</translation>
    </message>
    <message>
        <location filename="editctrl.cpp" line="276"/>
        <source>% property &apos;have callback&apos; from the properties editor</source>
        <oldsource>% propertie &apos;have callback&apos; from the properties editor</oldsource>
        <translation>% property &apos;generateCallBack&apos; from the properties editor</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="542"/>
        <source>Export project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="542"/>
        <source>The project was exported to folder </source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="602"/>
        <source>Not defined a main dialog</source>
        <translation>There are not defined a main dialog</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="602"/>
        <source>To run a project, you must define a main dialog. This will be the first dialog showed. If there is no dialog in project, add one. If have some dialog, mark as main dialog (left click over them in the project mannager, and select &quot;set as main dialog&quot;)</source>
        <translation>To run a project, you must define a main dialog. This will be the first dialog showed. If there is no dialog in project, add one. If have some dialog, mark as main dialog (left click over them in the project mannager, and select &quot;set as main dialog&quot;)</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="615"/>
        <location filename="guiproject.cpp" line="650"/>
        <source>Generate octave package</source>
        <translation>Generate octave package</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="616"/>
        <source>To generate a package from this project, you must configure the local path to gzip and tar program from File/Properties menu. This applications can be found in /bin or /usr/bin folder in GNU Linux or must be installed as external program in MS Windows.</source>
        <translation type="unfinished">To generate a package from this project, you must configure the local path to gzip and tar program from File/Properties menu. This applications can be found in /bin or /usr/bin folder in GNU Linux or must be installed as external program in MS Windows.</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="651"/>
        <source>A octave package must have special texts files with license information, details about of package and other data. Not all files are finded in project, basic versions of them are created and added.</source>
        <translation>A octave package must have special texts files with license information, details about of package and other data. Not all files are finded in project, basic versions of them are created and added. Please review the content of this files.</translation>
    </message>
    <message>
        <location filename="callbackctrl.cpp" line="52"/>
        <source>% This code can define a callback.</source>
        <translation>% This code can define a callback.</translation>
    </message>
    <message>
        <location filename="callbackctrl.cpp" line="53"/>
        <source>% You must associate them to figure or control onload like event window</source>
        <translation>% You must associate them to figure or control onload like event window</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="66"/>
        <location filename="childwndimg.cpp" line="62"/>
        <location filename="childwndmfile.cpp" line="65"/>
        <location filename="childwndtextfile.cpp" line="61"/>
        <source>Warning!!! This operation cannot be undone</source>
        <translation>Warning!!! This operation cannot be undone</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="67"/>
        <location filename="childwndimg.cpp" line="63"/>
        <location filename="childwndmfile.cpp" line="66"/>
        <location filename="childwndtextfile.cpp" line="62"/>
        <source>You are trying to delete the file %1, this operation can&apos;t be undone. Are you sure to delete this file?</source>
        <translation>You are trying to delete the file %1, this operation can&apos;t be undone. Are you sure to delete this file?</translation>
    </message>
    <message>
        <location filename="sliderctrl.cpp" line="200"/>
        <source>% This code will be executed when user change the value of slider.</source>
        <translation>% This code will be executed when user change the value of slider.</translation>
    </message>
    <message>
        <location filename="sliderctrl.cpp" line="202"/>
        <source>% property &apos;generateCallbck&apos; from the properties editor</source>
        <oldsource>% propertie &apos;generateCallbck&apos; from the properties editor</oldsource>
        <translation>% property &apos;generateCallback&apos; from the properties editor</translation>
    </message>
</context>
<context>
    <name>QextSerialPort</name>
    <message>
        <source>No Error has occurred</source>
        <translation type="vanished">No Error has occurred</translation>
    </message>
    <message>
        <source>Operation timed out (POSIX)</source>
        <translation type="vanished">Operation timed out</translation>
    </message>
</context>
<context>
    <name>aboutDlg</name>
    <message>
        <location filename="aboutdlg.ui" line="14"/>
        <source>About GUIEditor...</source>
        <translation>About guiEditor ...</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;GUI Editor for Octave&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;guiEditor for GNU Octave&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="33"/>
        <source>version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="43"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2020, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor for Octave is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;You should have received a copy of the GNU General Public License along with GUI Editor for Octave. If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2021, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;guiEditor for GNU Octave is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;guiEditor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;You should have received a copy of the GNU General Public License along with GUI Editor for Octave. If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2017, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor for Octave is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;You should have received a copy of the GNU General Public License along with GUI Editor for Octave. If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is free and open source application developed as part of a investigation project in the National Technological University, Regional Faculty Parana at Entre Rios (country of Argentina).&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Project group:&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Sergio Burgos (e.sergio.burgos@gmail.com) main developper of GUIEditor.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Eduardo J. Adam (eadam.fiq@gmail.com) and Francisco Sala (franciscosala@gmail.com) octave simulations developpers.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Omar Berardi (oberardi@gigared.com) and Fernando Sato (fsatopna@gmail.com) software development team.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Sebastian Vicario (sebastian_vicario@hotmail.com), Sergio Panoni (sergiohectorpanoni@yahoo.com.ar) and Hector Ramos (hectorramos@frp.utn.edu.ar) hardware developpers.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2020, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor for Octave is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;You should have received a copy of the GNU General Public License along with GUI Editor for Octave. If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="68"/>
        <source>Close</source>
        <translation type="unfinished">Close</translation>
    </message>
</context>
<context>
    <name>ardUnoPrgWnd</name>
    <message>
        <location filename="ardunoprgwnd.ui" line="14"/>
        <source>Arduino board program firmware</source>
        <translation>Arduino board program firmware</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="26"/>
        <source>Arduino program firmware</source>
        <translation>Arduino program firmware</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Using this option you can program a firmware to use a arduino board. At this moment, only arduino boards based on ATMega328 microcontroller are supported. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;The firmware to program are included in GUIEditor and is selected from the option selected in Property Editor. The associated class to comunicate with board are generated automacally.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: the previus firmware will be erase&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Using this option you can program a firmware to use a arduino board. At this moment, only arduino boards based on ATMega328 microcontroller are supported. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;The firmware to program is included in GUIEditor, the source code can be found in &quot;run/firmware&quot; folder. The associated class to comunicate with board are generated automacally.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: the previus firmware will be erase&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="61"/>
        <source>Serial port:</source>
        <translation>Serial port:</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="112"/>
        <source>Communication speed:</source>
        <translation>Communication speed:</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="120"/>
        <source>115200</source>
        <translation>115200</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="125"/>
        <source>57600</source>
        <translation>57600</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="179"/>
        <source>Programming %p%</source>
        <translation>Programming %p %</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="150"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="199"/>
        <source>Program</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="31"/>
        <source>Successful programming</source>
        <translation>Successful programming</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="31"/>
        <source>The programming process has been successfully completed</source>
        <translation>The programming process has been successfully completed</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="37"/>
        <source>Programming error</source>
        <translation>Programming error</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="37"/>
        <source>Something failed during the programming process. The action associated with the fault has returned the value:</source>
        <translation>Something failed during the programming process. The action associated with the fault has returned the value:</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="85"/>
        <source>Serial port error</source>
        <translation>Serial port error</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="85"/>
        <source>The selected serial port</source>
        <translation>The selected serial port</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="85"/>
        <source>could not open.</source>
        <translation>could not open.</translation>
    </message>
</context>
<context>
    <name>childWndDlg</name>
    <message>
        <location filename="childwnddlg.cpp" line="100"/>
        <source>Se perderá información</source>
        <translation>You can louse information</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="101"/>
        <source>Las modificaciones realizadas no se han guardado. Desea salir de todos modos?</source>
        <translation>The last changes are not saved. Do you want to leave anyway?</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="173"/>
        <source>Align left</source>
        <translation>Align left</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="174"/>
        <source>H center</source>
        <translation>Horizontal center</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="175"/>
        <source>Aling right</source>
        <translation>Align right</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="179"/>
        <source>Align top</source>
        <translation>Align top</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="180"/>
        <source>V center</source>
        <translation>Vertical centering</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="181"/>
        <source>Align bottom</source>
        <translation>Align bottom</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="185"/>
        <source>Center in dialog (H)</source>
        <translation>Center in dialog (H)</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="186"/>
        <source>Center in dialog (V)</source>
        <translation>Center in dialog (V)</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="190"/>
        <source>Resize to highest</source>
        <translation>Resize to highest</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="191"/>
        <location filename="childwnddlg.cpp" line="196"/>
        <source>Resize to small</source>
        <translation>Resize to small</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="195"/>
        <source>Resize to large</source>
        <translation>Resize to large</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="202"/>
        <source>Move to:</source>
        <translation>Move to:</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="262"/>
        <location filename="childwnddlg.cpp" line="283"/>
        <source>Select destination file name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="262"/>
        <source>XML Dialogs (*.dlg);;All files (*.*);;</source>
        <translation>Dialogs XML (*.xml);; All files (*.*);;</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="283"/>
        <source>XML Dialogs (*.xml);;All files (*);;</source>
        <translation>XML Dialogs (*.xml);;All files(*.*);;</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="875"/>
        <source>Warning!</source>
        <translation>Warning!</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="875"/>
        <source>You cannot move a control inside itself!</source>
        <translation>You cannot move a control inside itself!</translation>
    </message>
</context>
<context>
    <name>childWndMFile</name>
    <message>
        <location filename="childwndmfile.cpp" line="115"/>
        <location filename="childwndmfile.cpp" line="157"/>
        <source>Select destination file name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childwndmfile.cpp" line="115"/>
        <source>Octave script (*.m);;All files (*.*);;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childwndmfile.cpp" line="157"/>
        <source>Octave script (*.m);;All files (*);;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>childWndTextFile</name>
    <message>
        <location filename="childwndtextfile.cpp" line="97"/>
        <location filename="childwndtextfile.cpp" line="134"/>
        <source>Select destination file name</source>
        <translation>Select destination file name</translation>
    </message>
    <message>
        <location filename="childwndtextfile.cpp" line="97"/>
        <source>Text files (*.txt);;All files (*.*);;</source>
        <translation>Text files (*.txt);; All files (*.*);;</translation>
    </message>
    <message>
        <location filename="childwndtextfile.cpp" line="134"/>
        <source>Text files (*.txt);;All files (*);;</source>
        <translation>Text files (*.txt);;All files (*);;</translation>
    </message>
</context>
<context>
    <name>comManager</name>
    <message>
        <location filename="prgArduino/commanager.cpp" line="146"/>
        <source>
Don&apos;t have response from board when initialize the programming procedure</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>confirmWnd</name>
    <message>
        <location filename="confirmwnd.ui" line="14"/>
        <source>Exit?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="confirmwnd.ui" line="20"/>
        <source>Some documents are not saved. You want:
- Don&apos;t save any document and exit.
- Cancel and return to the main application.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="confirmwnd.ui" line="31"/>
        <source>Don&apos;t save and close</source>
        <translation></translation>
    </message>
    <message>
        <location filename="confirmwnd.ui" line="51"/>
        <source>Cancel and return</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>frameDlg</name>
    <message>
        <location filename="framedlg.cpp" line="482"/>
        <location filename="framedlg.cpp" line="485"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="framedlg.cpp" line="483"/>
        <source>% The source code written here will be executed when</source>
        <oldsource>% The source code writed here will be executed when</oldsource>
        <translation>% The source code written here will be executed when</translation>
    </message>
    <message>
        <location filename="framedlg.cpp" line="484"/>
        <source>% windows load. Work like &apos;onLoad&apos; event of other languages.</source>
        <translation>% windows load. Works like &apos;onLoad&apos; event of other languages.</translation>
    </message>
</context>
<context>
    <name>mainWnd</name>
    <message>
        <location filename="mainwnd.ui" line="14"/>
        <source>guiEditor</source>
        <oldsource>GUI Editor</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="57"/>
        <source>File</source>
        <translation type="unfinished">&amp;File</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="76"/>
        <source>View</source>
        <translation type="unfinished">&amp;View</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="85"/>
        <location filename="mainwnd.ui" line="422"/>
        <source>About</source>
        <translation type="unfinished">&amp;About</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="105"/>
        <source>Control Toolbar</source>
        <translation>Control toolbar</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="144"/>
        <source>File toolbar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="163"/>
        <source>Debug toolbar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="177"/>
        <source>Hardware Devices</source>
        <translation>Hadware devices</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="207"/>
        <source>Button</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="242"/>
        <location filename="mainwnd.cpp" line="97"/>
        <source>Properties</source>
        <translation type="unfinished">&amp;Properties</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="247"/>
        <source>Exit</source>
        <translation type="unfinished">&amp;Exit</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="256"/>
        <source>Label</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="268"/>
        <source>Combo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="271"/>
        <source>Add combo box</source>
        <translation>Add a pop up menu</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="280"/>
        <source>Edit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="283"/>
        <source>Add a line edit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="288"/>
        <source>Property Editor</source>
        <translation type="unfinished">Property editor</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="297"/>
        <source>Export dialog...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="302"/>
        <location filename="mainwnd.cpp" line="112"/>
        <source>Source code editor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="305"/>
        <source>Make the source code editor visible.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="314"/>
        <source>List</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="317"/>
        <source>Add a list control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="326"/>
        <source>Check box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="329"/>
        <source>Add a checkbox control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="338"/>
        <source>Radio Button</source>
        <translation type="unfinished">Radio button</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="341"/>
        <source>Add a radio button control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="394"/>
        <source>Run script</source>
        <translation>Run script</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="470"/>
        <source>GroupBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="473"/>
        <source>Add a group box panel</source>
        <translation>Add a panel control (uipanel)</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="478"/>
        <source>View project manager</source>
        <oldsource>View project mannager</oldsource>
        <translation>Project manager</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="481"/>
        <source>View project manager windows</source>
        <translation>View project manager window</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="490"/>
        <source>New Project</source>
        <translation type="unfinished">&amp;New project</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="499"/>
        <source>Open Project</source>
        <translation type="unfinished">&amp;Open project</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="508"/>
        <source>Save Project</source>
        <translation type="unfinished">&amp;Save project</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="517"/>
        <source>Run project</source>
        <translation type="unfinished">Run &amp;project</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="522"/>
        <source>Close project</source>
        <translation>&amp;Close project</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="525"/>
        <source>Close active project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="534"/>
        <source>CallBack</source>
        <translation>Callback</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="537"/>
        <source>Add a callback definition control</source>
        <translation>Add a callback definition control</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="546"/>
        <source>Add button group</source>
        <translation>Add a button group control (uibuttongroup)</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="549"/>
        <source>Add a new button group</source>
        <translation type="unfinished">Add a new button group control (uibuttongroup)</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="358"/>
        <source>Toggle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="361"/>
        <source>Add a toggle button control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="370"/>
        <source>Slider</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="373"/>
        <source>Add a slider control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="382"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="385"/>
        <source>Add a image control based on axes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="434"/>
        <source>Arduino Nano</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="437"/>
        <source>Add a Arduino Nano control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="446"/>
        <source>Node MCU</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="449"/>
        <source>Add a Node MCU Control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="458"/>
        <source>Edu CIAA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="461"/>
        <source>Add a Edu CIAA board</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="91"/>
        <source>Run</source>
        <translation>&amp;Run</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="195"/>
        <source>New dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="198"/>
        <source>New stand alone dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="216"/>
        <source>Open dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="225"/>
        <source>Save dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="234"/>
        <source>Save dialog as ....</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="237"/>
        <source>Save dialog as ...</source>
        <translation>Save dialog as ...</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="259"/>
        <source>Add label</source>
        <translation>Add label</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="350"/>
        <source>Frame</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="353"/>
        <source>Add a frame control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="397"/>
        <source>Test script in octave</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="406"/>
        <source>Stop</source>
        <translation type="unfinished">&amp;Stop</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="409"/>
        <source>Stop running script in octave</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="414"/>
        <source>Octave Console</source>
        <translation type="unfinished">GNU Octave console</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="417"/>
        <source>View the Octave console</source>
        <translation type="unfinished">View the GNU Octave console</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="425"/>
        <source>About GUIEditor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="132"/>
        <location filename="mainwnd.cpp" line="212"/>
        <location filename="mainwnd.cpp" line="564"/>
        <source>Starting octave...</source>
        <translation type="unfinished">Starting GNU Octave...</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="156"/>
        <location filename="mainwnd.cpp" line="237"/>
        <location filename="mainwnd.cpp" line="587"/>
        <source>started.</source>
        <translation></translation>
    </message>
    <message>
        <source>XML Dialogs (*.dlg);;All files (*.*);;</source>
        <translation type="vanished">Dialogs XML (*.xml);; All files (*.*);;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="388"/>
        <source>XML Dialogs (*.xml);;All files (*);;</source>
        <translation>XML Dialogs (*.xml);;All files(*.*);;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="388"/>
        <source>Select dialog to open</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="409"/>
        <source>Target folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="709"/>
        <source>Open gui project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="709"/>
        <source>GUI Project files (*.prj);;All files (*.*);;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="249"/>
        <location filename="mainwnd.cpp" line="598"/>
        <source>Bad configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="91"/>
        <source>Project Manager</source>
        <oldsource>Project Mannager</oldsource>
        <translation>Project Manager</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="105"/>
        <source>Octave console</source>
        <translation type="unfinished">GNU Octave console</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="249"/>
        <location filename="mainwnd.cpp" line="598"/>
        <source>To run this script is neccesary configure the temporary directory and the octave path from the File/Property menu</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>mfileEditorWidget</name>
    <message>
        <location filename="mfileeditorwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>newFileWnd</name>
    <message>
        <location filename="newfilewnd.ui" line="14"/>
        <source>Add new file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="22"/>
        <source>File name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="49"/>
        <source>A file with this name alredy exists.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="73"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="93"/>
        <source>Add</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>newProjectWnd</name>
    <message>
        <location filename="newprojectwnd.ui" line="14"/>
        <source>Project setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="22"/>
        <source>Project name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="36"/>
        <source>Project path:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="46"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="57"/>
        <source>Version:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="131"/>
        <source>The tarjet folder with this name alredy exists.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="155"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="178"/>
        <source>Create</source>
        <translation></translation>
    </message>
    <message>
        <location filename="newprojectwnd.cpp" line="40"/>
        <source>Project folder</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>pkgGenWnd</name>
    <message>
        <location filename="pkggenwnd.ui" line="14"/>
        <source>Generate octave package</source>
        <translation>Generate octave package</translation>
    </message>
    <message>
        <location filename="pkggenwnd.ui" line="25"/>
        <source>Open Folder</source>
        <translation>Open Folder</translation>
    </message>
    <message>
        <location filename="pkggenwnd.ui" line="45"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="52"/>
        <location filename="pkggenwnd.cpp" line="87"/>
        <source>Deleting previus version of </source>
        <translation>Deleting previus version of</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="62"/>
        <source>Staring tar...</source>
        <translation>Starting tar ...</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="75"/>
        <source> [started]</source>
        <translation> [started]</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="83"/>
        <source>Tar finished Ok</source>
        <translation>Tar finished Ok</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="93"/>
        <source>Staring gzip...</source>
        <translation>Starting gzip ...</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="97"/>
        <source>Tar call fail. </source>
        <translation>Tar call fail. </translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="123"/>
        <source>Generation finished!</source>
        <translation>Generation finished!</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="124"/>
        <source>You can open the container folder to use the package</source>
        <translation>You can open the container folder to use the package</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="127"/>
        <source>Error gziping package.</source>
        <translation>Error gziping package.</translation>
    </message>
</context>
<context>
    <name>prjPropWnd</name>
    <message>
        <location filename="prjpropwnd.ui" line="14"/>
        <source>Project Properties</source>
        <translation>Project Properties</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="20"/>
        <source>Project version:</source>
        <translation>Project version:</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="38"/>
        <source>Define _basePath var</source>
        <translation>Define _basePath var</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="45"/>
        <source>Define _imgPath var</source>
        <translation>Define imgPath var</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="67"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="87"/>
        <source>Accept</source>
        <translation>Accept</translation>
    </message>
</context>
<context>
    <name>projectWnd</name>
    <message>
        <source>Gestor de proyectos</source>
        <translation type="vanished">Project mannager</translation>
    </message>
    <message>
        <source>Proyectos Abiertos</source>
        <translation type="vanished">Opened projects</translation>
    </message>
    <message>
        <source>Se perderá información</source>
        <translation type="vanished">You can lose the last changes</translation>
    </message>
    <message>
        <source>Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?</source>
        <translation type="vanished">Some files included in the project are not saved. Do you want save them?</translation>
    </message>
</context>
<context>
    <name>propertyWndClass</name>
    <message>
        <location filename="propertywndclass.ui" line="14"/>
        <source>GUIEditor Properties</source>
        <translation>guiEditor Properties</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="251"/>
        <source>Debug options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="257"/>
        <source>Path to octave app:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="83"/>
        <location filename="propertywndclass.ui" line="114"/>
        <location filename="propertywndclass.ui" line="157"/>
        <location filename="propertywndclass.ui" line="191"/>
        <location filename="propertywndclass.ui" line="266"/>
        <location filename="propertywndclass.ui" line="355"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="68"/>
        <source>Package generation tools</source>
        <translation>Package generation tools</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="74"/>
        <source>gzip path:</source>
        <translation>gzip path:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="96"/>
        <location filename="propertywndclass.ui" line="127"/>
        <location filename="propertywndclass.ui" line="170"/>
        <location filename="propertywndclass.ui" line="204"/>
        <location filename="propertywndclass.ui" line="285"/>
        <location filename="propertywndclass.ui" line="337"/>
        <location filename="propertywndclass.ui" line="374"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="105"/>
        <source>tar path:</source>
        <translation>tar path:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="139"/>
        <source>Fonts types</source>
        <translation>Font types</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="147"/>
        <source>GUI Controls:</source>
        <translation>GUI Controls:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="181"/>
        <source>Source code editor:</source>
        <translation>Source code editor:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="218"/>
        <source>Source code editor tab size:</source>
        <translation>Source code editor tab size:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="294"/>
        <source>QT_PLUGIN_PATH:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="318"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="346"/>
        <source>Temp dir path:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="385"/>
        <source>Librarys path:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="417"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="436"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="24"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="32"/>
        <source>Language:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="40"/>
        <source>Spanish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="45"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="467"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="487"/>
        <source>Accept</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="38"/>
        <source>Octave file path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="38"/>
        <source>Octave binary(octave*);;All files (*.*);;</source>
        <translation>Octave binary (octave*);;All files (*.*);;</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="50"/>
        <source>Termporal dir path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="61"/>
        <source>Library path to add</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="77"/>
        <source>QT_PLUGIN_PATH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="92"/>
        <source>gzip file path</source>
        <translation>gzip file path</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="92"/>
        <source>gzip binary(gzip*);;All files (*.*);;</source>
        <translation>gzip binary(gzip*);;All files (*.*);;</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="102"/>
        <source>tar file path</source>
        <translation>tar file path</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="102"/>
        <source>tar binary(tar*);;All files (*.*);;</source>
        <translation>tar binary(tar*);;All files (*.*);;</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="113"/>
        <location filename="propertywndclass.cpp" line="124"/>
        <source>Select controls font name</source>
        <translation>Select controls font name</translation>
    </message>
</context>
<context>
    <name>treeItemDialog</name>
    <message>
        <location filename="childwnddlg.cpp" line="58"/>
        <source>Set as main dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="59"/>
        <source>Delete </source>
        <translation>Delete </translation>
    </message>
</context>
<context>
    <name>treeItemImage</name>
    <message>
        <location filename="childwndimg.cpp" line="45"/>
        <source>Delete </source>
        <translation>Delete </translation>
    </message>
</context>
<context>
    <name>treeItemMFile</name>
    <message>
        <location filename="childwndmfile.cpp" line="58"/>
        <source>Delete</source>
        <oldsource>Delete </oldsource>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>treeItemProject</name>
    <message>
        <location filename="guiproject.cpp" line="71"/>
        <source>Add a new dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="72"/>
        <source>Add a new m file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="81"/>
        <source>Run project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="75"/>
        <source>Export project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="73"/>
        <source>Add a new image</source>
        <translation>Add a new image</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="76"/>
        <source>Generate octave package</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="78"/>
        <source>Save project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="79"/>
        <source>Close project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="83"/>
        <source>Properties</source>
        <translation></translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="160"/>
        <source>Select image to add</source>
        <translation>Select image to add</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="162"/>
        <source>PNG Files (*.png);;SVG Files (*.svg);;All files (*.*);;</source>
        <translation>PNG Files (*.png);;SVG Files (*.svg);;All files (*.*);;</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="203"/>
        <location filename="guiproject.cpp" line="206"/>
        <source>Define </source>
        <translation>Define </translation>
    </message>
</context>
<context>
    <name>treeItemTextFile</name>
    <message>
        <location filename="childwndtextfile.cpp" line="44"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>wdgArgEditor</name>
    <message>
        <location filename="wdgargeditor.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="wdgargeditor.ui" line="50"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="wdgargeditor.cpp" line="67"/>
        <source>Callback argument editor</source>
        <translation>Callback argument editor</translation>
    </message>
</context>
<context>
    <name>wdgClrChooser</name>
    <message>
        <location filename="wdgclrchooser.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wdgclrchooser.ui" line="38"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>wdgDebug</name>
    <message>
        <location filename="wdgdebug.ui" line="14"/>
        <source>Form</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="wdgdebug.ui" line="47"/>
        <source>Clear output console</source>
        <translation>Clear output console</translation>
    </message>
    <message>
        <location filename="wdgdebug.ui" line="61"/>
        <source>Copy selected lines from output console</source>
        <translation>Copy selected lines from output console</translation>
    </message>
    <message>
        <location filename="wdgdebug.ui" line="96"/>
        <source>exec</source>
        <translation>exec</translation>
    </message>
    <message>
        <location filename="wdgdebug.cpp" line="83"/>
        <source>Octave is not running!</source>
        <translation>Octave is not running</translation>
    </message>
</context>
<context>
    <name>wdgItemEditor</name>
    <message>
        <location filename="wdgitemeditor.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wdgitemeditor.ui" line="50"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>wdgPrjMan</name>
    <message>
        <location filename="wdgprjman.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="wdgprjman.ui" line="36"/>
        <source>Open projects</source>
        <translation>Open projects</translation>
    </message>
    <message>
        <location filename="wdgprjman.cpp" line="106"/>
        <source>Se perderá información</source>
        <translation>Information can be lose</translation>
    </message>
    <message>
        <location filename="wdgprjman.cpp" line="107"/>
        <source>Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?</source>
        <translation>Some files included in the project are not saved. Do you want save them?</translation>
    </message>
</context>
<context>
    <name>wdgProp</name>
    <message>
        <location filename="wdgprop.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="wdgprop.ui" line="48"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="wdgprop.ui" line="63"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
</context>
<context>
    <name>wdgSrcEditor</name>
    <message>
        <location filename="wdgsrceditor.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="wdgsrceditor.ui" line="55"/>
        <source>Associated control:</source>
        <translation>Associated control:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Save</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Load</translation>
    </message>
</context>
<context>
    <name>wndItemEdit</name>
    <message>
        <location filename="wnditemedit.ui" line="14"/>
        <source>List item editor</source>
        <translation>List item editor</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="22"/>
        <source>New val:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="32"/>
        <source>Add</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="39"/>
        <source>Delete selected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="42"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="58"/>
        <source>Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="78"/>
        <source>Down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="91"/>
        <source>Accept</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="111"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
</context>
</TS>
