/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "configsettings.h"
#include "propertywndclass.h"
#include "ui_propertywndclass.h"
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QTranslator>
#include <QFontDatabase>

configSettings::configSettings()
{
  this->loadCfg();
}

void configSettings::loadCfg()
{
    int countLibPath;
    int i;
    QString pathLib;
    QFontDatabase fontDb;
    QSettings cfg("GuiEditor", "GuiEditor");
    this->octavePath = cfg.value("octavePath").toString();
    this->tempDir = cfg.value("tmpDir", QDir::tempPath()).toString();
    this->language = cfg.value("language", "Spanish").toString();
    this->qt5PlugginPath = cfg.value("qt5PlugginPath", "").toString();
    this->gzipPath = cfg.value("gzipPath", "").toString();
    this->tarPath = cfg.value("tarPath", "").toString();
    this->ctrlFont.fromString(cfg.value("controlFont", fontDb.font("Arial", "Light", 10).toString()).toString());
    this->editorFont.fromString(cfg.value("editorFont", fontDb.font("Courier", "Light", 11).toString()).toString());
    this->editorTabSize = cfg.value("editorTabSize", 4).toInt();
    countLibPath =  cfg.value("pathLibCount", 0).toInt();
    pathLib.clear();
    for(i=0; i < countLibPath; i++)
    {
        pathLib = cfg.value("pathLib" + QString::number(i), "").toString();
        if(pathLib.length() > 0)
          this->libPaths.push_back(pathLib);
    }
}

void configSettings::saveCfg()
{
    int i;
    QSettings cfg("GuiEditor", "GuiEditor");
    cfg.setValue("octavePath", this->octavePath );
    cfg.setValue("tmpDir", this->tempDir);
    cfg.setValue("language", this->language);
    cfg.setValue("pathLibCount", this->libPaths.count());
    cfg.setValue("qt5PlugginPath", this->qt5PlugginPath);
    cfg.setValue("gzipPath", this->gzipPath);
    cfg.setValue("tarPath", this->tarPath);

    cfg.setValue("controlFont", this->ctrlFont.toString());
    cfg.setValue("editorFont", this->editorFont.toString());
    cfg.setValue("editorTabSize", this->editorTabSize);

    for(i=0; i < this->libPaths.count(); i++)
        cfg.setValue("pathLib" + QString::number(i), this->libPaths[i]);
}

bool configSettings::octavePathOk()
{
    return QFile::exists(octavePath);
}

bool configSettings::tmpDirOk()
{
    QDir qDirTmp(tempDir);
    return qDirTmp.exists();
}

bool configSettings::gzipPathOk()
{
    return QFile::exists(this->gzipPath);
}

bool configSettings::tarPathOk()
{
    return QFile::exists(this->tarPath);
}

void configSettings::paramToWnd(propertyWndClass *wnd)
{
    int i;
    wnd->ui->labOctavePath->setText(this->octavePath);
    wnd->ui->labOctavePath->setToolTip(this->octavePath);
    wnd->ui->labTempDir->setText(this->tempDir);
    wnd->ui->labTempDir->setToolTip(this->tempDir);

    wnd->ui->labQt5Pluggin->setText(this->qt5PlugginPath);
    wnd->ui->labQt5Pluggin->setToolTip(this->qt5PlugginPath);

    wnd->ui->labGZipPath->setText(this->getGzipPath());
    wnd->ui->labGZipPath->setToolTip(this->getGzipPath());

    wnd->ui->labTarPath->setText(this->getTarPath());
    wnd->ui->labTarPath->setToolTip(this->getTarPath());

    wnd->ui->labFontCtrl->setText(this->getCtrlFont().family() + ", " + QString::number(this->getCtrlFont().pointSize()));
    wnd->ui->labSrcFont->setText(this->getEditorFont().family() + ", " + QString::number(this->getEditorFont().pointSize()));

    wnd->ui->sbTabSize->setValue(this->getEditorTabSize());

    wnd->editor = this->getEditorFont();
    wnd->ctrls  = this->getCtrlFont();

    if(this->language == "Spanish")
        wnd->ui->cmbLen->setCurrentIndex(0);
    else if(this->language == "English")
        wnd->ui->cmbLen->setCurrentIndex(1);

    for(i=0; i < this->libPaths.count(); i++)
        wnd->ui->lstPathLib->addItem(this->libPaths[i]);
}

void configSettings::wndToParam(propertyWndClass *wnd)
{
    int i;
    bool mustRestart = false;
    this->octavePath = wnd->ui->labOctavePath->text();
    this->tempDir = wnd->ui->labTempDir->text();
    this->qt5PlugginPath = wnd->ui->labQt5Pluggin->text();
    this->tarPath = wnd->ui->labTarPath->text();
    this->gzipPath = wnd->ui->labGZipPath->text();
    this->editorTabSize = wnd->ui->sbTabSize->value();
    mustRestart = (this->editorFont != wnd->editor) || (this->ctrlFont != wnd->ctrls);
    this->editorFont = wnd->editor;
    this->ctrlFont = wnd->ctrls;
    if(mustRestart)
        QMessageBox::information(NULL, QObject::tr("Configuration changed"), QObject::tr("To apply the selected fonts, guiEditor must be restart. Please, save your work and restart it."));

    if(wnd->ui->cmbLen->currentIndex() == 0)
    {
        if(this->language != "Spanish")
            QMessageBox::information(NULL,QObject::tr("Cambio de configuración"), QObject::tr("El idioma será actualizado la próxima vez que inicie la aplicación"));
        this->language = "Spanish";
    }
    else if(wnd->ui->cmbLen->currentIndex() == 1)
    {
        if(this->language != "English")
            QMessageBox::information(NULL,QObject::tr("Cambio de configuración"), QObject::tr("El idioma será actualizado la próxima vez que inicie la aplicación"));
        this->language = "English";
    }
    this->libPaths.clear();    
    for(i=0; i < wnd->ui->lstPathLib->count(); i++)
        this->libPaths.push_back(wnd->ui->lstPathLib->item(i)->text());
}

int configSettings::getEditorTabSize(){
    return this->editorTabSize;
}

QString configSettings::getQt5PlugginPath()
{
    return this->qt5PlugginPath;
}

QFont configSettings::getCtrlFont()
{
    return this->ctrlFont;
}

QFont configSettings::getEditorFont()
{
    return this->editorFont;
}

QStringList configSettings::getLibPaths()
{
    return QStringList(this->libPaths);
}

QString configSettings::getOctavePath()
{
    return this->octavePath;
}

QString configSettings::getTempDir()
{
    return this->tempDir;
}
QString configSettings::getLanguage()
{
    return this->language;
}

QString configSettings::getGzipPath()
{
    return this->gzipPath;
}

QString configSettings::getTarPath()
{
    return this->tarPath;
}


configSettings::~configSettings()
{
    this->libPaths.clear();
}
