#-------------------------------------------------
#
# Project created by QtCreator 2015-07-08T08:15:17
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ../guiEditor
TEMPLATE = app

TRANSLATIONS = srcGuiEditor_en.ts \
	srcGuiEditor_sp.ts 
CONFIG += staticlib
SOURCES += main.cpp\
    buttongroupctrl.cpp \
    editablebuttongroup.cpp \
    mainwnd.cpp \
    framedlg.cpp \
    confirmwnd.cpp \
    adjpointtopleftclass.cpp \
    adjpointbottomleftclass.cpp \
    adjpointmidleleftclass.cpp \
    adjpointtoprightclass.cpp \
    adjpointmidlerightclass.cpp \
    adjpointbottomrightclass.cpp \
    adjpointtopmidleclass.cpp \
    adjpointbottommidleclass.cpp \
    abstractuicctrl.cpp \
    editablelineedit.cpp \
    editablebutton.cpp \
    editabletext.cpp \
    editctrl.cpp \
    buttonctrl.cpp \
    textctrl.cpp \
    storagemanager.cpp \
    commonproperties.cpp \
    controlgenerator.cpp \
    wdgclrchooser.cpp \
    cmbctrl.cpp \
    editablecmb.cpp \
    wdgitemeditor.cpp \
    wnditemedit.cpp \
    editablelist.cpp \
    listctrl.cpp \
    editableframe.cpp \
    framectrl.cpp \
    checkboxctrl.cpp \
    editablecheckbox.cpp \
    radiobuttonctrl.cpp \
    editableradiobutton.cpp \
    editabletoggle.cpp \
    togglectrl.cpp \
    editableslider.cpp \
    sliderctrl.cpp \
    editableimg.cpp \
    imgctrl.cpp \
    propertywndclass.cpp \
    configsettings.cpp \
    aboutdlg.cpp \
    editableANano.cpp \
    ananoctrl.cpp \
    editableNodeMCU.cpp \
    nodeMCUctrl.cpp \
    editableEduCIAA.cpp \
    eduCIAACtrl.cpp \
    editablegrouppanel.cpp \
    grouppanelctrl.cpp \
    guiproject.cpp \
    projectitem.cpp \
    newprojectwnd.cpp \
    prjtreewidget.cpp \
    newfilewnd.cpp \
    abstractchildwnd.cpp \
    childwnddlg.cpp \
    childwndmfile.cpp \
    mfileeditorwidget.cpp \
    prjpropwnd.cpp \
    childwndimg.cpp \
    childwndtextfile.cpp \
    pkggenwnd.cpp \
    callbackctrl.cpp \
    editablecallback.cpp \
    wdgargeditor.cpp \
    ardunoprgwnd.cpp \
    prgArduino/actcheckmem.cpp \
    prgArduino/actenableprogram.cpp \
    prgArduino/actleaveprg.cpp \
    prgArduino/actsetdevparam.cpp \
    prgArduino/actsignature.cpp \
    prgArduino/actsoftversion.cpp \
    prgArduino/actsync.cpp \
    prgArduino/actwritemem.cpp \
    prgArduino/commanager.cpp \
    prgArduino/hexfile.cpp \
    prgArduino/prgaction.cpp \
    prgArduino/sccheckmem.cpp \
    prgArduino/scenableprgm.cpp \
    prgArduino/scgetparam.cpp \
    prgArduino/scleaveprg.cpp \
    prgArduino/scloadpage.cpp \
    prgArduino/scsetdevparam.cpp \
    prgArduino/scsignature.cpp \
    prgArduino/scsync.cpp \
    prgArduino/scwritemem.cpp \
    prgArduino/serialcmd.cpp \
    wdgprop.cpp \
    wdgprjman.cpp \
    wdgdebug.cpp \
    wdgsrceditor.cpp \
    codeeditor.cpp

HEADERS  += mainwnd.h \
    buttongroupctrl.h \
    editablebuttongroup.h \
    framedlg.h \
    confirmwnd.h \
    adjpointtopleftclass.h \
    adjpointbottomleftclass.h \
    adjpointmidleleftclass.h \
    adjpointtoprightclass.h \
    adjpointmidlerightclass.h \
    adjpointbottomrightclass.h \
    adjpointtopmidleclass.h \
    adjpointbottommidleclass.h \
    abstractuicctrl.h \
    editablelineedit.h \
    editablebutton.h \
    editabletext.h \
    editctrl.h \
    buttonctrl.h \
    textctrl.h \
    tblwdgitempropertyvalue.h \
    storagemanager.h \
    commonproperties.h \
    controlgenerator.h \
    wdgclrchooser.h \
    cmbctrl.h \
    editablecmb.h \
    wdgitemeditor.h \
    wnditemedit.h \
    editablelist.h \
    listctrl.h \
    editableframe.h \
    framectrl.h \
    checkboxctrl.h \
    editablecheckbox.h \
    editableradiobutton.h \
    radiobuttonctrl.h \
    editabletoggle.h \
    togglectrl.h \
    editableslider.h \
    sliderctrl.h \
    editableimg.h \
    imgctrl.h \
    propertywndclass.h \
    configsettings.h \
    aboutdlg.h \
    editableANano.h \
    ananoctrl.h \
    editableNodeMCU.h \
    nodeMCUctrl.h \
    editableEduCIAA.h \
    eduCIAACtrl.h \
    editablegrouppanel.h \
    grouppanelctrl.h \
    guiproject.h \
    projectitem.h \
    newprojectwnd.h \
    prjtreewidget.h \
    newfilewnd.h \
    abstractchildwnd.h \
    childwnddlg.h \
    childwndmfile.h \
    mfileeditorwidget.h \
    prjpropwnd.h \
    childwndimg.h \
    childwndtextfile.h \
    pkggenwnd.h \
    callbackctrl.h \
    editablecallback.h \
    wdgargeditor.h \
    ardunoprgwnd.h \
    prgArduino/actcheckmem.h \
    prgArduino/actenableprogram.h \
    prgArduino/actleaveprg.h \
    prgArduino/actsetdevparam.h \
    prgArduino/actsignature.h \
    prgArduino/actsoftversion.h \
    prgArduino/actsync.h \
    prgArduino/actwritemem.h \
    prgArduino/commanager.h \
    prgArduino/command.h \
    prgArduino/Devices.h \
    prgArduino/hexfile.h \
    prgArduino/prgaction.h \
    prgArduino/sccheckmem.h \
    prgArduino/scenableprgm.h \
    prgArduino/scgetparam.h \
    prgArduino/scleaveprg.h \
    prgArduino/scloadpage.h \
    prgArduino/scsetdevparam.h \
    prgArduino/scsignature.h \
    prgArduino/scsync.h \
    prgArduino/scwritemem.h \
    prgArduino/serialcmd.h \
    wdgprop.h \
    wdgprjman.h \
    wdgdebug.h \
    wdgsrceditor.h \
    codeeditor.h \
    guiEditorVersion.h

FORMS += mainwnd.ui \
    confirmwnd.ui \
    wdgclrchooser.ui \
    wdgitemeditor.ui \
    wnditemedit.ui \
    propertywndclass.ui \
    aboutdlg.ui \
    newprojectwnd.ui \
    newfilewnd.ui \
    mfileeditorwidget.ui \
    prjpropwnd.ui \
    pkggenwnd.ui \
    wdgargeditor.ui \
    ardunoprgwnd.ui \
    wdgprop.ui \
    wdgprjman.ui \
    wdgdebug.ui \
    wdgsrceditor.ui

RESOURCES += \
    resources.qrc

RC_ICONS = ../../iconos/guiEditor_icon.ico

OTHER_FILES += \
    modelo_doc.txt

TRANSLATIONS =    srcGuiEditor_sp.ts \
    srcGuiEditor_en.ts

transSp.tarjet = srcGuiEditor_sp.qm
transSp.commands = lrelease \"$$_PRO_FILE_PWD_\"/srcGuiEditor_sp.ts -qm ../srcGuiEditor_sp.qm

transEn.tarjet = srcGuiEditor_en.qm
transEn.commands = lrelease \"$$_PRO_FILE_PWD_\"/srcGuiEditor_en.ts -qm ../srcGuiEditor_en.qm


unix{
  QMAKE_LFLAGS += -no-pie
}

QMAKE_EXTRA_TARGETS += transSp
QMAKE_EXTRA_TARGETS += transEn

POST_TARGETDEPS += transSp
POST_TARGETDEPS += transEn
