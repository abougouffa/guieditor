/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "abstractuicctrl.h"
#include <QWidget>
#include <QRegExp>
#include <QLabel>
#include "framedlg.h"
#include "mainwnd.h"
#include "commonproperties.h"
#include <QRadioButton>
#include <QPushButton>

updPropWdg::updPropWdg(abstractPropEditor * prop)
{
    this->prop = prop;
}

abstractPropEditor * updPropWdg::getPropEditor()
{
    return this->prop;
}

editUpdPropWdg::editUpdPropWdg(abstractPropEditor * prop):QLineEdit(0), updPropWdg(prop){}
void editUpdPropWdg::updPropValue()
{
  QLineEdit::setText(this->getPropEditor()->getValue());
}

comboUpdPropWdg::comboUpdPropWdg(abstractPropEditor * prop):QComboBox(0), updPropWdg(prop){}
void comboUpdPropWdg::updPropValue()
{
    QComboBox::setCurrentIndex(this->findText(this->getPropEditor()->getValue()));
}

spinUpdPropWdg::spinUpdPropWdg(abstractPropEditor * prop):QSpinBox(0), updPropWdg(prop){}
void spinUpdPropWdg::updPropValue()
{
  QSpinBox::setValue(this->getPropEditor()->getValue().toInt());
}

checkBoxUpdPropWdg::checkBoxUpdPropWdg(abstractPropEditor * prop):QCheckBox(0), updPropWdg(prop){}
void checkBoxUpdPropWdg::updPropValue()
{
    QCheckBox::setChecked(this->getPropEditor()->getValue().toLower() == "true");
}



abstractPropEditor::abstractPropEditor(abstractUICCtrl *ctrl):QObject(0)
{
    this->propName = "";
    this->ctrl = ctrl;
}

QString abstractPropEditor::getPropName()
{
    return this->propName;
}

QString abstractPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

bool abstractPropEditor::canGenerateCode()
{
    return true;
}

QWidget * abstractPropEditor::getTitleWidget()
{
    QLabel * text = new QLabel;
    text->setText(this->propName);
    return text;
}

QWidget * abstractPropEditor::getEditWidget()
{
    editUpdPropWdg * edit = new editUpdPropWdg(this);
    edit->setFrame(false);
    edit->setText(this->getValue());
    connect(edit, SIGNAL(editingFinished()), this, SLOT(valueChanged()));
    return edit;
}

void abstractPropEditor::valueChanged()
{
    editUpdPropWdg * ed;
    ed = (editUpdPropWdg *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {
        ed->setText(this->getValue());        
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChanged()));
}

namePropEditor::namePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "Name";
}

bool namePropEditor::nameExists(QString newName, abstractUICCtrl *actCtrl, abstractUICCtrl *ctrlContainer){
    abstractUICCtrl *inCtrl;
    bool ret = false;
    inCtrl = ctrlContainer->getFirstCtrl();
    while((inCtrl) && (!ret)){
        if(inCtrl != actCtrl){
            ret = newName == inCtrl->getCtrlName();
            if(inCtrl->isContainer() && (inCtrl->getOctaveParent() != NULL))
                ret = ret || nameExists(newName, actCtrl, inCtrl);
        }
        inCtrl = ctrlContainer->getNextCtrl();
    }
    return ret;
}

QString namePropEditor::getValue()
{
    return this->ctrl->getCtrlName();
}


bool namePropEditor::isValidValue(QString newVal)
{
    QRegExp r("^[a-zA-Z_][a-zA-Z0-9_]*$");
    /* Unique name  validator */
    /* Find the top level container to search all possible containers for a control name */
    abstractUICCtrl * parent =  this->ctrl;
    while (parent->getOctaveParent() != NULL)
        parent = parent->getOctaveParent();

    return (newVal.contains(r) == 1) && (this->ctrl->canChangeName() && (!nameExists(newVal, this->ctrl, parent)));
}

void namePropEditor::setValue(QString newVal)
{
    this->ctrl->setCtrlName(newVal);
}

QWidget * namePropEditor::getEditWidget()
{
  editUpdPropWdg * edit = new editUpdPropWdg(this);
  edit->setFrame(false);
  edit->setText(this->getValue());
  connect(edit, SIGNAL(editingFinished()), this, SLOT(nameChanged()));
  return edit;
}

void namePropEditor::nameChanged()
{
    editUpdPropWdg * ed;
    ed = (editUpdPropWdg *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(nameChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {
        ed->setText(this->getValue());
    }
    ed->selectAll();
    if(ctrl->getParentWnd()->getSrcWnd()!= NULL)
    {
        ctrl->getParentWnd()->getSrcWnd()->updateWidgetsNames();
    }
    connect(ed, SIGNAL(editingFinished()), this, SLOT(nameChanged()));
}


propEditor::propEditor()
{
}

bool nameCmp(abstractPropEditor * prop1, abstractPropEditor * prop2)
{
    return prop1->getPropName().toLower() < prop2->getPropName().toLower();
}

void propEditor::registerProp(abstractPropEditor * prop)
{
    this->propertieList.push_back(prop);
    std::sort(this->propertieList.begin(), this->propertieList.end(), nameCmp);

}



abstractPropEditor * propEditor::getFirst()
{
    abstractPropEditor * ret = NULL;
    itera = this->propertieList.begin();
    if(itera != this->propertieList.end())
        ret = *itera;
    return ret;
}

abstractPropEditor * propEditor::getNext()
{
    abstractPropEditor * ret = NULL;
    itera++;
    if(itera != this->propertieList.end())
        ret = *itera;
    return ret;
}

QString propEditor::getValueByName(QString name)
{
    QList<abstractPropEditor *>::iterator ctrl;
    bool finded = false;
    QString ret = "";
    for(ctrl = this->propertieList.begin();
        ctrl != this->propertieList.end() && !finded;
        ctrl++)
        if((*ctrl)->getPropName() == name)
        {
            ret = (*ctrl)->getValue();
            finded = true;
        }
    return ret;
}

abstractPropEditor * propEditor::getPropByName(QString name)
{
    QList<abstractPropEditor *>::iterator ctrl;
    bool finded = false;
    abstractPropEditor * ret = NULL;
    for(ctrl = this->propertieList.begin();
        ctrl != this->propertieList.end() && !finded;
        ctrl++)
        if((*ctrl)->getPropName() == name)
        {
            ret = (*ctrl);
            finded = true;
        }
    return ret;
}

unsigned int propEditor::getCount()
{
    return this->propertieList.count();
}

propEditor::~propEditor()
{
    itera = this->propertieList.begin();
    while(itera != this->propertieList.end())
    {
        delete *itera;
        itera++;
    }
}

QStringList abstractUICCtrl::preCallback(QString fcnName, QStringList argName)
{
    QStringList ret;
    QString name = this->properties.getValueByName("Name");
    QString arg = this->parentWnd->getFrame()->properties.getValueByName("Name");
    QString argC = "@var{" + arg + "}";
    if(argName.count() != 0){
        arg = "";
        argC = "";
        for(int i=0; i < argName.count(); i++){
            if(i < argName.count() - 1){
              arg = arg + argName[i] + ", ";
              argC = arg + "@var{" + argName[i] + "}, ";
            }
            else{
              arg = arg + argName[i];
              argC = argC + "@var{" + argName[i] + "}";
            }
        }
    }

    if(fcnName == "")
    {
        ret.append("## @deftypefn  {} {} " + name + "_doIt (@var{src}, @var{data}, " + argC +")");
        ret.append("##");
        ret.append("## Define a callback for default action of "+name+" control.");
        ret.append("##");
        ret.append("## @end deftypefn");
        ret.append("function " + name + "_doIt(src, data, " + arg + ")");
    }
    else
    {
        if(argName.count() != 0)
          ret.append("## @deftypefn  {} {} " + fcnName + "(@var{src}, @var{data}, " + argC +")");
        else
          ret.append("## @deftypefn  {} {} " + fcnName + "(@var{src}, @var{data})");
        ret.append("##");
        ret.append("## Define a callback for default action of "+ name +" control.");
        ret.append("##");
        ret.append("## @end deftypefn");
        if(argName.count() != 0)
          ret.append("function " + fcnName + "(src, data, " + arg + ")");
        else
          ret.append("function " + fcnName + "(src, data)");
    }
    ret.append("");
    return ret;
}

QStringList abstractUICCtrl::posCallback(void)
{
    QStringList ret;
    ret.append("end");
    ret.append("");
    return ret;
}
bool abstractUICCtrl::haveCtrlWithName(QString name, abstractUICCtrl *parent){
    bool ret = false;
    bool isCont;
    abstractUICCtrl *ctrl = parent->getFirstCtrl();
    while((ctrl) && (!ret)){
        ret = ctrl->getCtrlName() == name;
        isCont = ctrl->isContainer();
        if((ctrl != this) && (isCont))
            ret = ret || haveCtrlWithName(name, ctrl);
        ctrl = parent->getNextCtrl();
    }
    return ret;
}

abstractUICCtrl * abstractUICCtrl::getFirstCtrl()
{
    abstractUICCtrl * ret = NULL;
    this->itAddedWdg = this->addedWidgets.begin();
    if(this->itAddedWdg != this->addedWidgets.end())
        ret = *this->itAddedWdg;
    return ret;
}

abstractUICCtrl * abstractUICCtrl::getNextCtrl()
{
    abstractUICCtrl * ret = NULL;
    this->itAddedWdg++;
    if(this->itAddedWdg != this->addedWidgets.end())
        ret = *this->itAddedWdg;
    return ret;
}

void abstractUICCtrl::addWidget(abstractUICCtrl *w)
{
    this->addedWidgets.push_back(w);
    this->getParentWnd()->getSrcWnd()->updateWidgetList(this->addedWidgets);
}


abstractUICCtrl::abstractUICCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg *parentWnd)
{
    this->parentContainer = parent;
    this->vAssociatedWidget = NULL;
    this->vIsContainer = false;
    this->parentWnd = parentWnd;
    this->adjTL = new adjPointTopLeftClass(parent);
    this->adjML = new adjPointMidleLeftClass(parent);
    this->adjBL = new adjPointBottomLeftClass(parent);
    this->adjTR = new adjPointTopRightClass(parent);
    this->adjMR = new adjPointMidleRightClass(parent);
    this->adjBR = new adjPointBottomRightClass(parent);
    this->adjTM = new adjPointTopMidleClass(parent);
    this->adjBM = new adjPointBottomMidleClass(parent);
    this->properties.registerProp(new namePropEditor(this));    
    this->vHaveCallback = false;
    this->octaveParent = octaveParent;
    this->setCanChangeName(true);
}

abstractUICCtrl * abstractUICCtrl::getOctaveParent()
{
    return this->octaveParent;
}

QWidget * abstractUICCtrl::getParentContainer()
{
    return this->parentContainer;
}

bool abstractUICCtrl::isAutoSize()
{
    return false;
}

QString abstractUICCtrl::getCtrlName()
{
    return this->ctrlName;
}

void abstractUICCtrl::setCtrlName(QString name)
{
    if(vCanChangeName)
      this->ctrlName = name;
}

void abstractUICCtrl::select()
{
    adjTL->show();
    adjML->show();
    adjBL->show();
    adjTR->show();
    adjMR->show();
    adjBR->show();
    adjTM->show();
    adjBM->show();
    if(this->associatedWidget())
        this->associatedWidget()->setFocus();
}

void abstractUICCtrl::deselect()
{
    adjTL->hide();
    adjML->hide();
    adjBL->hide();
    adjTR->hide();
    adjMR->hide();
    adjBR->hide();
    adjTM->hide();
    adjBM->hide();

}

abstractUICCtrl::~abstractUICCtrl()
{
    QList<abstractUICCtrl *>::iterator it;
    for(it = this->addedWidgets.begin(); it != this->addedWidgets.end(); it++)
        if((*it) != this) /* Esto es porque el frameDlg se carga como primer control */
            delete (*it);
    delete adjTL;
    delete adjML;
    delete adjBL;
    delete adjTR;
    delete adjMR;
    delete adjBR;
    delete adjTM;
    delete adjBM;
}

QWidget *abstractUICCtrl::associatedWidget()
{
    return this->vAssociatedWidget;
}

void abstractUICCtrl::updateAdjPoints()
{
    adjTL->resetPosition();
    adjML->resetPosition();
    adjBL->resetPosition();
    adjTR->resetPosition();
    adjMR->resetPosition();
    adjBR->resetPosition();
    adjTM->resetPosition();
    adjBM->resetPosition();

    adjTL->update();
    adjML->update();
    adjBL->update();
    adjTR->update();
    adjMR->update();
    adjBR->update();
    adjTM->update();
    adjBM->update();
}

const QRect abstractUICCtrl::position()
{
    QRect ret;
    if(this->vAssociatedWidget)
        ret = this->vAssociatedWidget->geometry();
    else
        ret = QRect(1,1,1,1);
    return ret;
}

bool abstractUICCtrl::haveCallBack(void)
{
    return this->vHaveCallback;
}

void abstractUICCtrl::setHaveCallBack(bool v)
{
    this->vHaveCallback = v;
}

QStringList abstractUICCtrl::getSrcCallBack()
{
    return this->srcCallBack;
}

void abstractUICCtrl::setSrcCallBack(QStringList src)
{
    this->srcCallBack = src;
//    for(i=this->srcCallBack.count()-1;i>0; i--)
//    {
//        if(this->srcCallBack[i].length() == 0)
//            this->srcCallBack.removeLast();
//        else
//            break;
//    }
}
