/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef EDUCIAACTRL_H
#define EDUCIAACTRL_H

#include <QWidget>
#include "abstractuicctrl.h"
#include <QLabel>
#include "commonproperties.h"
#include "controlgenerator.h"

class edDacValEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    QString ip;
  public:
    edDacValEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
};


class cbCIAASerialPortEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    QString serialPort;
  public:
    cbCIAASerialPortEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void cbSerialPortChanged(QString text);
};

class eduCiaaLedValueEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    unsigned int pinNumber;
  public:
    eduCiaaLedValueEditor(abstractUICCtrl *ctrl, unsigned int pinNumber);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void eduCiaaPinValueChanged(QString text);
};

class eduCIAACtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;  

public:
    static unsigned int getNameCounter();
    eduCIAACtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual ~eduCIAACtrl();
    //
    virtual QString className() { return "eduCIAACtrl";}
    virtual bool isAutoSize() { return true;}
};


class eduCIAACtrlGen: public controlGenerator
{
public:
  eduCIAACtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // eduCIAACtrl_H
