/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef GROUPPANELCTRL_H
#define GROUPPANELCTRL_H
#include <QWidget>
#include "abstractuicctrl.h"
#include <QGroupBox>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"

enum gbBorderStyle{ bsBevelIn, bsBevelOut, bsEtchedIn, bsEtchedOut, bsLine, bsNone};

enum gbTitlePos{tpCenterBottom=0, tpCenterTop=1, tpLeftBottom=2, tpLeftTop=3, tpRightBottom=4, tpRightTop=5};
const QString vGbTitlePos[] ={"centerbottom" , "centertop" , "leftbottom" , "lefttop" , "rightbottom" , "righttop"};
enum gbFontWeight {fwNormal=0, fwBold=1, fwDemi = 2, fwLight = 3};
const QString vGbFontWeight[] = {"normal", "bold", "demi", "light"};

enum gbFontAngle {faItalic=0, faNormal=1, faOblique=2};
const QString vGbgFontAngle[] = {"italic", "normal", "oblique"};

class gbStyleGenerator
{
private:
    QWidget *widget;
    int vBorderWidth;
    QColor vBackgroundColor;
    QColor vForegroundColor;
    gbBorderStyle vBorderStyle;
    gbTitlePos vTitlePos;
    QString vFontName;
    gbFontWeight vFontWeight;
    gbFontAngle vFontAngle;
    int vFontSize;
public:

    gbStyleGenerator(abstractUICCtrl *w);

    QString getStyleSheet();

    int borderWidth();
    void setBorderWidth(int w);

    QColor backgroundColor();
    void setBackgroundColor(QColor c);

    QColor foregroundColor();
    void setForegroundColor(QColor c);

    gbBorderStyle borderStyle();
    void setBorderStyle(gbBorderStyle bs);

    gbTitlePos titlePos();
    void setTitlePos(gbTitlePos p);

    QString fontName();
    void setFontName(QString f);

    gbFontWeight fontWeight();
    void setFontWight(gbFontWeight);

    gbFontAngle fontAngle();
    void setFontAngle(gbFontAngle);

    int fontSize();
    void setFontSize(int s);
};


class gbBgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class gbFgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class gbTitlePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbTitlePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
};

class gbTitlePosPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:

public:
    gbTitlePosPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void titlePosChange(const QString &txt);
};

class gbFontNamePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QFontDatabase fontDB;
public:
    gbFontNamePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontChanged(const QString &txt);
};

class gbFontSizePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbFontSizePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontSizeChanged(const QString & text);
};

class gbFontWeightPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbFontWeightPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontWeightChanged(const QString & text);
};

class gbFontAnglePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbFontAnglePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontAngleChanged(const QString & text);
};

class gbBorderWidtPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    gbBorderWidtPropEditor(abstractUICCtrl *ctrl);
    QString getValue();

    bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void borderSizeChanged(const QString & text);
};



class groupPanelCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

protected:

public:
    static unsigned int getNameCounter();
    gbStyleGenerator *styleGenerator;
    groupPanelCtrl(QWidget * parent,
                   abstractUICCtrl *octaveParent,
                   childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
     virtual QStringList generateMFile(QString path);

    virtual QString className() { return "groupPanel";}
    void addWidget(abstractUICCtrl *w);
    virtual ~groupPanelCtrl();
};

class groupPanelCtrlGen: public controlGenerator
{
public:
  groupPanelCtrlGen();
virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);

};

#endif // GROUPPANELCTRL_H
