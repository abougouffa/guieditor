/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef COMMONPROPERTIES_H
#define COMMONPROPERTIES_H
#include "abstractuicctrl.h"
#include <QFontDatabase>

class positionPropEditor: public abstractPropEditor
{
public:
    positionPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
};

class fontNamePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QFontDatabase fontDB;
public:
    fontNamePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontChanged(const QString &txt);
};

class fontSizePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    fontSizePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontSizeChanged(const QString & text);
};

class fontWeightPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    fontWeightPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontWeightChanged(const QString & text);
};

class fontAnglePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    fontAnglePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontAngleChanged(const QString & text);
};

class toolTipPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QString toolTipString;
public:
    toolTipPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
};

class callBackPropEditor: public abstractPropEditor
{
  Q_OBJECT
public:
    callBackPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual bool canGenerateCode();
    virtual QWidget * getEditWidget();
public slots:
  virtual void callBackChanged(int state);
};



#endif // COMMONPROPERTIES_H
