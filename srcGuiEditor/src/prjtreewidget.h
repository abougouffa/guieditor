/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef PRJTREEWIDGET_H
#define PRJTREEWIDGET_H

#include <QTreeWidget>
#include <QTreeWidgetItem>


class abstractPrjTreeItem : public QTreeWidgetItem, public QObject
{

public:
    abstractPrjTreeItem(const QTreeWidgetItem &other):QTreeWidgetItem(other){ this->setExpanded(true);}
    abstractPrjTreeItem(QTreeWidgetItem *parent, QTreeWidgetItem *preceding, int type = Type) : QTreeWidgetItem(parent, preceding, type){this->setExpanded(true);}

    abstractPrjTreeItem(QTreeWidgetItem *parent, const QStringList &strings, int type = Type):QTreeWidgetItem(parent, strings, type ){this->setExpanded(true);}
        abstractPrjTreeItem(QTreeWidgetItem *parent, int type = Type):QTreeWidgetItem(parent, type){this->setExpanded(true);}
        abstractPrjTreeItem(QTreeWidget *parent, QTreeWidgetItem *preceding, int type = Type):QTreeWidgetItem(parent, preceding, type ){this->setExpanded(true);}
        abstractPrjTreeItem(QTreeWidget *parent, const QStringList &strings, int type = Type):QTreeWidgetItem(parent, strings, type){this->setExpanded(true);}
        abstractPrjTreeItem(QTreeWidget *parent, int type = Type):QTreeWidgetItem(parent, type){this->setExpanded(true);}
        abstractPrjTreeItem(const QStringList &strings, int type = Type):QTreeWidgetItem(strings, type ){this->setExpanded(true);}
        abstractPrjTreeItem(int type = Type):QTreeWidgetItem(type){this->setExpanded(true);}

    virtual void populateMenu(QMenu &) = 0;
    virtual void activate() = 0;
    virtual void selectProject() = 0;
};


class prjTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit prjTreeWidget(QWidget *parent = 0);

signals:

public slots:
    void showContextMenu(const QPoint &pos);
};

#endif // PRJTREEWIDGET_H
