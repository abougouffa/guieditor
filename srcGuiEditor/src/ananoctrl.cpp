/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "ananoctrl.h"

#include "editableANano.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include <QSerialPortInfo>
expOctClass::expOctClass(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "exportClass";
    this->expClass = true;
}

QString expOctClass::getValue()
{
    QString ret;
    ret = "false";
    if(this->expClass)
        ret = "true";
    return ret;
}
bool expOctClass::isValidValue(QString newVal)
{
    return (newVal.toLower() == "true") || (newVal.toLower() == "false");
}
void expOctClass::setValue(QString newVal)
{
    this->expClass=(newVal.toLower() == "true");
}

QString expOctClass::generateCode()
{
    return "";
}

bool expOctClass::canGenerateCode()
{
    return false;
}

QWidget * expOctClass::getEditWidget()
{
    checkBoxUpdPropWdg * cb = new checkBoxUpdPropWdg(this);
    cb->setChecked(this->expClass);
    connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    return cb;
}

void expOctClass::callBackChanged(int state)
{
    checkBoxUpdPropWdg * cb = (checkBoxUpdPropWdg *)QObject::sender();
    if(state == Qt::Unchecked)
    {
        this->setValue("false");
    }
    else if(state == Qt::Checked)
    {
        this->setValue("true");
    }
    else
    {
        disconnect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
        if(this->getValue().toLower() == "true")
            cb->setChecked(true);
        else
            cb->setChecked(false);
        connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    }
}


cbArduinoSerialPortEditor::cbArduinoSerialPortEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "serialPort";
}

QString cbArduinoSerialPortEditor::getValue()
{
    editableANano * cb;
    QString ret = "";
    cb = (editableANano * )this->ctrl->associatedWidget();
    ret = cb->getSerialPortName();
    return ret;
}

bool cbArduinoSerialPortEditor::isValidValue(QString newVal __attribute__((unused)))
{
    return true;
}

void cbArduinoSerialPortEditor::setValue(QString newVal)
{
    editableANano * cb;
    cb = (editableANano * )this->ctrl->associatedWidget();
    cb->setSerialPortName(newVal);
}

QString cbArduinoSerialPortEditor::generateCode()
{
#ifdef Q_OS_LINUX
    QString ret = "'" + this->propName + "', '/dev/" + this->getValue() + "'";
#endif

#ifdef Q_OS_MAC
    QString ret = "'" + this->propName + "', '" + this->getValue() + "'";
#endif

#ifdef Q_OS_WIN
    QString ret = "'" + this->propName + "', '\\\\.\\" + this->getValue() + "'";
#endif
    return ret;
}

QWidget * cbArduinoSerialPortEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();

    foreach (QSerialPortInfo info, ports)
    {
        if(info.portName().length() > 0)
          cb->addItem(info.portName());
    }
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbSerialPortChanged(QString)));
    return cb;
}

void cbArduinoSerialPortEditor::cbSerialPortChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbSerialPortChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbSerialPortChanged(QString)));
    }
}


cbPinTypeEditor::cbPinTypeEditor(abstractUICCtrl *ctrl, int pinNumber, bool isReadOnly):
    abstractPropEditor(ctrl)
{
    this->pinIsReadOnly = isReadOnly;
    this->pinNumber = pinNumber;
    this->propName = "pinDir" + QString::number(pinNumber);
}

QString cbPinTypeEditor::getValue()
{
    editableANano * cb;
    QString ret = "none";
    cb = (editableANano * )this->ctrl->associatedWidget();
    if(cb->getPinType(this->pinNumber) == input)
        ret = "input";
    else if(cb->getPinType(this->pinNumber) == inputPullUp)
        ret = "inputPullUp";
    if(cb->getPinType(this->pinNumber) == output)
        ret = "output";
    return ret;
}

bool cbPinTypeEditor::isValidValue(QString newVal)
{
    if (this->pinIsReadOnly)
      return ((newVal == "input") || (newVal == "none"));
    else
      return (newVal == "input")||(newVal == "output")||(newVal == "inputPullUp")|| (newVal == "none");
}

void cbPinTypeEditor::setValue(QString newVal)
{
    editableANano * eNano;
    eNano = (editableANano * )this->ctrl->associatedWidget();
    if(newVal == "input")
        eNano->setPinType(this->pinNumber, input);
    else if(newVal == "inputPullUp")
        eNano->setPinType(this->pinNumber, inputPullUp);
    else if(newVal == "output")
        eNano->setPinType(this->pinNumber, output);
    else
        eNano->setPinType(this->pinNumber, none);
}

QString cbPinTypeEditor::generateCode()
{
    QString ret;
    editableANano * eNano;
    eNano = (editableANano * )this->ctrl->associatedWidget();
    if (eNano->getPinType(this->pinNumber) != none)
      ret = "'" + this->propName + "', '" + this->getValue() + "'";
    return ret;
}

QWidget * cbPinTypeEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("none");
    cb->addItem("input");
    cb->addItem("inputPullUp");
    if (!this->pinIsReadOnly)
      cb->addItem("output");   
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbPinDirChanged(QString)));
    return cb;
}

void cbPinTypeEditor::cbPinDirChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbPinDirChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbPinDirChanged(QString)));
    }
}


unsigned int aNanoCtrl::uicNameCounter = 0;

unsigned int aNanoCtrl::getNameCounter()
{
    return uicNameCounter;
}

aNanoCtrl::aNanoCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):
    abstractUICCtrl(parent, octaveParent, parentWnd)
{
    int i;
    editableANano * img = new editableANano(parent, this);
    uicNameCounter++;
    this->setCtrlName("aNano_"+QString::number(uicNameCounter));
    img->resize(52, 52);

    this->setAssociatedWidget(img);
    this->adjTL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjML->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjMR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTM->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBM->asociateCtrl(this->getAssociatedWidget(), false);
    this->properties.registerProp(new cbArduinoSerialPortEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    for(i=2; i <= 19; i++)
    {
      this->properties.registerProp(new cbPinTypeEditor(this, i));
    }
    this->properties.registerProp(new cbPinTypeEditor(this, 20, true));
    this->properties.registerProp(new cbPinTypeEditor(this, 21, true));
    this->properties.registerProp(new expOctClass(this));
}

void aNanoCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableANano * nano = dynamic_cast<editableANano *>(this->associatedWidget());
    nano->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList aNanoCtrl::generateMFile(QString path)
{
    QStringList ret;
    abstractPropEditor * prop;
    QString lineMCode;
    int i;
    QDir otherDir(path);
    otherDir.cdUp();
    otherDir.mkdir("firmware");
    otherDir.cd("firmware");
    QStringList arduinoFiles;
    if(this->properties.getPropByName("exportClass")->getValue() == "true")
    {
        QDir libDir(path);
        libDir.cdUp();
        libDir.cd("libs");
        QFile::copy(":/ctrlAduino/octaveCtrls/serialCfg_def.m",
                    libDir.path() + QDir::separator() + "serialCfg_def.m");
        QFile(libDir.path() + QDir::separator() + "serialCfg_def.m").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);

        QFile::copy(":/ctrlAduino/octaveCtrls/serialCfg.m",
                    libDir.path() + QDir::separator() + "serialCfg.m");
        QFile(libDir.path() + QDir::separator() + "serialCfg.m").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);

        libDir.mkdir("@arduinoNano");
        libDir.cd("@arduinoNano");
        arduinoFiles << "aNanoAnalogRead.m";
        arduinoFiles << "aNanoAnalogWrite.m";
        arduinoFiles << "aNanoDigitalRead.m";
        arduinoFiles << "aNanoDigitalWrite.m";
        arduinoFiles << "aNanoSetPinType.m";
        arduinoFiles << "arduinoNano.m";
        arduinoFiles << "aNanoClose.m";
        arduinoFiles << "display.m";
        arduinoFiles << "aNanoServoAttach.m";
        arduinoFiles << "aNanoServoDetach.m";
        arduinoFiles << "aNanoServoRead.m";
        arduinoFiles << "aNanoServoWrite.m";
        for(i = 0; i < arduinoFiles.count(); i++){
            QFile::copy(":/ctrlAduino/octaveCtrls/@arduinoNano/" + arduinoFiles[i],
                        libDir.path() + QDir::separator() + arduinoFiles[i]);
            QFile(libDir.path() + QDir::separator() + arduinoFiles[i]).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
        }
    }
    QFile::copy(":/ctrlAduino/octaveCtrls/arduino_cfw.tar.gz",
                otherDir.path() + QDir::separator() + "arduino_cfw.tar.gz");
    QFile(otherDir.path() + QDir::separator() + "arduino_cfw.tar.gz").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);

    ret.append("  " + this->properties.getValueByName("Name") + " = arduinoNano( ...") ;
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = prop->generateCode();
          if(lineMCode.length() > 0)
            ret.append("\t" + lineMCode );
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList aNanoCtrl::createCallBack(QString path __attribute__((unused)))
{
  QStringList ret;
  return ret;
}

aNanoCtrl::~aNanoCtrl()
{
    delete this->getAssociatedWidget();
}

aNanoCtrlGen::aNanoCtrlGen():controlGenerator("aNanoCtrl")
{
}

abstractUICCtrl * aNanoCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    aNanoCtrl * aNano = new aNanoCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = aNano->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = aNano->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      aNano->setSrcCallBack(src.split('\n'));

    aNano->deselect();
    return aNano;
}

