/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include "framedlg.h"
#include <QLabel>
#include "buttonctrl.h"
#include "radiobuttonctrl.h"
#include "checkboxctrl.h"
#include "editctrl.h"
#include "togglectrl.h"
#include "textctrl.h"
#include "sliderctrl.h"
#include "framectrl.h"
#include "cmbctrl.h"
#include "listctrl.h"
#include "imgctrl.h"
#include "ananoctrl.h"
#include "confirmwnd.h"
#include "storagemanager.h"
#include <QFileDialog>
#include <QMdiSubWindow>
#include <QTextStream>
#include <QMessageBox>
#include <QDebug>
#include "wdgdebug.h"
#include "ui_wdgdebug.h"
#include <QFileInfo>
#include "aboutdlg.h"
#include "nodeMCUctrl.h"
#include "eduCIAACtrl.h"
#include "childwnddlg.h"
#include "grouppanelctrl.h"
#include "buttongroupctrl.h"
#include "childwndmfile.h"

#include "newprojectwnd.h"
#include "ui_newprojectwnd.h"

#include "childwndtextfile.h"

#include <QStandardPaths>
#include "callbackctrl.h"

#include <QDockWidget>
#include <QHeaderView>

wdgProp *mainWnd::propPanel = NULL;

wdgDebug * mainWnd::debugPanel= NULL;
wdgSrcEditor * mainWnd::srcEdPanel= NULL;
wdgPrjMan * mainWnd::prjManPanel= NULL;
mainWnd * mainWnd::mainWndPtr = NULL;
pkgGenWnd * mainWnd::pkgWnd = NULL;

wdgProp * mainWnd::getPropPan()          { return mainWnd::propPanel;  }
wdgDebug * mainWnd::getDebugWnd()   { return mainWnd::debugPanel;}
wdgSrcEditor * mainWnd::getSrcWnd()         { return mainWnd::srcEdPanel;     }
wdgPrjMan *mainWnd::getPrjWnd()        { return mainWnd::prjManPanel;     }
mainWnd * mainWnd::getMainWnd()          { return mainWnd::mainWndPtr; }
pkgGenWnd * mainWnd::getPkgGenWnd()      { return mainWnd::pkgWnd;     }

mainWnd::mainWnd(configSettings *appCfg, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mainWnd)
{
    ui->setupUi(this);

    cfg = appCfg;
    mainWndPtr = this;
    setCentralWidget(ui->mdiArea);
    this->setMouseTracking(true);

    /*Creamos los paneles dockeables*/
    dockPrj = new QDockWidget(tr("Project Manager"), this);
    dockPrj->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    prjManPanel = new wdgPrjMan(dockPrj);
    dockPrj->setWidget(prjManPanel);
    addDockWidget(Qt::RightDockWidgetArea, dockPrj);

    dockProp = new QDockWidget(tr("Properties"), this);
    dockProp->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    propPanel = new wdgProp(dockProp);
    propPanel->getTblProp()->horizontalHeader()->setStretchLastSection(true);
    propPanel->getTblProp()->setSelectionMode(QTableWidget::NoSelection);
    dockProp->setWidget(propPanel);
    addDockWidget(Qt::RightDockWidgetArea, dockProp);

    dockDbg = new QDockWidget(tr("Octave console"), this);
    this->octaveExec.setParent(this);
    dockDbg->setAllowedAreas(Qt::BottomDockWidgetArea | Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    debugPanel = new wdgDebug(&this->octaveExec, dockDbg);
    dockDbg->setWidget(debugPanel);
    addDockWidget(Qt::BottomDockWidgetArea, dockDbg);

    dockSrc = new QDockWidget(tr("Source code editor"), this);
    dockSrc->setAllowedAreas(Qt::BottomDockWidgetArea | Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    srcEdPanel = new wdgSrcEditor(this, ui->mdiArea);
    dockSrc->setWidget(srcEdPanel);
    addDockWidget(Qt::BottomDockWidgetArea, dockSrc);
    /*Fin creación paneles dockeables*/

    this->uicState = uicStateNone;
    this->toAdd = NULL;

    this->pkgWnd = new pkgGenWnd(this);
    this->pkgWnd ->setAttribute(Qt::WA_DeleteOnClose, false);
    this->pkgWnd->setWindowFlags(this->pkgWnd->windowFlags() | Qt::WindowStaysOnTopHint);

    connect(&octaveExec, SIGNAL(readyReadStandardError()), debugPanel, SLOT(showStdErr()));
    connect(&octaveExec, SIGNAL(readyReadStandardOutput()), debugPanel, SLOT(showStdOut()));
    QStringList arg;
    if(cfg->octavePathOk() && cfg->tmpDirOk())
    {
      QFileInfo fInfo(cfg->getOctavePath());
      this->debugPanel->ui->lstConsoleOut->addItem(tr("Starting octave..."));
      QString cmd = fInfo.path();
      arg << "--path";
      arg << cfg->getTempDir();
      for(int i = 0; i < cfg->getLibPaths().count(); i++)
      {
          arg << "--path";
          arg << cfg->getLibPaths()[i];
      }
      octaveExec.setWorkingDirectory(cmd);

      if (cfg->getQt5PlugginPath().length() > 0)
      {
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        env.insert("QT_PLUGIN_PATH", cfg->getQt5PlugginPath());
        octaveExec.setProcessEnvironment(env);
      }

      octaveExec.start(cfg->getOctavePath(), arg, QIODevice::ReadWrite);

      octaveExec.setTextModeEnabled(true);
      octaveExec.waitForStarted();
      if(octaveExec.isOpen())
      {
        this->debugPanel->ui->lstConsoleOut->addItem(tr("started."));
        cmd = "cd " + cfg->getTempDir() + ";\n";
        octaveExec.write(cmd.toLatin1());
      }
    }
    this->showMaximized();
}


mainWnd::~mainWnd()
{    
    abstractChildWnd* dlg;
    QList<QMdiSubWindow *> listDlg;
    QList<QMdiSubWindow *>::Iterator itDlg;
    //ui->mdiArea->closeAllSubWindows();
    listDlg = ui->mdiArea->subWindowList();
    for(itDlg = listDlg.begin(); itDlg != listDlg.end(); itDlg++)
    {
        dlg = dynamic_cast<abstractChildWnd *>(*itDlg);
        dlg->setAttribute(Qt::WA_DeleteOnClose, false);
        dlg->close();
        delete dlg;
    }

    this->pkgWnd->close();
    delete this->pkgWnd;

    if(this->octaveExec.isOpen())
        this->octaveExec.close();

    delete ui;
}

void mainWnd::runScript(QString fn, QStringList pathsToAdd)
{
    QStringList arg;

    QString cmd;
    int i;

    if(cfg->octavePathOk() && cfg->tmpDirOk())
    {
        QFileInfo fInfo(cfg->getOctavePath());
        QFileInfo scriptFile(fn);
        if(octaveExec.isOpen())
            octaveExec.close();
        disconnect(&octaveExec, SIGNAL(readyReadStandardError()), debugPanel, SLOT(showStdErr()));
        disconnect(&octaveExec, SIGNAL(readyReadStandardOutput()), debugPanel, SLOT(showStdOut()));


        this->debugPanel->ui->lstConsoleOut->clear();

        connect(&octaveExec, SIGNAL(readyReadStandardError()), debugPanel, SLOT(showStdErr()));
        connect(&octaveExec, SIGNAL(readyReadStandardOutput()), debugPanel, SLOT(showStdOut()));


        this->debugPanel->ui->lstConsoleOut->addItem(tr("Starting octave..."));

        cmd = fInfo.path();
        arg << "--path";
        arg << scriptFile.absolutePath();

        for(i = 0; i < pathsToAdd.count(); i++)
        {
            arg << "--path";
            arg << pathsToAdd[i];
        }
        octaveExec.setWorkingDirectory(cmd);

        if (cfg->getQt5PlugginPath().length() > 0)
        {
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            env.insert("QT_PLUGIN_PATH", cfg->getQt5PlugginPath());
            octaveExec.setProcessEnvironment(env);
        }

        octaveExec.start(cfg->getOctavePath(), arg,QIODevice::ReadWrite);
        octaveExec.setTextModeEnabled(true);
        octaveExec.waitForStarted();
        if(octaveExec.isOpen())
        {
            this->debugPanel->ui->lstConsoleOut->addItem(tr("started."));
            cmd = "cd '" + scriptFile.absolutePath() + "';\n";
            octaveExec.write(cmd.toLatin1());

            cmd = "runApp;\n";

            octaveExec.write(cmd.toLatin1());
        }
    }
    else
    {
        //Show message falta configurar
        QMessageBox::information(this, tr("Bad configuration"), tr("To run this script is neccesary configure the temporary directory and the octave path from the File/Property menu"));
    }
}



configSettings * mainWnd::getAppConfig()
{
    return this->cfg;
}

void mainWnd::on_actNew_triggered()
{
    childWndDlg* dlg = new childWndDlg(ui->mdiArea, 0, this->srcEdPanel, this->propPanel);
    dlg->setAttribute(Qt::WA_DeleteOnClose);
    dlg->setWindowTitle("Documento " + QString::number(frameDlg::getFormNumber()) + "[*]");
    dlg->setWindowModified(false);
    ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
    dlg->setGeometry(1, 1, 250, 100);
    dlg->show();
}

void mainWnd::on_actAddBtn_triggered()
{
    buttonCtrl * btn;
    frameDlg * frm;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    if(dlg)
    {
        frm = dlg->getFrame();
        btn = new buttonCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = btn;
    }
}


void mainWnd::on_actExit_triggered()
{
    close();
}

void mainWnd::closeEvent(QCloseEvent *e)
{
    confirmWnd confirmDlg(this);
    bool allSaved = true;
    QList<QMdiSubWindow *> wndList;
    QList<QMdiSubWindow *>::const_iterator wnd;
    abstractChildWnd *dlg;
    wndList = ui->mdiArea->subWindowList();

    for(wnd = wndList.begin();
        wnd != wndList.end();
        wnd++)
    {
        dlg = dynamic_cast<abstractChildWnd*>(*wnd);
        if(dlg->isWindowModified())
           allSaved = false;
    }
    if(!allSaved)
    {
        confirmDlg.exec();
        if(confirmDlg.result() == QDialog::Rejected)
        {
            e->setAccepted(false);
        }
        else
        {
            for(wnd = wndList.begin();
                wnd != wndList.end();
                wnd++)
            {
                dlg = dynamic_cast<abstractChildWnd*>(*wnd);
                dlg->setWindowModified(false);
            }
            e->setAccepted(true);
        }
    }
}

void mainWnd::on_actAddLineEdit_triggered()
{
    editCtrl * edit;
    frameDlg * frm;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    if(dlg)
    {
        frm = dlg->getFrame();
        edit = new editCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = edit;
    }
}

void mainWnd::on_actAddLabel_triggered()
{
    textCtrl *txt;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        txt = new textCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = txt;
    }
}

void mainWnd::on_actViewPropEdit_triggered()
{
    dockProp->show();
}

void mainWnd::on_actSave_triggered()
{
    abstractChildWnd *dlg = dynamic_cast<abstractChildWnd *>(ui->mdiArea->activeSubWindow());
    if(dlg)
    {
        if(this->srcEdPanel)
          this->srcEdPanel->saveSourceCode();
        dlg->save();
    }
}

void mainWnd::on_actSaveAs_triggered()
{
    abstractChildWnd *dlg = dynamic_cast<abstractChildWnd *>(ui->mdiArea->activeSubWindow());
    if(dlg)
    {
        if(this->srcEdPanel)
          this->srcEdPanel->saveSourceCode();
        dlg->saveAs();
    }
}

void mainWnd::on_actLoad_triggered()
{        
    childWndDlg* dlg;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Select dialog to open"), QApplication::applicationDirPath(), tr("XML Dialogs (*.xml);;All files (*);;"), 0);
    if(!fileName.isNull())
    {
        dlg = new childWndDlg(ui->mdiArea, 0, this->srcEdPanel, this->propPanel);
        dlg->setAttribute(Qt::WA_DeleteOnClose);


        dlg->load(fileName);
        ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);

        dlg->move(1, 1);
        dlg->show();
    }

}

void mainWnd::on_actExport_triggered()
{     
    QString path;
    abstractChildWnd * dlg = dynamic_cast<abstractChildWnd *>(ui->mdiArea->activeSubWindow());
    if(dlg)
      path = QFileDialog::getExistingDirectory(this, tr("Target folder"), QApplication::applicationDirPath());
    if(!path.isEmpty() && dlg)
    {
        if(this->srcEdPanel)
          this->srcEdPanel->saveSourceCode();
        dlg->exportAsScript(path);
    }
}

void mainWnd::on_actViewSrc_triggered()
{
    dockSrc->show();
}

void mainWnd::on_actAddCombo_triggered()
{
    cmbCtrl *cmb;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        cmb = new cmbCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = cmb;
    }
}

void mainWnd::on_actAddList_triggered()
{
    listCtrl *list;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        list = new listCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = list;
    }
}



void mainWnd::on_actAddCheckBox_triggered()
{
    checkBoxCtrl *cbCtrl;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        cbCtrl = new checkBoxCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = cbCtrl;
    }
}

void mainWnd::on_actAddRbBtn_triggered()
{
    radioButtonCtrl *rbCtrl;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        rbCtrl = new radioButtonCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = rbCtrl;
    }
}

void mainWnd::on_actAddToggle_triggered()
{
    toggleCtrl *tggl;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        tggl = new toggleCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = tggl;
    }
}

void mainWnd::on_actAddSlider_triggered()
{
    sliderCtrl *sld;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        sld = new sliderCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = sld;
    }
}

void mainWnd::on_actAddImg_triggered()
{
    imgCtrl *img;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        img = new imgCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = img;
    }
}



void mainWnd::on_actProperties_triggered()
{
    propertyWndClass * wnd = new propertyWndClass();
    cfg->paramToWnd(wnd);
    if(wnd->exec())
    {
        cfg->wndToParam(wnd);
        cfg->saveCfg();
    }
    delete wnd;
}

void mainWnd::on_actRun_triggered()
{
    QStringList arg;
    QFile f;
    QString cmd;        
    int i;

    abstractChildWnd *dlg = dynamic_cast<abstractChildWnd *>(ui->mdiArea->activeSubWindow());
    if(cfg->octavePathOk() && cfg->tmpDirOk())
    {
      QFileInfo fInfo(cfg->getOctavePath());
      if(dlg)
      {
          if(this->srcEdPanel)
              this->srcEdPanel->saveSourceCode();
          dlg->run(cfg->getTempDir());

            if(octaveExec.isOpen())
                octaveExec.close();
            disconnect(&octaveExec, SIGNAL(readyReadStandardError()), debugPanel, SLOT(showStdErr()));
            disconnect(&octaveExec, SIGNAL(readyReadStandardOutput()), debugPanel, SLOT(showStdOut()));
            //disconnect(octaveExec, SIGNAL(readyRead()), debugOutWnd, SLOT(availData()));
            this->debugPanel->ui->lstConsoleOut->clear();

            connect(&octaveExec, SIGNAL(readyReadStandardError()), debugPanel, SLOT(showStdErr()));
            connect(&octaveExec, SIGNAL(readyReadStandardOutput()), debugPanel, SLOT(showStdOut()));
            //connect(octaveExec, SIGNAL(readyRead()), debugOutWnd, SLOT(availData()));
            this->debugPanel->ui->lstConsoleOut->addItem(tr("Starting octave..."));
            cmd = fInfo.path();
            arg << "--path";
            arg << cfg->getTempDir();
            for(i = 0; i < cfg->getLibPaths().count(); i++)
            {
                arg << "--path";
                arg << cfg->getLibPaths()[i];
            }
            octaveExec.setWorkingDirectory(cmd);

            if (cfg->getQt5PlugginPath().length() > 0)
            {
              QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
              env.insert("QT_PLUGIN_PATH", cfg->getQt5PlugginPath());
              octaveExec.setProcessEnvironment(env);
            }

            octaveExec.start(cfg->getOctavePath(), arg,QIODevice::ReadWrite);
            octaveExec.setTextModeEnabled(true);
            octaveExec.waitForStarted();
            if(octaveExec.isOpen())
            {
                this->debugPanel->ui->lstConsoleOut->addItem(tr("started."));
                cmd = "cd " + cfg->getTempDir() + ";\n";
                octaveExec.write(cmd.toLatin1());
                cmd = "runApp;\n";
                octaveExec.write(cmd.toLatin1());
            }
      }
    }
    else
    {
        //Show message falta configurar
        QMessageBox::information(this, tr("Bad configuration"), tr("To run this script is neccesary configure the temporary directory and the octave path from the File/Property menu"));
    }
}

void mainWnd::on_actStop_triggered()
{
    if(octaveExec.isOpen())
        octaveExec.close();
}

void mainWnd::on_actViewOctCon_triggered()
{
    dockDbg->show();
}

void mainWnd::on_actAbout_triggered()
{
    aboutDlg wnd(this);
    wnd.exec();
}

void mainWnd::on_addANano_triggered()
{
    aNanoCtrl *nano;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        nano = new aNanoCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = nano;
    }
}

void mainWnd::on_actAddNodeMCU_triggered()
{
    nodeMCUCtrl *node;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        node = new nodeMCUCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = node;
    }
}

void mainWnd::on_addEduCIAA_triggered()
{
    eduCIAACtrl *eduCiaa;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        eduCiaa = new eduCIAACtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = eduCiaa;
    }
}

void mainWnd::on_actAddFrame_triggered()
{
    frameCtrl *frmCtrl;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        frmCtrl = new frameCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = frmCtrl;
    }
}

void mainWnd::on_actAddGroupBox_triggered()
{
    groupPanelCtrl *gp;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        gp = new groupPanelCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = gp;
    }
}

void mainWnd::on_actViewProject_triggered()
{
  dockPrj->show();
}

void mainWnd::on_actNewProject_triggered()
{
    newProjectWnd * newWnd = new newProjectWnd(this);
    if(newWnd->exec() == QDialog::Accepted)
    {
        guiProject * prj = new guiProject(newWnd->ui->leProjectPath->text() + "/" + newWnd->ui->leProjectName->text(), newWnd->ui->leProjectName->text());
        prj->setVersion(newWnd->ui->sbVerA->value(), newWnd->ui->sbVerB->value(), newWnd->ui->sbVerC->value());
        storageManager::saveProject(prj);
        prjManPanel->addProject(prj);
    }
    delete newWnd;
}

void mainWnd::on_actOpenProject_triggered()
{
    QString prjName = QFileDialog::getOpenFileName(this, tr("Open gui project"),QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)[0],tr("GUI Project files (*.prj);;All files (*.*);;"));
    if(!prjName.isNull())
    {
        guiProject * prj = storageManager::loadProject(prjName);
        prjManPanel->setActiveProject(prj);
        prjManPanel->addProject(prj);
    }
}

void mainWnd::on_actRunPrj_triggered()
{
    if (prjManPanel->activeProject())
        prjManPanel->activeProject()->run();
}

void mainWnd::on_actSaveProject_triggered()
{
    if (prjManPanel->activeProject())
        prjManPanel->activeProject()->save();
}


void mainWnd::on_actClosePrj_triggered()
{
    prjManPanel->closeActiveProject();
}

void mainWnd::on_actAddCallBack_triggered()
{
    callBackCtrl *cb;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        cb = new callBackCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = cb;
    }
}

void mainWnd::on_actAddBtnGrp_triggered()
{
    buttonGroupCtrl *bg;
    childWndDlg *dlg = dynamic_cast<childWndDlg *>(ui->mdiArea->activeSubWindow());
    frameDlg * frm;
    if(dlg)
    {
        frm = dlg->getFrame();
        bg = new buttonGroupCtrl(frm, frm, dlg);
        this->uicState = uicStateAddWidget;
        this->toAdd = bg;
    }
}
