/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "buttongroupctrl.h"
#include "editablebuttongroup.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include <QStyleFactory>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QHeaderView>
#include <QPushButton>
#include <QRadioButton>

bgStyleGenerator::bgStyleGenerator(abstractUICCtrl *w)
{
    QFontDatabase fontDB;
    this->widget = w->associatedWidget();
    this->widget->setFont(mainWnd::getMainWnd()->getAppConfig()->getCtrlFont() );

    this->vFontName = this->widget->font().family();
    this->vFontSize = this->widget->font().pointSize();

    if(this->widget->font().bold())
        this->vFontWeight = bgWBold;
    else
        this->vFontWeight = bgWNormal;

    if(this->widget->font().italic())
        this->vFontAngle = bgItalic;
    else
         this->vFontAngle = bgNormal;

    this->vBackgroundColor = this->widget->palette().background().color();
    this->vBorderStyle = bgEtchedIn;
    this->vBorderWidth = 1;
    this->vForegroundColor = this->widget->palette().foreground().color();
    this->vTitlePos = bgLeftTop;


    this->widget->setStyleSheet(this->getStyleSheet());
}

QString bgStyleGenerator::getStyleSheet()
{
    QString ret;
    QString color;

    ret = ret + "QGroupBox {";

    color.sprintf("%02X%02X%02X", vBackgroundColor.red(), vBackgroundColor.green(), vBackgroundColor.blue());
    ret = ret + "background-color: #" + color + ";";

    ret = ret + "border-width: " + QString::number(this->vBorderWidth) + "px;";
    ret = ret + "border-top-color: gray;";
    ret = ret + "border-left-color: gray;";
    ret = ret + "border-bottom-color: gray;";
    ret = ret + "border-right-color: gray;";
    ret = ret + "border-style: solid;";
    ret = ret + "border-radius: 0px;";
    ret = ret + "padding: 20px;";
    ret = ret + "margin-top:10px;";
    ret = ret + "margin-bottom:0px;";

    switch(vTitlePos)
    {
      case bgLeftTop:case bgCenterTop:case bgRightTop:
        ret = ret + "margin-top:10px;";
        ret = ret + "margin-bottom:0px;";
        break;

      case bgLeftBottom:case bgCenterBottom:case bgRightBottom:
        ret = ret + "margin-top:0px;";
        ret = ret + "margin-bottom:10px;";
        break;
    }

    ret = ret + "font-family:" + this->vFontName + ";";
    ret = ret + "font-weight:" + bgFontWeight[this->vFontWeight] + ";";
    ret = ret + "font-size:" + QString::number(this->vFontSize) + "pt;";
    ret = ret + "font-style:"+ bgFontAngle[this->vFontAngle] + ";";
    ret = ret + "}";

    ret = ret + "QGroupBox::title {";

    color.sprintf("%02X%02X%02X", vForegroundColor.red(), vForegroundColor.green(), vForegroundColor.blue());
    ret = ret + "color: #" + color + ";";

    ret = ret + "subcontrol-origin: border;";
    switch(vTitlePos)
    {
      case bgLeftTop:
        ret = ret + "subcontrol-position: top left;";
        ret = ret + "left:5px;";
        ret = ret + "top:-8px;";        
        break;
      case bgCenterTop:
        ret = ret + "subcontrol-position: top center;";
        ret = ret + "top:-8px;";
        break;
      case bgRightTop:
        ret = ret + "subcontrol-position: top right;";
        ret = ret + "top:-8px;";
        ret = ret + "right:10px;";
        break;
      case bgLeftBottom:
        ret = ret + "subcontrol-position: bottom left;";
        ret = ret + "left:5px;";
        ret = ret + "bottom:-8px;";
        break;
      case bgCenterBottom:
        ret = ret + "subcontrol-position: bottom center;";
        ret = ret + "bottom:-8px;";
        break;
      case bgRightBottom:
        ret = ret + "subcontrol-position: bottom right;";
        ret = ret + "bottom:-8px;";
        ret = ret + "right:10px;";
        break;
    }


    ret = ret + "font-family:" + this->vFontName + ";";

    ret = ret + "}";
    return ret;
}

int bgStyleGenerator::borderWidth(){
    return this->vBorderWidth;
}
void bgStyleGenerator::setBorderWidth(int w) {
    this->vBorderWidth = w;
    this->widget->setStyleSheet(this->getStyleSheet());
}

QColor bgStyleGenerator::backgroundColor(){
    return this->vBackgroundColor;
}
void bgStyleGenerator::setBackgroundColor(QColor c) {
    this->vBackgroundColor = c;
    this->widget->setStyleSheet(this->getStyleSheet());
}

QColor bgStyleGenerator::foregroundColor(){
    return this->vForegroundColor;
}
void bgStyleGenerator::setForegroundColor(QColor c){
    this->vForegroundColor = c;
    this->widget->setStyleSheet(this->getStyleSheet());
}

bgBsBorderStyle bgStyleGenerator::borderStyle(){
    return this->vBorderStyle;
}
void bgStyleGenerator::setBorderStyle(bgBsBorderStyle bs) {
    this->vBorderStyle = bs;
    this->widget->setStyleSheet(this->getStyleSheet());
}

bgTpTitlePos bgStyleGenerator::titlePos(){
    return this->vTitlePos;
}
void bgStyleGenerator::setTitlePos(bgTpTitlePos p) {
    this->vTitlePos = p;
    this->widget->setStyleSheet(this->getStyleSheet());
}

QString bgStyleGenerator::fontName(){
    return this->vFontName;
}

void bgStyleGenerator::setFontName(QString f){
    this->vFontName = f;
    this->widget->setStyleSheet(this->getStyleSheet());
}

bgFwFontWeight bgStyleGenerator::fontWeight()
{
    return this->vFontWeight;
}

void bgStyleGenerator::setFontWight(bgFwFontWeight fw)
{
    this->vFontWeight = fw;
    this->widget->setStyleSheet(this->getStyleSheet());
}

bgFaFontAngle bgStyleGenerator::fontAngle()
{
    return this->vFontAngle;
}

void bgStyleGenerator::setFontAngle(bgFaFontAngle fa)
{
    this->vFontAngle = fa;
    this->widget->setStyleSheet(this->getStyleSheet());
}


int bgStyleGenerator::fontSize(){
    return this->vFontSize;
}

void bgStyleGenerator::setFontSize(int s) {
    this->vFontSize = s;
    this->widget->setStyleSheet(this->getStyleSheet());
}


bgBgClrPropEditor::bgBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";    
    ctrl->associatedWidget()->setAutoFillBackground(true);
}

QString bgBgClrPropEditor::getValue()
{
    QString ret;
    QColor bk;
    bk = dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->backgroundColor();
    ret = "[";
    ret += QString::number(bk.redF(), 'f', 3) + " ";
    ret += QString::number(bk.greenF(), 'f', 3) + " ";
    ret += QString::number(bk.blueF(), 'f', 3) + "]";
    return ret;
}

bool bgBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void bgBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {        
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");

        QColor bkGround = QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat());
        dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setBackgroundColor(bkGround);
    }
}

QString bgBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}


QWidget * bgBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void bgBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}
////
///

bgFgClrPropEditor::bgFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString bgFgClrPropEditor::getValue()
{
    QString ret;
    QColor fg;
    fg = dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->foregroundColor();
    ret = "[";
    ret += QString::number(fg.redF(), 'f', 3) + " ";
    ret += QString::number(fg.greenF(), 'f', 3) + " ";
    ret += QString::number(fg.blueF(), 'f', 3) + "]";
    return ret;
}

bool bgFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void bgFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");

        QColor fg = QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat());
        dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setForegroundColor(fg);
    }
}

QString bgFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}


QWidget * bgFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void bgFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

///
////
bgTitlePropEditor::bgTitlePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "Title";
}

QString bgTitlePropEditor::getValue()
{
    return dynamic_cast<QGroupBox *>(this->ctrl->associatedWidget())->title();
}

bool bgTitlePropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    return true;
}

void bgTitlePropEditor::setValue(QString newVal)
{
    dynamic_cast<QGroupBox *>(this->ctrl->associatedWidget())->setTitle(newVal);
}

QString bgTitlePropEditor::generateCode()
{
    QString ret;
    ret = "'title', '";
    ret += dynamic_cast<QGroupBox *>(this->ctrl->associatedWidget())->title();
    ret += "'";
    return ret;
}
////
////
///

bgTitlePosPropEditor::bgTitlePosPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "TitlePosition";
}

QString bgTitlePosPropEditor::getValue()
{
    return bgTitlePos[dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->titlePos()];
}

bool bgTitlePosPropEditor::isValidValue(QString newVal)
{
    bool ret = (newVal == bgTitlePos[bgLeftTop]) ||
            (newVal == bgTitlePos[bgCenterTop]) ||
            (newVal == bgTitlePos[bgRightTop])||
            (newVal == bgTitlePos[bgLeftBottom])||
            (newVal == bgTitlePos[bgCenterBottom])||
            (newVal == bgTitlePos[bgRightBottom]);
    return ret;
}

void bgTitlePosPropEditor::setValue(QString newVal)
{
    if(bgTitlePos[bgLeftTop] == newVal)
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setTitlePos(bgLeftTop);
    else if(bgTitlePos[bgCenterTop] == newVal)
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setTitlePos(bgCenterTop);
    else if(bgTitlePos[bgRightTop] == newVal)
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setTitlePos(bgRightTop);
    else if(bgTitlePos[bgLeftBottom] == newVal)
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setTitlePos(bgLeftBottom);
    else if(bgTitlePos[bgCenterBottom] == newVal)
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setTitlePos(bgCenterBottom);
    else if(bgTitlePos[bgRightBottom] == newVal)
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setTitlePos(bgRightBottom);
}

QString bgTitlePosPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * bgTitlePosPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);


    cb->addItem(bgTitlePos[bgLeftTop]);
    cb->addItem(bgTitlePos[bgCenterTop]);
    cb->addItem(bgTitlePos[bgRightTop]);
    cb->addItem(bgTitlePos[bgLeftBottom]);
    cb->addItem(bgTitlePos[bgCenterBottom]);
    cb->addItem(bgTitlePos[bgRightBottom]);

    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(titlePosChange(QString)));
    return cb;
}

void bgTitlePosPropEditor::titlePosChange(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(titlePosChange(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(titlePosChange(QString)));
    }
}

//////
///

bgFontNamePropEditor::bgFontNamePropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "FontName";
}

QString bgFontNamePropEditor::getValue()
{
    return dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->fontName();
}

bool bgFontNamePropEditor::isValidValue(QString newVal)
{
    return fontDB.families().contains(newVal);
}

void bgFontNamePropEditor::setValue(QString newVal)
{
    dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setFontName(newVal);
}

QString bgFontNamePropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * bgFontNamePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItems(fontDB.families());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
    return cb;
}


void bgFontNamePropEditor::fontChanged(const QString & txt)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(txt))
        this->setValue(txt);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontChanged(QString)));
    }
}

///
///

bgFontSizePropEditor::bgFontSizePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "FontSize";
}

QString bgFontSizePropEditor::getValue()
{
    int size;
    size = dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->fontSize();
    return QString::number(size);
}

bool bgFontSizePropEditor::isValidValue(QString newVal)
{
    bool Ok;
    int dVal;
    dVal = newVal.toInt(&Ok);
    return (dVal > 0) && Ok;
}

void bgFontSizePropEditor::setValue(QString newVal)
{
    dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setFontSize(newVal.toInt());
}

QString bgFontSizePropEditor::generateCode()
{
    QString ret;

    ret = "'" + this->propName + "', " + this->getValue() + ", ";
    ret += "'FontUnits', 'points'";
    return ret;
}

QWidget * bgFontSizePropEditor::getEditWidget()
{
    spinUpdPropWdg * sb = new spinUpdPropWdg(this);
    sb->setAlignment(Qt::AlignRight);
    sb->setMinimum(1);
    sb->setMaximum(5000);
    sb->setSingleStep(1);
    sb->setValue(this->getValue().toInt());
    connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
    return sb;
}

void bgFontSizePropEditor::fontSizeChanged(const QString &text)
{
    spinUpdPropWdg * sb = (spinUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
        this->setValue(text);
    }
    else
    {
        disconnect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
        sb->setValue(this->getValue().toInt());
        connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(fontSizeChanged(QString)));
    }
}
///
///

bgFontWeightPropEditor::bgFontWeightPropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "FontWeight";
}

QString bgFontWeightPropEditor::getValue()
{
    return bgFontWeight[dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->fontWeight()];
}

bool bgFontWeightPropEditor::isValidValue(QString newVal)
{
    return (newVal == "bold") || (newVal == "normal");
}

void bgFontWeightPropEditor::setValue(QString newVal)
{
    if (newVal == "bold")
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setFontWight(bgWBold);
    else
      dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setFontWight(bgWNormal);
}

QString bgFontWeightPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * bgFontWeightPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("bold");

    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
    return cb;
}

void bgFontWeightPropEditor::fontWeightChanged(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontWeightChanged(QString)));
    }
}
///
///

bgFontAnglePropEditor::bgFontAnglePropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->propName = "FontAngle";
}

QString bgFontAnglePropEditor::getValue()
{
    return bgFontAngle[dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->fontAngle()];
}

bool bgFontAnglePropEditor::isValidValue(QString newVal)
{
    return (newVal == "italic") || (newVal == "normal");
}

void bgFontAnglePropEditor::setValue(QString newVal)
{
    if(newVal == "italic")
        dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setFontAngle(bgItalic);
    else
        dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setFontAngle(bgNormal);
}

QString bgFontAnglePropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * bgFontAnglePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("normal");
    cb->addItem("italic");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
    return cb;
}

void bgFontAnglePropEditor::fontAngleChanged(const QString & text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
      this->setValue(text);
    }
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontAngleChanged(QString)));
    }
}

///
///
bgBorderWidtPropEditor::bgBorderWidtPropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
        this->propName = "BorderWidth";
}

QString bgBorderWidtPropEditor::getValue(){
    int width;
    QString ret;
    width = dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->borderWidth();
    ret = QString::number(width);
    return ret;
}

bool bgBorderWidtPropEditor::isValidValue(QString newVal){
    bool ret;
    newVal.toInt(&ret);
    return ret;
}

void bgBorderWidtPropEditor::setValue(QString newVal){
    int width = newVal.toInt();
    dynamic_cast<buttonGroupCtrl *>(this->ctrl)->styleGenerator->setBorderWidth(width);
}

QString bgBorderWidtPropEditor::generateCode(){
    return "'" + this->propName + "', " + this->getValue();
 }

QWidget * bgBorderWidtPropEditor::getEditWidget()
{
    spinUpdPropWdg * sb = new spinUpdPropWdg(this);
    sb->setAlignment(Qt::AlignRight);
    sb->setMinimum(1);
    sb->setMaximum(5000);
    sb->setSingleStep(1);
    sb->setValue(this->getValue().toInt());
    connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(borderSizeChanged(QString)));
    return sb;
}

void bgBorderWidtPropEditor::borderSizeChanged(const QString &text)
{
    spinUpdPropWdg * sb = (spinUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
    {
        this->setValue(text);
    }
    else
    {
        disconnect(sb, SIGNAL(valueChanged(QString)), this, SLOT(borderSizeChanged(QString)));
        sb->setValue(this->getValue().toInt());
        connect(sb, SIGNAL(valueChanged(QString)), this, SLOT(borderSizeChanged(QString)));
    }
}


unsigned int buttonGroupCtrl::uicNameCounter = 0;

unsigned int buttonGroupCtrl::getNameCounter()
{
    return uicNameCounter;
}

void buttonGroupCtrl::addWidget(abstractUICCtrl *w){
    /* Resolve exclusive radio button implementation*/
    if (w->className() == "radioButtonCtrl")
        ((QRadioButton *)w->associatedWidget())->setAutoExclusive(true);
    else if(w->className() == "toggleCtrl")
        ((QPushButton *)w->associatedWidget())->setAutoExclusive(true);
    this->addedWidgets.push_back(w);
    this->getParentWnd()->getSrcWnd()->updateWidgetList(this->addedWidgets);
}

buttonGroupCtrl::buttonGroupCtrl(QWidget * parent,
                               abstractUICCtrl *octaveParent,
                               childWndDlg *parentWnd):
    abstractUICCtrl(parent, octaveParent, parentWnd)

{


    QColor bkGround;

    editableButtonGroup * groupPanel = new editableButtonGroup(parent, this);

    bkGround = groupPanel->palette().color(groupPanel->backgroundRole());

    uicNameCounter++;

    this->setCtrlName("ButtonGroup_"+QString::number(uicNameCounter));
    groupPanel->setTitle(this->getCtrlName());
    this->vIsContainer = true;
    this->setAssociatedWidget(groupPanel);

    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new positionPropEditor(this));
    //this->properties.registerProp(new fontNamePropEditor(this));

    this->properties.registerProp(new bgTitlePropEditor(this));
    this->properties.registerProp(new bgBgClrPropEditor(this));
    this->properties.registerProp(new bgFgClrPropEditor(this));
    this->properties.registerProp(new bgBorderWidtPropEditor(this));    
    this->properties.registerProp(new bgFontNamePropEditor(this));
    this->properties.registerProp(new bgFontSizePropEditor(this));
    this->properties.registerProp(new bgFontWeightPropEditor(this));
    this->properties.registerProp(new bgFontAnglePropEditor(this));
    this->properties.registerProp(new bgTitlePosPropEditor(this));
    styleGenerator = new bgStyleGenerator(this);
}

void buttonGroupCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableButtonGroup * gp = dynamic_cast<editableButtonGroup *>(this->associatedWidget());
    gp->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList buttonGroupCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    QList<abstractUICCtrl *>::Iterator ctrl;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uibuttongroup( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";

    for(ctrl  = this->addedWidgets.begin();
        ctrl != this->addedWidgets.end();
        ++ctrl)
    {
        if((*ctrl)!= this)
          ret << (*ctrl)->generateMFile(path);
    }

    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList buttonGroupCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}



buttonGroupCtrl::~buttonGroupCtrl()
{
    QList<abstractUICCtrl *>::Iterator w;    
    mainWnd::getPropPan()->getTblProp()->setRowCount(0);
    mainWnd::getPropPan()->getTblProp()->verticalHeader()->hide();
    for(w = addedWidgets.begin(); w != addedWidgets.end(); ++w)
        if((*w) != this)
          delete (*w);
    addedWidgets.clear();
    delete this->getAssociatedWidget();
    delete this->styleGenerator;
}

buttonGroupCtrlGen::buttonGroupCtrlGen():controlGenerator("buttonGroup")
{
}

abstractUICCtrl * buttonGroupCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    buttonGroupCtrl * gp = new buttonGroupCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    QXmlStreamReader::TokenType readedToken;
    prop = gp->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = gp->properties.getNext();
    }

    readedToken = xml.readNext();
    while(!xml.atEnd() && !xml.isEndElement())
    {
        if(readedToken == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "ContainerSrc")
            {
                src = xml.readElementText();
                if(src.length() > 0)
                    gp->setSrcCallBack(src.split('\n'));
            }
            readedToken = xml.tokenType();
        }
        else
            readedToken = xml.readNext();
    }
    gp->deselect();
    return gp;
}
