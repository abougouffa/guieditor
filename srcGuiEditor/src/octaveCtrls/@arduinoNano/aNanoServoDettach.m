% aNanoServoDetach(obj, nroServo)
% obj: the arduino controll used for communication
% nroServo: index of servo list. Valid values a in range [0, 3]
% The package send is:
% 0xB0, CMD = 7, nroServo, 0, 0, 0, 0, suma, 0x0b
function aNanoServoDetach(obj, nroServo)
  v = [176, 6, nroServo, 0, 0, 0, 0, 0, 11];
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  port = obj.serialDev;
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor

endfunction
