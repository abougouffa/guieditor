function retObj = eduCIAA(varargin)  
  havePkg = pkg('list', 'instrument-control');
  if(isempty(havePkg)) 
    disp "This package require a instrument-control package for run. ";
    disp "Please install instrument-control package from the forge repo to continue.";
    return;
  else 
    pkg('load', 'instrument-control')
  endif
  %% Verificamos que se pueda acceder al puerto serie.
  if (exist("serial") != 3)
    disp "Serial port unsupported";
    return;
  endif
  
  if (length (varargin) < 2 || rem (length (varargin), 2) != 0)
    error ("set: expecting property/value pairs");
  endif
  
  retObj.serialPort = ''; %serialPortName;
  retObj.serialDev = struct(); %serial(serialPortName, 115200, 1);
  retObj = class(retObj, "eduCIAA");    
  
  while (length (varargin) > 1)
    prop = varargin{1};
    val = varargin{2};
    varargin(1:2) = [];
    if (strcmp(prop, "serialPort"))
      retObj.serialPort = val;
      retObj.serialDev = serial(val, 115200);      
      srl_flush(retObj.serialDev);
    endif
           
    for ind=1:3
      strTest = ["ledNro_" num2str(ind)];
      if(strcmp(strTest, prop))
        setLed(retObj, ind, val);
      endif
    endfor 

    if(strcmp("ledRGBRed", prop))
        setLed(retObj, 4, val);
    elseif(strcmp("ledRGBGreen", prop))
        setLed(retObj, 5, val);
    elseif(strcmp("ledRGBBlue", prop))
        setLed(retObj, 6, val);
    endif      
    if(strcmp("dacOutput", prop))
        setDAC(retObj, str2num(val));
    endif      
  endwhile  
endfunction
