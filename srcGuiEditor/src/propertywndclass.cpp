/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "propertywndclass.h"
#include "ui_propertywndclass.h"
#include <QFileDialog>
#include <QFontDialog>

propertyWndClass::propertyWndClass(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::propertyWndClass)
{
    ui->setupUi(this);
}

propertyWndClass::~propertyWndClass()
{
    delete ui;
}

void propertyWndClass::on_btnChangeOctavePath_clicked()
{
    QString pOct = QFileDialog::getOpenFileName(this, tr("Octave file path"), ui->labOctavePath->text(), tr("Octave binary(octave*);;All files (*.*);;"));
    if(!pOct.isNull())
    {
        ui->labOctavePath->setText(pOct);
        ui->labOctavePath->setToolTip(pOct);

    }
}

void propertyWndClass::on_btnChangeTmpPath_clicked()
{
    QString tmpDir;
    tmpDir = QFileDialog::getExistingDirectory(this, tr("Termporal dir path"), ui->labTempDir->text());
    if(!tmpDir.isNull())
    {
        ui->labTempDir->setText(tmpDir);
        ui->labTempDir->setToolTip(tmpDir);
    }
}

void propertyWndClass::on_btnAddLib_clicked()
{
    QString libDir;
    libDir = QFileDialog::getExistingDirectory(this, tr("Library path to add"));
    if(!libDir.isNull())
    {
        ui->lstPathLib->addItem(libDir);
    }
}

void propertyWndClass::on_btnRemLib_clicked()
{
    if(ui->lstPathLib->currentRow() >= 0)
      delete ui->lstPathLib->takeItem(ui->lstPathLib->currentRow());
}

void propertyWndClass::on_btnQtPlugginPath_clicked()
{
    QString plugginPath;
    plugginPath = QFileDialog::getExistingDirectory(this, tr("QT_PLUGIN_PATH"), ui->labQt5Pluggin->text());
    if(!plugginPath.isNull())
    {
        ui->labQt5Pluggin->setText(plugginPath);
        ui->labQt5Pluggin->setToolTip(plugginPath);
    }
}

void propertyWndClass::on_btnDeletePlugPath_clicked()
{
    ui->labQt5Pluggin->setText("");
}

void propertyWndClass::on_btnGZipPath_clicked()
{
    QString pGz = QFileDialog::getOpenFileName(this, tr("gzip file path"), ui->labGZipPath->text(), tr("gzip binary(gzip*);;All files (*.*);;"));
    if(!pGz.isNull())
    {
        ui->labGZipPath->setText(pGz);
        ui->labGZipPath->setToolTip(pGz);
    }
}

void propertyWndClass::on_btnTarPath_clicked()
{
    QString pTar = QFileDialog::getOpenFileName(this, tr("tar file path"), ui->labTarPath->text(), tr("tar binary(tar*);;All files (*.*);;"));
    if(!pTar.isNull())
    {
        ui->labTarPath->setText(pTar);
        ui->labTarPath->setToolTip(pTar);
    }
}

void propertyWndClass::on_btnChangeFontCtrl_clicked()
{
    bool ok;
    QFont newFont = QFontDialog::getFont(&ok, ctrls, this, tr("Select controls font name"));
    if(ok)
    {
        ctrls = newFont;
        ui->labFontCtrl->setText(newFont.family() + ", " + QString::number(newFont.pointSize()));
    }
}

void propertyWndClass::on_btnChangeFontEditor_clicked()
{
    bool ok;
    QFont newFont = QFontDialog::getFont(&ok, editor, this, tr("Select controls font name"));
    if(ok)
    {
        editor = newFont;
        ui->labSrcFont->setText(newFont.family() + ", " + QString::number(newFont.pointSize()));
    }
}
