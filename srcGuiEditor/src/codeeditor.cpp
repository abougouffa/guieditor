/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#include "codeeditor.h"
#include <QFontMetrics>
#include <QPainter>
#include <QTextBlock>

CodeEditor::CodeEditor(QWidget *parent) : QPlainTextEdit(parent)
{
    lineNumberArea = new LineNumberArea(this);

    connect(this, &CodeEditor::blockCountChanged, this, &CodeEditor::updateLineNumberAreaWidth);
    connect(this, &CodeEditor::updateRequest, this, &CodeEditor::updateLineNumberArea);
    connect(this, &CodeEditor::cursorPositionChanged, this, &CodeEditor::highlightCurrentLine);

    updateLineNumberAreaWidth(0);
    highlightCurrentLine();
    highlighter = new Highlighter(this->document());
    setLineWrapMode(QPlainTextEdit::NoWrap);
}
int CodeEditor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }
    if(digits < 4)
        digits = 4;
    int space = 4 + fontMetrics().width(QString(digits, QChar('9')));

    return space;
}

void CodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(Qt::yellow).lighter(160);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);
    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = qRound(blockBoundingGeometry(block).translated(contentOffset()).top());
    int bottom = top + qRound(blockBoundingRect(block).height());
    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::darkGray);
            painter.drawText(0, top, lineNumberArea->width()-2, fontMetrics().height(),
                             Qt::AlignRight, number);
        }
        block = block.next();
        top = bottom;
        bottom = top + qRound(blockBoundingRect(block).height());
        ++blockNumber;
    }
}

CodeEditor::~CodeEditor(){
   delete highlighter;
}

Highlighter::Highlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    keywordFormat.setForeground(Qt::darkRed);
    keywordFormat.setFontWeight(QFont::Bold);
    const QString keywordPatterns[] = {
        QStringLiteral("\\b__FILE__\\b"),
        QStringLiteral("\\b__LINE__\\b"),
        QStringLiteral("\\bbreak\\b"),
        QStringLiteral("\\bcase\\b"),
        QStringLiteral("\\bcatch\\b"),
        QStringLiteral("\\bclassdef\\b"),
        QStringLiteral("\\bcontinue\\b"),
        QStringLiteral("\\bdo\\b"),
        QStringLiteral("\\belse\\b"),
        QStringLiteral("\\belseif\\b"),
        QStringLiteral("\\bend\\b"),
        QStringLiteral("\\bend_try_catch\\b"),
        QStringLiteral("\\bend_unwind_protect\\b"),
        QStringLiteral("\\bendclassdef\\b"),
        QStringLiteral("\\bendenumeration\\b"),
        QStringLiteral("\\bendevents\\b"),
        QStringLiteral("\\bendfor\\b"),
        QStringLiteral("\\bendfunction\\b"),
        QStringLiteral("\\bendif\\b"),
        QStringLiteral("\\bendmethods\\b"),
        QStringLiteral("\\bendparfor\\b"),
        QStringLiteral("\\bendproperties\\b"),
        QStringLiteral("\\bendswitch\\b"),
        QStringLiteral("\\bendwhile\\b"),
        QStringLiteral("\\benumeration\\b"),
        QStringLiteral("\\bevents\\b"),
        QStringLiteral("\\bfor\\b"),
        QStringLiteral("\\bfunction\\b"),
        QStringLiteral("\\bglobal\\b"),
        QStringLiteral("\\bif\\b"),
        QStringLiteral("\\bmethods\\b"),
        QStringLiteral("\\botherwise\\b"),
        QStringLiteral("\\bparfor\\b"),
        QStringLiteral("\\bpersistent\\b"),
        QStringLiteral("\\bproperties\\b"),
        QStringLiteral("\\breturn\\b"),
        QStringLiteral("\\bswitch\\b"),
        QStringLiteral("\\btry\\b"),
        QStringLiteral("\\buntil\\b"),
        QStringLiteral("\\bunwind_protect\\b"),
        QStringLiteral("\\bunwind_protect_cleanup\\b"),
        QStringLiteral("\\bwhile\\b")
    };
    for (const QString &pattern : keywordPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    singleLineCommentFormat.setForeground(Qt::darkGray);
    singleLineCommentFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegularExpression(QStringLiteral("[#%]([^{}]|$)[^\n]*"));
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    multiLineCommentFormat.setForeground(Qt::darkGray);
    multiLineCommentFormat.setFontWeight(QFont::Bold);
    comStart = QRegularExpression(QStringLiteral("[#%]{"));
    comEnd = QRegularExpression(QStringLiteral("[#%]}"));

    quotationFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression(QStringLiteral("[\"'].*[\"']"));
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    vectorFormat.setBackground(QColor::fromRgb(180, 255, 180));
    rule.pattern = QRegularExpression(QStringLiteral("\\[.*\\]"));
    rule.format = vectorFormat;
    highlightingRules.append(rule);

    functionFormat.setFontWeight(QFont::Bold);
    functionFormat.setForeground(Qt::blue);
    rule.pattern = QRegularExpression(QStringLiteral("\\b[A-Za-z0-9_]+(?=\\()"));
    rule.format = functionFormat;
    highlightingRules.append(rule);

}

void Highlighter::highlightBlock(const QString &text)
{    
    QTextCharFormat defaultFormat;
    setCurrentBlockState(0);
    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = text.indexOf(comStart);
    while (startIndex >= 0) {
        QRegularExpressionMatch match = comEnd.match(text, startIndex);
        int endIndex = match.capturedStart();
        int commentLength = 0;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                            + match.capturedLength();
        }

        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = text.indexOf(comStart, startIndex + commentLength);
    }

    for (const HighlightingRule &rule : qAsConst(highlightingRules)) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            if (format(match.capturedStart()) == defaultFormat)
              setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
}
