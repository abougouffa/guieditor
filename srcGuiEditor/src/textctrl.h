/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef TEXTCTRL_H
#define TEXTCTRL_H
#include <QWidget>
#include "abstractuicctrl.h"
#include <QLabel>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"

class textHAlignmentPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    textHAlignmentPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void textHAlignmentChanged(QString text);
};


class textTextPropEditor: public abstractPropEditor
{
public:
    textTextPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "String";
    }
    QString getValue(){
        return ((QLabel *)this->ctrl->associatedWidget())->text();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}
    virtual void setValue(QString newVal)
    {        

        ((QLabel *)this->ctrl->associatedWidget())->setText(newVal);                
        if(this->ctrl->properties.getValueByName("isAutoSize").toLower() == "true")
        {
          QFontMetrics fm(((QLabel *)this->ctrl->associatedWidget())->font());
          ((QLabel *)this->ctrl->associatedWidget())->resize(fm.width(newVal), fm.height());
        }

    }

    virtual QString generateCode()
    {
        return "'" + this->propName + "', '" + this->getValue() + "'";
    }
};

class textBgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    textBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class textFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    textFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class autoSizeTextPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    bool defValue;
public:
    autoSizeTextPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual bool canGenerateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void autoSizeChanged(int state);
};


class textCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

public:
    static unsigned int getNameCounter();
    textCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual QString className() { return "textCtrl";}
    virtual bool isAutoSize();
    virtual ~textCtrl();
};

class textCtrlGen: public controlGenerator
{
public:
  textCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // TEXTCTRL_H


