Selected modules:
adc bit enduser_setup file gpio i2c mqtt net node ow pwm sntp softuart spi struct tmr u8g2 uart wifi.

Flash process:

1) Erase entire flash space: ./esptool.py --port /dev/ttyUSB0 erase_flash
2) Write a new firmware image: ./esptool.py --port /dev/ttyUSB0 write_flash -fm dio 0x00000 <flash_file_name>
3) Connect to nodemcu board using a serial text terminal like gtkterm. Select a serial port (ttyUSB0) a setup the speed to 115200.
4) Do a hardware reset through on board button. You must view some special chars and a text showing information about the firmware. Next vew a lot of "." while system do a first boot.
5) Is not necessary but, if you want, can make a file system format. Using the serial terminal, when board has end of initialization, write: file.format()
6) Do a new hardware reset, view in terminal console the startup process. When startup end, check the available space using this commands:

> remaining, used, total=file.fsinfo()
> print("\nFile system info:\nTotal : "..total.." (k)Bytes\nUsed : "..used.." (k)Bytes\nRemain: "..remaining.." (k)Bytes\n")

Copy entire lines (not the ">" simbol). You must show something like: 

File system info:
Total : 3462796 (k)Bytes
Used : 0 (k)Bytes
Remain: 3462796 (k)Bytes

7) To use the nodeMCU control in guiEditor, you must unpack the lua application (luaApp.tar.gz) and copy all files to board. To do this you can use the ESPlorer. 

