--
--  Configure pin of ESP8266
--

local retCmd = {}

local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(1)
  if (sm["data"][2] == 2) then
    str = "Output opendrain"
  elseif(sm["data"][2] == 1) then
    str = "Output ttl"
  elseif(sm["data"][2] == 3) then
    str = "Input pullup"
  elseif(sm["data"][2] == 4) then
    str = "Input float"
  end            
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str);
  str = "Pin nro.: " .. sm["data"][1]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str);
  disp:sendBuffer()

  if (sm["data"][2] == 2) then
    gpio.mode(sm["data"][1], gpio.OPENDRAIN);
  elseif(sm["data"][2] == 1) then
    gpio.mode(sm["data"][1], gpio.OUTPUT);
  elseif(sm["data"][2] == 3) then
    gpio.mode(sm["data"][1], gpio.INPUT, gpio.PULLUP);
  elseif(sm["data"][2] == 4) then    
    gpio.mode(sm["data"][1], gpio.INPUT, gpio.FLOAT);
  end
end

retCmd.doCmd = doCmd;

collectgarbage()

return retCmd;
