--
--  Erase the file eus_params.lua
--  where wifi configuration is stored.
--
local retCmd = {}

local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(13)
  local str = "Remove config"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str);

  str = "Network config removed"
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str);
  disp:sendBuffer()
  file.remove("eus_params.lua")
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
