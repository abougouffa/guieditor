![Image of Yaktocat](/img/guiEditorWin.png)
# guiEditor Proyect Home Page

guiEditor is a visual graphical user interface editor for GNU Octave. It allows the visual graphic edition of the interfaces generating scripts that, from invocations to native GNU Octave functions, can be reproduced.
Some highlights of guiEditor are:
- It allows the management of projects that can incorporate multiple dialog windows, script files or images.
- Automatic generation of packages from projects, allowing applications to be distributed in a simple way.
- In addition to the controls specific to user interfaces such as buttons or edit boxes, it incorporates the representation of special controls in order to represent special entities (arduino development boards for example).
- Integration with GNU Octave in order to simplify the process of debugging applications.
- It has been developed using the Qt framework, so it can be used in the operating systems supported by this framework.

Please, if you make a scientific publication using this software, cite it through the paper titled "Graphical user interface editor for Octave applications" (https://onlinelibrary.wiley.com/doi/pdf/10.1002/eng2.12269) 

# Getting started

To start using guiEditor, you can compile the development environment or, in case of using Microsoft Windows as operating system, you can [download](/binaries/windows/guiEditor-1.0.0.zip) a binary with the latest version of the application. Similarly, if you use Ubuntu 20.04 you can use the latest version compiled in the cloud [download](https://gitlab.com/labinformatica/guieditor/-/jobs/artifacts/master/download?job=build) (to to use this version you must have the libqt5serialport5 package installed on your system) 
It is our intention to build packages to simplify their installation in other operating systems (GNU Linux specifically), but for now we do not have this alternative. In these cases, you must compile it from the source code.

## Compiling guiEditor

The compilation process is similar to that of any application developed using the qt framework. The current version of the editor has been developed using Qt 5.9.5. As a guide you can see this [video](https://www.youtube.com/watch?v=6z9W0_EJuto), the basic steps are as follows:

Make sure you have a suitable development environment. In various GNU Linux distributions this is accomplished by installing the build-essential package and the packages associated with the Qt environment (guiEditor uses the serial port to communicate with embedded systems so the libqt5serialport5-dev and libqt5serialport5 modules are also required). In Microsoft Windows OS just install the binary distribution of Qt with mingw compiler.

Build the app. Clone this repository or download it as a compressed file. Then you can open the project using Qt Creator (recommended option in Microsoft Windows) or from a terminal you can build the application using qmake and make:

  1. git clone https://gitlab.com/labinformatica/guieditor.git
  2. cd guieditor/
  3. mkdir build
  4. cd build
  5. qmake ../guiEditorPrj.pro -recursive -config release
  6. make
  7. cd srcGuiEditor

You will find 3 files, guiEditor (the application itself) and two additional files (srcGuiEditor_en.qm and srcGuiEditor_sp.qm) that contain the translation of the interface into Spanish and English. All three files make up the final application and you will need to store them in the same folder in order for the environment to work properly.

If you use the Linux operating system, you can find additional [files](/linuxLauncher) and [icons](/iconos) to create a launcher in the repository.

### Arch Linux / Manjaro
For Arch Linux based distributions, guiEditor can be installed from the [`guieditor-git` package on AUR](https://aur.archlinux.org/packages/guieditor-git).

Using `yay`, you can type:

```shell
yay -S guieditor-git
```

## Initial setup

guiEditor can work as a standalone application. But you get a better user experience when integrating with GNU Octave, tar and gzip applications.
The required configuration is done through the File / Properties menu, which will present a dialog window with two tabs. The first will allow configuring the route to gzip and tar, these tools are used to generate packages for GNU Octave. In GNU Linux they are usually found in the / bin or / usr / bin folders. In the case of Microsoft Windows, these tools are not part of the operating system and must be downloaded separately. There are different repositories where you can find versions of these tools, an alternative are the sites http://gnuwin32.sourceforge.net/packages/gtar.htm and http://gnuwin32.sourceforge.net/packages/gzip.htm.
Once installed, it is recommended to try to run them from a terminal, in order to verify that the routes to their dependencies are established.
In the second tab of the properties dialog window you can set the path to GNU Octave. In the case of GNU Linux, just indicating the path to the executable will suffice (it is usually / usr / bin / octave). guiEditor requires making calls to GNU Octave through in order to start an interactive session, therefore, in principle, it is not possible to use snap or flatpack packages.
In Microsoft Windows it may be necessary to indicate the path to Qt plugins used by GNU Octave. The plugins used by GNU Octave are part of the binary distribution so they can be found in the application folder (<install dir>/mingw64/qt5/plugins).

# Using guiEditor

The editor works similarly to other development tools for graphical interfaces. There are only a few details related to the fact that what you will get are script files.
The best way to use guiEditor for application development is through projects. A project is made up of a file and a set of folders that will contain different elements that make up the project. Namely:
- export: will contain the exported application so that it can be used in GNU Octave. It is an alternative to the automatic generation of packages in case you need to make some kind of change to the generated code.
- fcn: will contain script files with functions for GNU Octave. Each file must contain a function with the same name as the file. If files are copied to this folder, they will automatically become part of the project.
- gui: representation of dialogue windows. Each window is represented through an xml file, this folder contains this representation. As with functions, windows can be copied or removed as desired and the project structure will be modified.
- img: images that are part of the project. It should be considered that the format of the images must be consistent with the formats supported by GNU Octave.
- libs: additional libraries. If script files are included in this folder, they will be copied with the generated packages and will be part of their execution space. So if you want to add an existing library, it should only be copied to this folder and will be available for use.
- other: additional files, generally text, necessary for the generation of packages (toolbox)
- pkg: will contain the package (toolbox) to install the application once it has been generated. In addition to the tar.gz file that corresponds to the package, a folder with the package structure will be stored. It is only necessary to distribute the compressed file, but through the folder it is possible to verify its content.
- run: temporary folder where the application structure will be generated in order to be able to run it during the development process.

When you create a new project, this structure will be generated automatically. As you add elements to the project, its structure will be updated.
To start, see the [examples](/examples) and videos related.
